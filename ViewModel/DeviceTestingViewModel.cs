﻿using GreekFestPOS.Util;
using GreekFestPOS.Windows.Admin;
using GreekFestPOS.Windows.User;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;

namespace GreekFestPOS.ViewModel
{
    class DeviceTestingViewModel : ViewModelBase
    {
        public ICommand printReceiptCommand
        {
            get { return new DelegateCommand(printReceipt); }
        }

        public ICommand openDrawerCommand
        {
            get { return new DelegateCommand(openDrawer); }
        }

        public ICommand readMagstripeCommand
        {
            get { return new DelegateCommand(readMagstripe); }
        }

        private void printReceipt(object obj)
        {
            if(!Printer.printTest())
            {
                MessageBox.Show("Printing did not work. Please make sure the printer is defined in the configuration area.");
            }
        }

        private void openDrawer(object obj)
        {
            if (!Drawer.testOpen())
            {
                MessageBox.Show("Drawer did not work. Please make sure it is defined in the configuration area.");
            }
        }

        private void readMagstripe(object obj)
        {
            MagstripeTestWindow magstripeTestWindow = new MagstripeTestWindow();
            magstripeTestWindow.ShowDialog();
        }


    }
}
