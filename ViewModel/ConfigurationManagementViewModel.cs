﻿using GreekFestPOS.Model;
using GreekFestPOS.POCO;
using GreekFestPOS.Windows.Admin;
using Microsoft.PointOfService;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace GreekFestPOS.ViewModel
{
    class ConfigurationManagementViewModel : ViewModelBase
    {
        public DelegateCommand selectServiceObjectCommand { get { return new DelegateCommand(selectServiceObject); } }
        public DelegateCommand saveCommand { get { return new DelegateCommand(saveConfiguration); } }

        public List<ConfigurationItem> loadConfigurationItems
        {
            get
            {
                return App.configurationItems.Values.ToList<ConfigurationItem>();
            }
            
        }

        public void selectServiceObject(object parameter)
        {
            ServiceObjectBrowserWindow sob = new ServiceObjectBrowserWindow();
            sob.ShowDialog();

            ConfigurationItem<DeviceInfoConfiguration> config = (ConfigurationItem<DeviceInfoConfiguration>)parameter;
            config.value = new DeviceInfoConfiguration(((ServiceObjectBrowserViewModel)sob.DataContext).selectedItem);
        }

        public void saveConfiguration(object parameter)
        {
            App.persistConfiguration();
            App.loadLocalConfiguration();
            closeWindow();
        }

    }
}
