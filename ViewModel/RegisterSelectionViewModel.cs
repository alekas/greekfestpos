﻿using GreekFestPOS.Model;
using GreekFestPOS.POCO;
using GreekFestPOS.Windows;
using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;
using System.Windows.Input;
using System.Windows.Media.Imaging;

namespace GreekFestPOS.ViewModel
{
    class RegisterSelectionViewModel : ViewModelBase
    {
        private Register _register { get; set; }
        private List<Register> registers;

        public ICommand selectRegisterCommand
        {
            get { return new DelegateCommand(selectRegister); }
        }

        public void selectRegister(object parameter)
        {
            App.register = _register;

            ((LocalConfigurationItem<int>)App.configurationItems["registers_id"]).value = _register.id;
            App.persistConfiguration();

            closeWindow();
        }

        public ListCollectionView loadRegisters
        {
            get
            {
                registers = RegisterDTO.loadAll().ToList();
                ListCollectionView lcv = new ListCollectionView(registers);
                lcv.GroupDescriptions.Add(new PropertyGroupDescription("location.name"));
                
                return lcv;
            }
        }



        public Register selectedRegister
        {
            get { return registers.FirstOrDefault(register => _register != null && register.id == _register.id); }
            set
            {
                _register = value;
                RaisePropertyChangedEvent("selectedRegister");
            }
        }
    }

}
