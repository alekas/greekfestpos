﻿using GreekFestPOS.Model;
using GreekFestPOS.POCO;
using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using System.Windows.Media.Imaging;

namespace GreekFestPOS.ViewModel
{
    class AddEditCouponViewModel : ViewModelBase
    {

        public Coupon coupon { get; set; }
        private IEnumerable<Product> products;

        public ICommand saveCommand
        {
            get { return new DelegateCommand(saveCoupon); }
        }

        public void saveCoupon(object parameter)
        {
            if (coupon.id != 0)
            {
                CouponDTO.saveCoupon(coupon);
            }
            else
            {
                CouponDTO.createCoupon(coupon);
            }

            CouponDTO.setProducts(coupon, products);

            closeWindow();
        }

        public IEnumerable<Product> loadProducts
        {
            get
            {
                products = ProductDTO.loadAllByCoupon(coupon);
                return products;
            }
            
        }
    }

}
