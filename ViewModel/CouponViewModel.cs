﻿using GreekFestPOS.Model;
using GreekFestPOS.POCO;
using GreekFestPOS.Util;
using GreekFestPOS.Windows.Admin;
using GreekFestPOS.Windows.User;
using MagTekServiceObject;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading;
using System.Windows.Input;
using Microsoft.PointOfService;
using System.ComponentModel;

namespace GreekFestPOS.ViewModel
{
    class CouponViewModel : ViewModelBase
    {
        private Transaction _transaction;
        private string _couponCode="";
        private Coupon _selectedCoupon;
        private string _errorText = "";


        public CouponViewModel()
        {
            loadAllCoupons();
        }

        public ICommand doneCommand
        {
            get { return new DelegateCommand(done); }
        }

        public ICommand applyCommand
        {
            get 
            {
                return new DelegateCommand(apply, couponCodeNotEmpty) 
                {
                    
                }; 
            }
        }

        public string errorText
        {
            get { return _errorText; }
            set
            {
                _errorText = value;
                RaisePropertyChangedEvent("errorText");
            }
        }

        public Transaction transaction
        {
            get { return _transaction; }
            set
            {
                _transaction = value;
                RaisePropertyChangedEvent("transaction");
            }
        }

        public ObservableCollection<Coupon> coupons
        {
            get;
            set;
        }

        public IEnumerable<Coupon> loadAllCoupons()
        {
            if (coupons == null || coupons.Count == 0)
            {
                coupons = CouponDTO.loadAll();
            }

            return coupons;
        }

        public Coupon selectedCoupon
        {
            get
            {
                return _selectedCoupon;
            }
            set
            {
                _selectedCoupon = value;
                RaisePropertyChangedEvent("selectedCoupon");
            }
        }

        public string couponCode
        {
            get { return _couponCode; }
            set
            {
                _couponCode = value;
                RaisePropertyChangedEvent("couponCode");
            }
        }

        public void done(object parameter)
        {
            closeWindow();
        }

        public void apply(object parameter)
        {
            this.errorText = "";

            if(selectedCoupon == null)
            {
                selectedCoupon = CouponDTO.loadByCode(couponCode);

                if(selectedCoupon == null)
                {
                    this.errorText = "Coupon code not found.";
                    return;
                }
            }
            else
            {
            }



            //loop through the groups and find a product in the transaction for each group
            
            selectedCoupon.tryAdd(transaction);

            if(selectedCoupon.products.Count == 0)
            {
                errorText = "No eligible products found.";
                selectedCoupon = null;
            }
            else
            {
                closeWindow();
            }

        }

        public bool couponCodeNotEmpty()
        {
            return !couponCode.Equals("") || selectedCoupon != null;
        }

    }
}
