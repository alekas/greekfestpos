﻿using GreekFestPOS.Model;
using GreekFestPOS.POCO;
using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using System.Windows.Media.Imaging;
using static System.Drawing.Printing.PrinterSettings;

namespace GreekFestPOS.ViewModel
{
    class AddEditProductViewModel : ViewModelBase
    {

        public Product product 
        { 
            get 
            { 
                return _product; 
            } 
            set 
            { 
                _product = value; 
                if (value != null) 
                { 
                    _picture = value.picture; 
                } 
            } 
        }
        public byte[] _picture;
        public Product _product;

        public ICommand choosePictureCommand
        {
            get { return new DelegateCommand(choosePicture); }
        }

        public void choosePicture(object parameter)
        {
            OpenFileDialog op = new OpenFileDialog();
            op.Title = "Choose a Picture";
            op.Filter = "All Supported Pictures|*.jpg;*.jpeg";
            if(op.ShowDialog() == true)
            {
                byte[] data;
                BitmapImage image = new BitmapImage(new Uri(op.FileName));
                JpegBitmapEncoder encoder = new JpegBitmapEncoder();
                encoder.Frames.Add(BitmapFrame.Create(image));
                using(MemoryStream ms = new MemoryStream())
                {
                    encoder.Save(ms);
                    data = ms.ToArray();
                }

                picture = data;
            }
        }

        public byte[] picture
        {
            get { return _picture; }
            set
            {
                _picture = value;
                product.picture = _picture;
                RaisePropertyChangedEvent("picture");
            }
        }

        public ICommand saveCommand
        {
            get { return new DelegateCommand(saveProduct); }
        }

        public void saveProduct(object parameter)
        {
            if(picture.Length == 0)
            {
                byte[] data;
                BitmapImage image = new BitmapImage(new Uri("pack://application:,,,/GreekFestPOS;component/Resources/images/no_image_available.jpg"));
                JpegBitmapEncoder encoder = new JpegBitmapEncoder();
                encoder.Frames.Add(BitmapFrame.Create(image));
                using (MemoryStream ms = new MemoryStream())
                {
                    encoder.Save(ms);
                    data = ms.ToArray();
                }

                picture = data;
            }
            if (product.id != 0)
            {
                ProductDTO.saveProduct(product);
            }
            else
            {
                ProductDTO.createProduct(product);
            }

            closeWindow();
        }

        public StringCollection getPrinters()
        {
            return InstalledPrinters;
        }
    }

}
