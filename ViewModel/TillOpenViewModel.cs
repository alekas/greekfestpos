﻿using GreekFestPOS.Model;
using GreekFestPOS.POCO;
using GreekFestPOS.Util;
using GreekFestPOS.Windows.Admin;
using GreekFestPOS.Windows.User;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace GreekFestPOS.ViewModel
{
    class TillOpenViewModel : ViewModelBase
    {
        public User _selectedUser;
        private string _amount = "";
        private string _errorText;

        public TillOpenViewModel()
        {
            Drawer.openDrawer();
        }

        public DelegateCommand openTillCommand
        {
            get { return new DelegateCommand(openTill); }
        }

        public Register register
        {
            get
            {
                return App.register;
            }
        }

        public User selectedUser
        {
            get { return _selectedUser; }
            set
            {
                _selectedUser = value;
                RaisePropertyChangedEvent("selectedUser");
            }
        }

        public string amount
        {
            get { return _amount; }
            set
            {
                _amount = value;
                RaisePropertyChangedEvent("amount");
            }
        }

        public IEnumerable<User> loadAllUsers
        {
            get
            {
                ObservableCollection<User> users = UserDTO.loadAll();
                return users;
            }
        }

        public string errorText
        {
            get
            {
                return _errorText;
            }

            set
            {
                _errorText = value;
                RaisePropertyChangedEvent("errorText");
            }
        }

        public void openTill(object parameter)
        {

            if(selectedUser == null)
            {
                errorText = "Please select the cashier";
                return;
            }

            if(amount == "" || decimal.Parse(amount)<=0)
            {
                errorText = "Please enter a bank amount";
                return;
            }

            Till till = new Till(register, selectedUser, amount, DateTime.Now);

            TillDTO.openTill(till);

            App.till = till;
            if (App.mainWindow != null)
            {
                MainWindowViewModel viewModel = ((MainWindowViewModel)App.mainWindow.DataContext);
                viewModel.RaisePropertyChangedEvent("cashInTill");
            }
            closeWindow();
        }

    }
}
