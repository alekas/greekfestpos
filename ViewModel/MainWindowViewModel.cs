﻿using GreekFestPOS.Model;
using GreekFestPOS.POCO;
using GreekFestPOS.UserControls;
using GreekFestPOS.Util;
using GreekFestPOS.Windows;
using GreekFestPOS.Windows.Admin;
using GreekFestPOS.Windows.User;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Data;
using System.Windows.Input;

namespace GreekFestPOS.ViewModel
{
    class MainWindowViewModel : ViewModelBase
    {

        private decimal _cash_in_till;
        //private ObservableCollection<Product> _transactionProducts = new ObservableCollection<Product>();

        private Transaction transaction;
        private string _customProductValue;

        public MainWindowViewModel()
        {
            createTransaction();
        }

        public void createTransaction()
        {
            transaction = new Transaction(App.register, App.currentUser);
            TransactionDTO.createTransaction(transaction);
            transactionChanged();
        }

        public ICommand sendMedicalAlertCommand
        {
            get { return new DelegateCommand(sendMedicalAlert); }
        }

        public ICommand logoutCommand
        {
            get { return new DelegateCommand(logout); }
        }

        public ICommand openDrawerCommand
        {
            get { return new DelegateCommand(openDrawer); }
        }

        public ICommand sendCashPickupAlertCommand
        {
            get { return new DelegateCommand(sendCashPickupAlert); }
        }

        public ICommand openAdminMenuCommand
        {
            get { return new DelegateCommand(openAdminMenu); }
        }

        public ICommand tenderCashCommand
        {
            get { return new DelegateCommand(tenderCash, transactionHasProducts); }
        }

        public ICommand tenderCreditCommand
        {
            get
            {
                return new DelegateCommand(tenderCredit, transactionPriceGreaterThan0);
            }
        }

        public ICommand applyCouponCommand
        {
            get
            {
                return new DelegateCommand(applyCoupon, transactionHasProducts);
            }
        }

        public ICommand newTransactionCommand
        {
            get
            {
                return new DelegateCommand(voidTransaction, transactionHasProducts);
            }
        }

        public ICommand removeCouponCommand
        {
            get { return new DelegateCommand(removeCoupon); }
        }

        public ICommand removePriceModificationCommand
        {
            get { return new DelegateCommand(removePriceModification); }
        }

        public ICommand expandedCommand
        {
            get { return new DelegateCommand(expanded); }
        }

        public ICommand collapsedCommand
        {
            get { return new DelegateCommand(collapsed); }
        }

        public ICommand removeProductCommand
        {
            get { return new DelegateCommand(removeProduct); }
        }

        public ICommand modifyProductCommand
        {
            get { return new DelegateCommand(modifyProduct); }
        }

        public ICommand specialRequestCommand
        {
            get { return new DelegateCommand(addSpecialRequest); }
        }

        public ICommand addToTransactionCommand
        {
            get { return new DelegateCommand(addToTransaction); }
        }

        public ICommand addCustomProductCommand
        {
            get { return new DelegateCommand(addCustomProduct); }
        }

        public void expanded(object parameter)
        {
            if (parameter is Product)
            {
                Product p = transaction.getProduct(((Product)parameter).id);
                /*foreach (Product product in loadProductsForCurrentLocation)
                {
                    product.isSelected = false;
                }*/
                p.isSelected = true;
            }
        }

        public void collapsed(object parameter)
        {

        }

        public bool isAdmin
        {
            get
            {
                return App.currentUser.isAdmin;
            }
        }

        public decimal cashInTill
        {
            get
            {
                return TillDTO.getCurrentCashAmount();
            }

            set
            {
                _cash_in_till = value;
                RaisePropertyChangedEvent("cashInTill");
            }
        }

        public IEnumerable<Location> loadLocations
        {
            get
            {
                ObservableCollection<Location> locations = LocationDTO.loadAll();

                return locations;
            }
        }

        public IEnumerable<Product> loadProductsForAllLocations
        {
            get
            {
                ObservableCollection<Product> products = ProductDTO.loadByLocation(App.location);

                return products;
            }
        }

        public User user
        {
            get { return App.currentUser; }
        }

        public Register register
        {
            get { return App.register; }
        }

        public Location location
        {
            get { return App.location; }
        }

        public void addToTransaction(object parameter)
        {
            Product p = ((Product)parameter).Copy();
            transaction.addProduct(p);

            reApportionCoupons();
            transactionChanged();
        }

        public void addCustomProduct(object parameter)
        {
            Location l = ((Location)parameter).Copy();
            Product p = new Product(1, l.name);
            p.price = decimal.Parse(customProductValue);
            addToTransaction(p);
            customProductValue = "";
        }

        public void reApportionCoupons()
        {
            foreach(Product product in transaction.products)
            {
                product.hasCoupon = false;
                product.group = null;
            }

            foreach (Coupon coupon in transaction.coupons)
            {
                coupon.products.Clear();
                coupon.tryAdd(transaction);
            }
        }

        public decimal transactionTotal
        {
            get
            {
                return transaction.calculatedTotal;
            }
        }

        public string customProductValue
        {
            get
            {
                return _customProductValue;
            }

            set
            {
                _customProductValue = value;
                RaisePropertyChangedEvent("customProductValue");
            }
        }

        public CompositeCollection transactionListItems
        {
            get
            {
                ObservableCollection<Product> products = new ObservableCollection<Product>(transaction.products);
                ObservableCollection<Coupon> coupons = new ObservableCollection<Coupon>(transaction.coupons);

                CompositeCollection products_and_coupons = new CompositeCollection();

                products_and_coupons.Add(new CollectionContainer() { Collection = products});
                products_and_coupons.Add(new CollectionContainer() { Collection = coupons });

                return products_and_coupons;
            }
        }        

        public void removeProduct(object parameter)
        {
            //FIXME: Make this MVVM
            if (MessageBox.Show("Are you sure you want to remove this product?", "Are you sure?", MessageBoxButton.YesNo) == MessageBoxResult.Yes)
            {
                if (parameter is Product)
                {
                    Product product = (Product)parameter;
                    foreach(Coupon coupon in transaction.coupons)
                    {
                        if(coupon.tryRemove(product))
                        {
                            break;
                        }
                    }
                    transaction.removeProduct(product);

                    reApportionCoupons();
                    
                    transactionChanged();
                }
            }
        }

        public void modifyProduct(object parameter)
        {
            Product product = (Product)parameter;
            ModifyProductWindow modifyProductWindow = new ModifyProductWindow();
            ((ModifyProductViewModel)modifyProductWindow.DataContext).product = product;
            modifyProductWindow.ShowDialog();

            transactionChanged();
        }

        public void tenderCash(object parameter)
        {
            KeypadModalWindow keypadModal = new KeypadModalWindow();
            keypadModal.formatForMoney = true;
            keypadModal.ShowDialog();

            decimal cash_before = cashInTill;

            if (!keypadModal.cancelled)
            {
                Decimal cashTendered = Decimal.Parse(keypadModal.Value);
                if (cashTendered < transactionTotal)
                {
                    MessageBox.Show("The amount tendered is less than the amount owed. Please try again.");
                    tenderCash(parameter);
                }
                else
                {
                    Decimal change = cashTendered - transactionTotal;

                    Drawer.openDrawer();

                    transaction.payments.Add(new Payment(0, transaction, DateTime.Now, PaymentType.CASH, cashTendered));

                    if(bool.Parse(App.getConfig("print_to_network_printer").ToString()))
                    {
                        Printer.printToNetwork(transaction);
                    }

                    if (customerWantsReceipt())
                    {
                        Printer.printTransaction(transaction);
                    }
                    completeTransaction();
                    if (change > 0)
                    {
                        ChangeWindow changeWindow = new ChangeWindow();
                        ((ChangeViewModel)changeWindow.DataContext).change = change;
                        changeWindow.ShowDialog();
                    }

                    decimal threshold = decimal.Parse(App.getConfig("cash_notification_threshold").ToString());
                    if(cash_before < threshold && cashInTill > threshold)
                    {
                        sendAutomaticCashPickupAlert();
                    }
                }
            }
        }

        public void tenderCredit(object parameter)
        {
            CreditWindow creditWindow = new CreditWindow();
            ((CreditViewModel)creditWindow.DataContext).transaction = transaction;
            creditWindow.ShowDialog();

            if (((CreditViewModel)creditWindow.DataContext).state == CCState.APPROVED)
            {
                if (bool.Parse(App.getConfig("print_to_network_printer").ToString()))
                {
                    Printer.printToNetwork(transaction);
                }
                if (transaction.calculatedTotal >= decimal.Parse(App.getConfig("credit_card_receipt_threshold").ToString()))
                {
                    Printer.printTransaction(transaction, true);
                }

                if (customerWantsReceipt())
                {
                    Printer.printTransaction(transaction);
                }
                completeTransaction();
            }
        }

        public void applyCoupon(object parameter)
        {
            CouponWindow couponWindow = new CouponWindow();
            CouponViewModel couponViewModel = ((CouponViewModel)couponWindow.DataContext);
            couponViewModel.transaction = transaction;
            couponWindow.ShowDialog();

            if(couponViewModel.selectedCoupon != null)
            {
                transaction.addCoupon(couponViewModel.selectedCoupon.Copy());
            }

            transactionChanged();
        }

        public void removeCoupon(object parameter)
        {
            Coupon coupon = (Coupon) parameter;
            foreach(Product product in transaction.products)
            {
                bool found = false;
                foreach(Product coupon_product in coupon.products)
                {
                    if(product.id == coupon_product.id && product.hasCoupon)
                    {
                        product.hasCoupon = false;
                        product.group = 0;
                        found = true;
                        
                        break;
                    }
                }
                if(found)
                {
                    break;
                }
            }
            transaction.removeCoupon(coupon);

            transactionChanged();

        }

        public void removePriceModification(object parameter)
        {
            ((Product)parameter).priceModification = null;

            transactionChanged();

        }

        public void completeTransaction()
        {
            transaction.completed_timestamp = DateTime.Now;
            transaction.status = Status.COMPLETED;
            TransactionDTO.saveTransaction(transaction);

            RaisePropertyChangedEvent("cashInTill");

            createTransaction();

        }

        private void voidTransaction(object obj)
        {
            if (MessageBox.Show("Are you sure you want to void this transaction and start over?", "Are you sure?", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
            {
                transaction.completed_timestamp = DateTime.Now;
                transaction.status = Status.VOIDED;
                TransactionDTO.saveTransaction(transaction);

                createTransaction();
            }
        }

        public bool transactionPriceGreaterThan0()
        {
            return transaction.calculatedTotal > 0;
        }

        public bool transactionHasProducts()
        {
            return transaction.products.Count > 0;
        }

        public bool canModifyProduct()
        {
            foreach(Product product in transaction.products)
            {
                if(product.isSelected)
                {
                    return product.canModify();
                }
            }

            return true;
        }

        private void transactionChanged()
        {
            RaisePropertyChangedEvent("transactionTotal");
            RaisePropertyChangedEvent("transactionListItems");
        }

        public void sendMedicalAlert(object parameter)
        {
            if (MessageBox.Show("Are you sure you want to send a medical alert?", "Are you sure?", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
            {
                MailMessage mail = new MailMessage("andreaslekas@gmail.com", App.getConfig("medical_notification_email").ToString());
                mail.Subject = "Medical Emergency";
                mail.Body = "Medical emergency at " + App.location.name + " triggered by " + App.currentUser.name;
                SmtpClient client = new SmtpClient();
                client.Host = "smtp.googlemail.com";
                client.Port = 587;
                client.UseDefaultCredentials = false;
                client.DeliveryMethod = SmtpDeliveryMethod.Network;
                client.EnableSsl = true;
                client.Credentials = new NetworkCredential("andreaslekas@gmail.com", "bootee-ascribe-skyjack-verdure");
                client.Send(mail);
            }
        }

        public void sendCashPickupAlert(object parameter)
        {
            if (MessageBox.Show("Are you sure you want to send a cash pickup alert?", "Are you sure?", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
            {
                MailMessage mail = new MailMessage("andreaslekas@gmail.com", App.getConfig("cash_pickup_notification_email").ToString());
                mail.Subject = "Cash Pickup";
                mail.Body = "Cash pickup request at " + App.location.name + " (" + App.register.name + "). There is currently " + cashInTill.ToString("C") + " in the till. Triggered by " + App.currentUser.name;
                SmtpClient client = new SmtpClient();
                client.Host = "smtp.googlemail.com";
                client.Port = 587;
                client.UseDefaultCredentials = false;
                client.DeliveryMethod = SmtpDeliveryMethod.Network;
                client.EnableSsl = true;
                client.Credentials = new NetworkCredential("andreaslekas@gmail.com", "bootee-ascribe-skyjack-verdure");
                client.Send(mail);
            }
        }

        public void sendAutomaticCashPickupAlert()
        {
            MailMessage mail = new MailMessage("andreaslekas@gmail.com", App.getConfig("cash_pickup_notification_email").ToString());
            mail.Subject = "Cash Pickup";
            mail.Body = "Cash in till crossed threshold at " + App.location.name + " (" + App.register.name + "). There is currently " + cashInTill.ToString("C") + " in the till.";
            SmtpClient client = new SmtpClient();
            client.Host = "smtp.googlemail.com";
            client.Port = 587;
            client.UseDefaultCredentials = false;
            client.DeliveryMethod = SmtpDeliveryMethod.Network;
            client.EnableSsl = true;
            client.Credentials = new NetworkCredential("andreaslekas@gmail.com", "bootee-ascribe-skyjack-verdure");
            client.Send(mail);
        }

        public void addSpecialRequest(object parameter)
        {
            SpecialRequestWindow window = new SpecialRequestWindow();
            SpecialRequestViewModel vm = (SpecialRequestViewModel)window.DataContext;
            vm.product = (Product)parameter;
            window.ShowDialog();

            transactionChanged();
        }

        private void openAdminMenu(object obj)
        {
            AdminDashboardWindow window = new AdminDashboardWindow();
            window.ShowDialog();
        }

        private void logout(object parameter)
        {
            App.logout();
            closeWindow();
        }

        private void openDrawer(object parameter)
        {
            Drawer.openDrawer();
            AuditTrail.action("OPEN_DRAWER");
        }

        private bool customerWantsReceipt()
        {
            return MessageBox.Show("Would the customer like a receipt?", "Receipt?", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes;
        }
    }
}
