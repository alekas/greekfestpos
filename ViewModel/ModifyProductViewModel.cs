﻿using GreekFestPOS.Model;
using GreekFestPOS.POCO;
using GreekFestPOS.Util;
using GreekFestPOS.Windows.Admin;
using GreekFestPOS.Windows.User;
using MagTekServiceObject;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading;
using System.Windows.Input;
using Microsoft.PointOfService;
using System.ComponentModel;
using GreekFestPOS.Enums;

namespace GreekFestPOS.ViewModel
{
    class ModifyProductViewModel : ViewModelBase
    {
        private decimal _afterAmount;
        private PriceModification priceModification = new PriceModification();
        private Product _product;


        public ModifyProductViewModel()
        {
        }

        public ICommand doneCommand
        {
            get { return new DelegateCommand(done); }
        }

        public ICommand applyCommand
        {
            get 
            { return new DelegateCommand(apply); }
        }

        public DollarOrPercent dollarOrPercent
        {
            get { return priceModification.dollarOrPercent; }
            set
            {
                priceModification.dollarOrPercent = value;
                calculateAfterAmount();
                RaisePropertyChangedEvent("dollarOrPercent");
            }
        }

        public ModificationType modificationType
        {
            get { return priceModification.modificationType; }
            set
            {
                priceModification.modificationType = value;
                calculateAfterAmount();
                RaisePropertyChangedEvent("modificationType");
            }
        }

        public string amount
        {
            get { return priceModification.amount; }
            set
            {
                priceModification.amount = value;
                calculateAfterAmount();
                RaisePropertyChangedEvent("amount");
            }
        }

        public void calculateAfterAmount()
        {
            afterAmount = priceModification.calculatePrice(product.price);
        }

        public decimal afterAmount
        {
            get { return _afterAmount; }
            set
            {
                _afterAmount = value;
                RaisePropertyChangedEvent("afterAmount");
            }
        }


        public void done(object parameter)
        {
            closeWindow();
        }

        public void apply(object parameter)
        {
            _product.priceModification = priceModification;
            closeWindow();
        }


        public Product product 
        { 
            get 
            {
                return _product;
            } 
            set 
            {
                _product = value;
                if(_product.priceModification != null)
                {
                    priceModification = _product.priceModification;
                    calculateAfterAmount();
                    RaisePropertyChangedEvent("dollarOrPercent");
                    RaisePropertyChangedEvent("modificationType");
                    RaisePropertyChangedEvent("amount");
                    RaisePropertyChangedEvent("afterAmount");

                }
            } 
        }
    }
}
