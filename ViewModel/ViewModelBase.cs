﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GreekFestPOS.ViewModel
{
    class ViewModelBase : INotifyPropertyChanged, IRequestCloseViewModel
    {
        public event PropertyChangedEventHandler PropertyChanged;

        public void RaisePropertyChangedEvent(string propertyName)
        {
            var handler = PropertyChanged;
            if(handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        protected void closeWindow()
        {
            EventHandler handler = RequestClose;
            if (handler != null)
            {
                handler(this, null);
            }
        }

        public event EventHandler RequestClose;
    }
}
