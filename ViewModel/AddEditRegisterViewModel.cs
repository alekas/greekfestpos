﻿using GreekFestPOS.Model;
using GreekFestPOS.POCO;
using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using System.Windows.Media.Imaging;

namespace GreekFestPOS.ViewModel
{
    class AddEditRegisterViewModel : ViewModelBase
    {
        private Register _register;

        public AddEditRegisterViewModel() : base()
        {
            this.loadAllLocations();
        }

        public Register register 
        { 
            get
            {
                return _register;
            }

            set 
            {
                _register = value;
            }
        }


        public ICommand saveCommand
        {
            get { return new DelegateCommand(saveRegister); }
        }

        public void saveRegister(object parameter)
        {
            register.location = selectedLocation;

            if (register.id != 0)
            {
                RegisterDTO.saveRegister(register);
            }
            else
            {
                RegisterDTO.createRegister(register);
            }

            closeWindow();
        }

        public ObservableCollection<Location> locations
        {
            get;
            set;
        }

        public IEnumerable<Location> loadAllLocations()
        {
            if (locations == null || locations.Count == 0)
            {
                locations = LocationDTO.loadAll();
            }

            return locations;
        }

        public Location selectedLocation
        {
            get { return locations.FirstOrDefault(location => location.id == register.location.id); }
            set
            {
                register.location = value;
                RaisePropertyChangedEvent("selectedLocation");
            }
        }
    }

}
