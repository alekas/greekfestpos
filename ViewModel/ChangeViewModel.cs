﻿using GreekFestPOS.Model;
using GreekFestPOS.POCO;
using GreekFestPOS.Windows.Admin;
using GreekFestPOS.Windows.User;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace GreekFestPOS.ViewModel
{
    class ChangeViewModel : ViewModelBase
    {
        private Decimal _change;

        public ChangeViewModel()
        {

        }

        public ICommand doneCommand 
        {
            get { return new DelegateCommand(done); }
        }

        public Decimal change
        {
            get { return _change; }
            set
            {
                _change = value;
                RaisePropertyChangedEvent("change");
            }
        }

        public void done(object parameter)
        {
            closeWindow();
        }
    }
}
