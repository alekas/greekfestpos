﻿using GreekFestPOS.Model;
using GreekFestPOS.POCO;
using GreekFestPOS.Util;
using GreekFestPOS.Windows.Admin;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;
using System.Windows.Input;

namespace GreekFestPOS.ViewModel
{
    class TransactionDetailViewModel : ViewModelBase
    {
        public Transaction transaction
        {
            get;
            set;
        }

        public DelegateCommand reprintReceiptCommand
        {
            get { return new DelegateCommand(reprintReceipt, isNotVoided); }
        }

        private bool isNotVoided()
        {
            return transaction.status.id != Status.VOIDED.id;
        }

        private void reprintReceipt(object parameter)
        {
            Printer.printTransaction(transaction);
        }

        public CompositeCollection transactionListItems
        {
            get
            {
                ObservableCollection<Product> products = new ObservableCollection<Product>(transaction.products);
                ObservableCollection<Coupon> coupons = new ObservableCollection<Coupon>(transaction.coupons);

                CompositeCollection products_and_coupons = new CompositeCollection();

                products_and_coupons.Add(new CollectionContainer() { Collection = products });
                products_and_coupons.Add(new CollectionContainer() { Collection = coupons });

                return products_and_coupons;
            }
        }

        public decimal changeDue { 
            get 
            {
                if (transaction.firstPayment != null && transaction.firstPayment.paymentType == PaymentType.CASH)
                {
                    return transaction.firstPayment.amountTendered - transaction.total;
                }
                else
                {
                    return 0;
                }
            }
        }
    }
}
