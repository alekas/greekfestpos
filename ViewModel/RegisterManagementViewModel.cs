﻿using GreekFestPOS.Model;
using GreekFestPOS.POCO;
using GreekFestPOS.Windows.Admin;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace GreekFestPOS.ViewModel
{
    class RegisterManagementViewModel : ViewModelBase
    {
        public IEnumerable<Register> loadAllRegisters
        {
            get
            {
                ObservableCollection<Register> registers = RegisterDTO.loadAll();
                return registers;
            }
        }

        public ICommand editRegisterCommand
        {
            get { return new DelegateCommand(editRegister); }
        }

        public ICommand addRegisterCommand
        {
            get { return new DelegateCommand(addRegister); }
        }

        private void editRegister(object register)
        {
            AddEditRegisterWindow addEditRegisterWindow = new AddEditRegisterWindow();
            var vm = addEditRegisterWindow.DataContext as AddEditRegisterViewModel;
            vm.register = (Register)register;
            addEditRegisterWindow.Show();
        }

        private void addRegister(object register)
        {
            AddEditRegisterWindow addEditRegisterWindow = new AddEditRegisterWindow();
            var vm = addEditRegisterWindow.DataContext as AddEditRegisterViewModel;
            vm.register = new Register();
            addEditRegisterWindow.Show();
        }

        public void deleteRegister(Register register)
        {
            RegisterDTO.deleteRegister(register);
            register = null;
        }
    }
}
