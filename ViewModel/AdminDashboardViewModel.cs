﻿using GreekFestPOS.Windows.Admin;
using GreekFestPOS.Windows.User;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace GreekFestPOS.ViewModel
{
    class AdminDashboardViewModel : ViewModelBase
    {
        public ICommand productManagementCommand
        {
            get { return new DelegateCommand(manageProducts); }
        }

        public ICommand locationManagementCommand
        {
            get { return new DelegateCommand(manageLocations); }
        }

        public ICommand registerManagementCommand
        {
            get { return new DelegateCommand(manageRegisters); }
        }

        public ICommand transactionManagementCommand
        {
            get { return new DelegateCommand(manageTransactions); }
        }

        public ICommand tillManagementCommand
        {
            get { return new DelegateCommand(manageTills); }
        }

        public ICommand deviceTestingCommand
        {
            get { return new DelegateCommand(testDevices); }
        }

        public ICommand systemConfigurationCommand
        {
            get { return new DelegateCommand(systemConfiguration); }
        }

        public ICommand gotoRegisterCommand
        {
            get { return new DelegateCommand(gotoRegister); }
        }

        public ICommand logoutCommand
        {
            get { return new DelegateCommand(logout); }
        }

        public ICommand couponManagementCommand
        {
            get { return new DelegateCommand(manageCoupons); }
        }

        private void manageProducts(object parameter)
        {
            ProductManagementWindow productManagement = new ProductManagementWindow();
            productManagement.Show();
        }

        private void manageLocations(object parameter)
        {
            LocationManagementWindow locationManagement = new LocationManagementWindow();
            locationManagement.Show();
        }

        private void manageRegisters(object parameter)
        {
            RegisterManagementWindow registerManagement = new RegisterManagementWindow();
            registerManagement.Show();
        }

        private void manageTransactions(object parameter)
        {
            TransactionManagementWindow transactionManagement = new TransactionManagementWindow();
            transactionManagement.Show();
        }

        private void manageTills(object parameter)
        {
            TillManagementWindow tillManagement = new TillManagementWindow();
            tillManagement.Show();
        }

        private void testDevices(object parameter)
        {
            DeviceTestingWindow deviceTesting = new DeviceTestingWindow();
            deviceTesting.Show();
        }

        private void manageCoupons(object parameter)
        {
            CouponManagementWindow couponManagement = new CouponManagementWindow();
            couponManagement.Show();
        }

        private void gotoRegister(object parameter)
        {
            closeWindow();
            App.openRegister();
        }

        private void systemConfiguration(object parameter)
        {
            ConfigurationManagementWindow configurationManagement = new ConfigurationManagementWindow();
            configurationManagement.Show();

            //ServiceObjectBrowserWindow serviceObjectBrowser = new ServiceObjectBrowserWindow();
            //serviceObjectBrowser.Show();
        }

        private void logout(object parameter)
        {
            App.logout();
            closeWindow();
        }
    }
}
