﻿using GreekFestPOS.Model;
using GreekFestPOS.POCO;
using GreekFestPOS.Util;
using GreekFestPOS.Windows.Admin;
using GreekFestPOS.Windows.User;
using MagTekServiceObject;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading;
using System.Windows.Input;
using Microsoft.PointOfService;
using System.ComponentModel;
using System.Windows;
using Bluelaser.Utilities;
using Clifton.Payment.Gateway;
using static Clifton.Payment.Gateway.PayeezyGateway;

namespace GreekFestPOS.ViewModel
{
    class CreditViewModel : ViewModelBase
    {
        private Transaction _transaction;
        private string _creditCardNumber="";
        private string _expirationDate="";

        private CCState _state = CCState.WAITING_FOR_SWIPE;
        private string _errorMessage;
        private string _keypadInput;

        public CreditViewModel()
        {
            state = CCState.WAITING_FOR_SWIPE;
            Magstripe.setCardSwipedDelegate(MSRDataEventHandler);
            Magstripe.enableDataEvent();
        }

        private void MSRDataEventHandler(Msr sender)
        {
            Msr msr = (Msr)sender;
            if(msr.State == Microsoft.PointOfService.ControlState.Error)
            {
                Application.Current.Dispatcher.Invoke((Action)(() =>
                {
                    state = CCState.SWIPE_ERROR;
                }));
                Magstripe.enableDataEvent();
            }
            else if(msr.State==Microsoft.PointOfService.ControlState.Idle)
            {
                Application.Current.Dispatcher.Invoke((Action)(() =>
                {
                    string cardData = Util.Util.TrackDataToString(msr.Track1Data) + ";" + Util.Util.TrackDataToString(msr.Track2Data);
                    processCardSwipe(cardData);
                }));
            }
        }

        public ICommand doneCommand
        {
            get { return new DelegateCommand(done); }
        }

        public decimal creditCardFee
        {
            get
            {
                if (App.getConfig("credit_card_fee") != null)
                {
                    return decimal.Parse((string)App.getConfig("credit_card_fee"));
                }

                return 0;
            }
        }

        public ICommand processCommand
        {
            get 
            {
                return new DelegateCommand(process) 
                {
                    
                }; 
            }
        }

        public Transaction transaction
        {
            get { return _transaction; }
            set
            {
                _transaction = value;
                RaisePropertyChangedEvent("transaction");
            }
        }

        public string creditCardNumber
        {
            get { return _creditCardNumber; }
            set
            {
                _creditCardNumber = value;
                RaisePropertyChangedEvent("creditCardNumber");
            }
        }

        public string keypadInput
        {
            get 
            {
                return _keypadInput;
            }
            set
            {
                if(value.Length > 20)
                {
                    return;
                }
                _keypadInput = value;

                int cc_length = 16;
                int inputLength = _keypadInput.Length;

                if(_keypadInput.Length >= 15)
                {
                    if(CreditCardUtility.IsValidNumber(_keypadInput.Substring(0, 15)))
                    {
                        cc_length = 15;
                    }

                    if(_keypadInput.Length == 16)
                    {
                        if(CreditCardUtility.IsValidNumber(_keypadInput.Substring(0, 16)))
                        {
                            cc_length = 16;
                        }
                    }
                }

                creditCardNumber = _keypadInput.Substring(0, Math.Min(cc_length, inputLength));
                if (_keypadInput.Length > cc_length)
                {
                    expirationDate = _keypadInput.Substring(cc_length, Math.Min(inputLength-cc_length, 4));
                }
                else
                {
                    expirationDate = "";
                }
                RaisePropertyChangedEvent("keypadInput");
            }
        }

        public string expirationDate
        {
            get { return _expirationDate; }
            set
            {
                _expirationDate = value;

                if(_expirationDate.Length == 4)
                {
                    _expirationDate = _expirationDate.Substring(0, 2) + "/" + _expirationDate.Substring(2);
                }

                RaisePropertyChangedEvent("expirationDate");
            }
        }

        public CCState state 
        { 
            get 
            {
                return _state;
            } 
            set 
            {
                _state = value;
                RaisePropertyChangedEvent("state");
            } 
        }

        public string errorMessage
        {
            get
            {
                return _errorMessage;
            }
            set
            {
                _errorMessage = value;
                RaisePropertyChangedEvent("errorMessage");
            }
        }

        public void processCardSwipe(string cardData)
        {
            state = CCState.AUTHORIZING;
            errorMessage = "";

            BackgroundWorker bw = new BackgroundWorker();
            PayeezyGateway.Response resp = null;

            bw.DoWork += (sender, args) =>
            {
                resp = PayeezyHelper.processTransaction(this.transaction, cardData);
            };

            bw.RunWorkerCompleted += (sender, args) =>
            {
                if (resp.ParsedBankResponseCode == BankResponseCode.Approved && resp.ParsedGatewayResponseCode == GatewayResponseCode.TransactionNormal)
                {
                    state = CCState.APPROVED;
                    closeWindow();
                }
                else
                {
                    state = CCState.DENIED;
                    if (resp.ErrorMessages.Count > 0)
                    {
                        errorMessage = resp.ErrorMessages[0].Code + ": " + resp.ErrorMessages[0].Description;
                    }
                    else if (resp.BankMessage != null && resp.BankMessage != "")
                    {
                        errorMessage = resp.ParsedBankResponseCode + "(" + resp.BankResponseCode + "): " + resp.BankMessage;
                    }

                    errorMessage += " - Try again or ask for alternate payment.";

                    try
                    {
                        Magstripe.enableDataEvent();
                    } catch(Exception e)
                    {
                        Magstripe.enableDataEvent();
                    }
                }
            };

            bw.RunWorkerAsync();
        }

        public void processCardManual()
        {
            state = CCState.AUTHORIZING;

            BackgroundWorker bw = new BackgroundWorker();
            PayeezyGateway.Response resp = null;

            bw.DoWork += (sender, args) =>
            {
                try
                {
                    resp = PayeezyHelper.processTransaction(this.transaction, creditCardNumber, expirationDate);
                } catch (Exception ex)
                {
                    state = CCState.DENIED;
                    errorMessage = ex.Message;
                }
            };

            bw.RunWorkerCompleted += (sender, args) =>
            {
                if (resp != null)
                {
                    if (resp.ParsedBankResponseCode == BankResponseCode.Approved && resp.ParsedGatewayResponseCode == GatewayResponseCode.TransactionNormal)
                    {
                        state = CCState.APPROVED;
                        closeWindow();
                    }
                    else
                    {
                        state = CCState.DENIED;
                        if(resp.ErrorMessages.Count > 0)
                        {
                            errorMessage = resp.ErrorMessages[0].Code + ": " + resp.ErrorMessages[0].Description;
                        } else if(resp.BankMessage != null && resp.BankMessage != "")
                        {
                            errorMessage = resp.ParsedBankResponseCode + "(" + resp.BankResponseCode + "): " + resp.BankMessage;
                        }
                    }
                }
            };

            bw.RunWorkerAsync();
        }

        public void done(object parameter)
        {
            Magstripe.destroy();
            closeWindow();
        }

        public void process(object parameter)
        {
            processCardManual();//"B4737029035626226^LEKAS/ ANDREAS            ^17091010000000      00852000000");
        }

    }
}
