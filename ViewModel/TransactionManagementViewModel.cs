﻿using GreekFestPOS.Model;
using GreekFestPOS.POCO;
using GreekFestPOS.Windows.Admin;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace GreekFestPOS.ViewModel
{
    class TransactionManagementViewModel : ViewModelBase
    {
        public IEnumerable<Transaction> loadAllTransactions
        {
            get
            {
                ObservableCollection<Transaction> transactions = TransactionDTO.loadComplete();
                return transactions;
            }
        }

        public ICommand viewTransactionCommand
        {
            get { return new DelegateCommand(viewTransaction); }
        }

        public void viewTransaction(object parameter)
        {
            Transaction transaction = (Transaction)parameter;

            TransactionDetailWindow transactionDetails = new TransactionDetailWindow();
            TransactionDetailViewModel viewModel = (TransactionDetailViewModel)transactionDetails.DataContext;

            viewModel.transaction = TransactionDTO.loadById(transaction.id);
            transactionDetails.ShowDialog();
        }
    }
}
