﻿using GreekFestPOS.Model;
using GreekFestPOS.POCO;
using GreekFestPOS.Windows.Admin;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace GreekFestPOS.ViewModel
{
    class LocationManagementViewModel : ViewModelBase
    {
        public IEnumerable<Location> loadAllLocations
        {
            get
            {
                ObservableCollection<Location> locations = LocationDTO.loadAll();
                return locations;
            }
        }

        public ICommand editLocationCommand
        {
            get { return new DelegateCommand(editLocation); }
        }

        public ICommand addLocationCommand
        {
            get { return new DelegateCommand(addLocation); }
        }

        private void editLocation(object location)
        {
            AddEditLocationWindow addEditLocationWindow = new AddEditLocationWindow();
            var vm = addEditLocationWindow.DataContext as AddEditLocationViewModel;
            vm.location = (Location)location;
            addEditLocationWindow.Show();
        }

        private void addLocation(object location)
        {
            AddEditLocationWindow addEditLocationWindow = new AddEditLocationWindow();
            var vm = addEditLocationWindow.DataContext as AddEditLocationViewModel;
            vm.location = new Location();
            addEditLocationWindow.Show();
        }

        public void deleteLocation(Location location)
        {
            LocationDTO.deleteLocation(location);
            location = null;
        }
    }
}
