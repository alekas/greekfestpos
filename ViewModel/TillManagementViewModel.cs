﻿using GreekFestPOS.Windows.Admin;
using GreekFestPOS.Windows.User;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace GreekFestPOS.ViewModel
{
    class TillManagementViewModel : ViewModelBase
    {
        public ICommand openTillCommand
        {
            get { return new DelegateCommand(openTill, isTillClosed); }
        }

        public ICommand closeTillCommand
        {
            get { return new DelegateCommand(closeTill, isTillOpen); }
        }

        private bool isTillOpen()
        {
            return App.till != null;
        }

        private bool isTillClosed()
        {
            return App.till == null;
        }

        private void openTill(object parameter)
        {
            TillOpenWindow tillOpenWindow = new TillOpenWindow();
            tillOpenWindow.Show();
        }

        private void closeTill(object parameter)
        {
            TillCloseoutWindow tillCloseoutWindow = new TillCloseoutWindow();
            tillCloseoutWindow.Show();
        }

        private void manageRegisters(object parameter)
        {
            RegisterManagementWindow registerManagement = new RegisterManagementWindow();
            registerManagement.Show();
        }

        private void manageCoupons(object parameter)
        {
            CouponManagementWindow couponManagement = new CouponManagementWindow();
            couponManagement.Show();
        }
    }
}
