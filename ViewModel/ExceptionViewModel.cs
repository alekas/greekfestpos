﻿using GreekFestPOS.Model;
using GreekFestPOS.POCO;
using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Input;
using System.Windows.Media.Imaging;

namespace GreekFestPOS.ViewModel
{
    class ExceptionViewModel : ViewModelBase
    {

        private Exception _exception;

        public ICommand sendReportCommand
        {
            get { return new DelegateCommand(sendReport); }
        }
        public ICommand cancelCommand
        {
            get { return new DelegateCommand(cancel); }
        }

        public Exception exception
        {
            get
            {
                return _exception;
            }

            set
            {
                _exception = value;
                RaisePropertyChangedEvent("exception");
            }
        }

        public string exceptionHTML
        {
            get
            {
                StackTrace st = new StackTrace(exception, true);

                StackFrame usefulFrame = null;
                //Get the first stack frame with a filename
                foreach (StackFrame frame in st.GetFrames())
                {
                    if (frame.GetFileName() != null && frame.GetFileName() != "")
                    {
                        usefulFrame = frame;
                        break;
                    }
                }


                //Get the file name
                string fileName = usefulFrame.GetFileName();

                //Get the method name
                string methodName = usefulFrame.GetMethod().Name;

                //Get the line number from the stack frame
                int line = usefulFrame.GetFileLineNumber();

                //Get the column number
                int col = usefulFrame.GetFileColumnNumber();

                string html = "<h1>"+exception.Message+"</h1>";

                html += "<h2>Source Information:</h2>\n";
                html += fileName + ", line: " + line + ":" + col + " - Method: " + methodName + "<br><br>";

                html += "<h2>Application Information:</h2>\n";
                html += "<string>Current User: </strong>" + (App.currentUser==null ? "NULL" : App.currentUser.name + ":"+App.currentUser.id) +"<br>";
                html += "<string>Current Register: </strong>" + (App.register == null ? "NULL" : App.register.name + ":" + App.register.id) + "<br>";
                html += "<string>Current Location: </strong>" + (App.register == null ? "NULL" : App.location.name + ":" + App.register.id) + "<br>";

                html += "<br>";

                html += "<h2>Stack Trace</h2>\n";
                html += "<pre>"+st.ToString()+"</pre>";

                return html;
            }
        }

        public string exceptionText
        {
            get
            {
                return Regex.Replace(exceptionHTML.Replace("<br>", "\n"), @"<[^>]+>|&nbsp;", "").Trim();
            }
        }

        public void sendReport(object parameter)
        {
            MailMessage mail = new MailMessage("andreaslekas@gmail.com", "andreaslekas+greekfestPOSExceptions@gmail.com");
            mail.Subject = "Exception: "+exception.Message;
            mail.Body = exceptionHTML;
            mail.IsBodyHtml = true;
            SmtpClient client = new SmtpClient();
            client.Host = "smtp.googlemail.com";
            client.Port = 587;
            client.UseDefaultCredentials = false;
            client.DeliveryMethod = SmtpDeliveryMethod.Network;
            client.EnableSsl = true;
            client.Credentials = new NetworkCredential("andreaslekas@gmail.com", "bootee-ascribe-skyjack-verdure");
            client.Send(mail);

            closeWindow();
        }

        public void cancel(object parameter)
        {
            closeWindow();
        }
    }

}
