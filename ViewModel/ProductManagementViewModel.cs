﻿using GreekFestPOS.Model;
using GreekFestPOS.POCO;
using GreekFestPOS.Windows.Admin;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace GreekFestPOS.ViewModel
{
    class ProductManagementViewModel : ViewModelBase
    {
        public IEnumerable<Product> loadAllProducts
        {
            get
            {
                ObservableCollection<Product> products = ProductDTO.loadAll();
                return products;
            }
        }

        public ICommand editProductCommand
        {
            get { return new DelegateCommand(editProduct); }
        }

        public ICommand addProductCommand
        {
            get { return new DelegateCommand(addProduct); }
        }

        private void editProduct(object product)
        {
            AddEditProductWindow addEditProductWindow = new AddEditProductWindow();
            var vm = addEditProductWindow.DataContext as AddEditProductViewModel;
            vm.product = (Product)product;
            addEditProductWindow.ShowDialog();
            RaisePropertyChangedEvent("loadAllProducts");
        }

        private void addProduct(object product)
        {
            AddEditProductWindow addEditProductWindow = new AddEditProductWindow();
            var vm = addEditProductWindow.DataContext as AddEditProductViewModel;
            vm.product = new Product();
            addEditProductWindow.ShowDialog();
            RaisePropertyChangedEvent("loadAllProducts");
        }

        public void deleteProduct(Product product)
        {
            ProductDTO.deleteProduct(product);
            RaisePropertyChangedEvent("loadAllProducts");
            product = null;
        }
    }
}
