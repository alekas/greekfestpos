﻿using GreekFestPOS.Model;
using GreekFestPOS.POCO;
using GreekFestPOS.Util;
using GreekFestPOS.Windows.Admin;
using GreekFestPOS.Windows.User;
using MagTekServiceObject;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading;
using System.Windows.Input;
using Microsoft.PointOfService;
using System.ComponentModel;
using System.Windows;

namespace GreekFestPOS.ViewModel
{
    class DatabaseHostnameViewModel : ViewModelBase
    {
        private string _hostname;
        private bool tested = false;
        public DatabaseHostnameViewModel()
        {
            hostname = (string)App.getConfig("database_hostname");
        }
        
        public ICommand saveCommand
        {
            get { return new DelegateCommand(save); }
        }

        public string hostname
        {
            get
            {
                return _hostname;
            }

            set
            {
                this._hostname = value;
                RaisePropertyChangedEvent("hostname");
            }
        }

        public ICommand testDatabaseCommand
        {
            get 
            {
                return new DelegateCommand(testDatabase) 
                {
                    
                }; 
            }
        }

        public void save(object parameter)
        {
            if (tested || Database.init(hostname))
            {
                ((LocalConfigurationItem<string>)App.configurationItems["database_hostname"]).value = _hostname;
                App.persistConfiguration();
                closeWindow();
            }
        }

        public void testDatabase(object parameter)
        {
            if(Database.init(hostname))
            {
                tested = true;
                MessageBox.Show("Connected to the database successfully!", "Success", MessageBoxButton.OK, MessageBoxImage.Information);
            }
        }

    }
}
