﻿using GreekFestPOS.Model;
using GreekFestPOS.POCO;
using GreekFestPOS.Windows.Admin;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Microsoft.PointOfService;

namespace GreekFestPOS.ViewModel
{
    class ServiceObjectBrowserViewModel : ViewModelBase
    {
        DeviceInfo _selectedItem;

        public DelegateCommand saveCommand { get { return new DelegateCommand(save); } }

        public List<DeviceInfo> loadServiceObjects
        {
            get
            {
                PosExplorer explorer = new PosExplorer();

                DeviceCollection devices = explorer.GetDevices(DeviceCompatibilities.CompatibilityLevel1);

                List<DeviceInfo> good_devices = new List<DeviceInfo>();
                foreach(DeviceInfo device in devices)
                {
                    if(device.LogicalNames.Length > 0)
                    {
                        good_devices.Add(device);
                    }
                }
                return good_devices;
            }
            
        }

        public DeviceInfo selectedItem {
            get 
            {
                return _selectedItem; 
            }
            set 
            {
                _selectedItem = value;
                RaisePropertyChangedEvent("selectedItem");
            }
        }

        public void save(object parameter)
        {
            closeWindow();
        }
    }
}
