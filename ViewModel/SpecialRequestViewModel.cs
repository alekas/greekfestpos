﻿using GreekFestPOS.Model;
using GreekFestPOS.POCO;
using GreekFestPOS.Windows.Admin;
using GreekFestPOS.Windows.User;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace GreekFestPOS.ViewModel
{
    class SpecialRequestViewModel : ViewModelBase
    {
        private Product _product;

        public SpecialRequestViewModel()
        {

        }

        public ICommand doneCommand
        {
            get { return new DelegateCommand(done); }
        }

        public ICommand addCommand
        {
            get { return new DelegateCommand(addSpecialRequest); }
        }

        public ICommand removeCommand
        {
            get { return new DelegateCommand(removeSpecialRequest); }
        }

        public Product product
        {
            get
            {
                return _product;
            }

            set
            {
                _product = value;
                RaisePropertyChangedEvent("product");
            }
        }

        public ObservableCollection<string> specialRequests
        {
            get
            {
                return new ObservableCollection<string>(product.specialRequests);
            }
        }


        public string selectedRequestType
        {
            get;
            set;
        }

        public string selectedRequestItem
        {
            get;
            set;
        }

        public string[] requestTypes
        {
            get
            {
                return new string[] { "Add", "Extra", "Lite", "No" };
            }
        }

        public string[] requestItems
        {
            get
            {
                return new string[] { "Lettuce", "Onion", "Sauce", "Tomato" };
            }
        }

        public void done(object parameter)
        {
            closeWindow();
        }

        public void addSpecialRequest(object parameter)
        {
            product.addSpecialRequest(selectedRequestType + " " + selectedRequestItem);
            RaisePropertyChangedEvent("specialRequests");
        }

        public void removeSpecialRequest(object parameter)
        {
            product.removeSpecialRequest((string)parameter);
            RaisePropertyChangedEvent("specialRequests");
        }
    }
}
