﻿using GreekFestPOS.Model;
using GreekFestPOS.POCO;
using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using System.Windows.Media.Imaging;

namespace GreekFestPOS.ViewModel
{
    class AddEditLocationViewModel : ViewModelBase
    {

        public Location location { get; set; }
        private IEnumerable<Product> products;

        public ICommand saveCommand
        {
            get { return new DelegateCommand(saveLocation); }
        }

        public void saveLocation(object parameter)
        {
            if (location.id != 0)
            {
                LocationDTO.saveLocation(location);
            }
            else
            {
                LocationDTO.createLocation(location);
            }

            LocationDTO.setProducts(location, products);

            closeWindow();
        }

        public IEnumerable<Product> loadProducts
        {
            get
            {
                products = ProductDTO.loadAllByLocation(location);
                return products;
            }
            
        }
    }

}
