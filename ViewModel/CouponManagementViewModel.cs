﻿using GreekFestPOS.Model;
using GreekFestPOS.POCO;
using GreekFestPOS.Windows.Admin;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace GreekFestPOS.ViewModel
{
    class CouponManagementViewModel : ViewModelBase
    {
        public IEnumerable<Coupon> loadAllCoupons
        {
            get
            {
                ObservableCollection<Coupon> coupons = CouponDTO.loadAll();
                return coupons;
            }
        }

        public ICommand editCouponCommand
        {
            get { return new DelegateCommand(editCoupon); }
        }

        public ICommand addCouponCommand
        {
            get { return new DelegateCommand(addCoupon); }
        }

        private void editCoupon(object coupon)
        {
            AddEditCouponWindow addEditCouponWindow = new AddEditCouponWindow();
            var vm = addEditCouponWindow.DataContext as AddEditCouponViewModel;
            vm.coupon = (Coupon)coupon;
            addEditCouponWindow.Show();
        }

        private void addCoupon(object coupon)
        {
            AddEditCouponWindow addEditCouponWindow = new AddEditCouponWindow();
            var vm = addEditCouponWindow.DataContext as AddEditCouponViewModel;
            vm.coupon = new Coupon();
            addEditCouponWindow.Show();
        }

        public void deleteCoupon(Coupon coupon)
        {
            CouponDTO.deleteCoupon(coupon);
            coupon = null;
        }
    }
}
