﻿using GreekFestPOS.Model;
using GreekFestPOS.POCO;
using GreekFestPOS.Util;
using GreekFestPOS.Windows.Admin;
using GreekFestPOS.Windows.User;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;

namespace GreekFestPOS.ViewModel
{
    class LoginViewModel : ViewModelBase
    {
        private User _selectedUser;
        private static User _currentUser;
        private string _pin;
        private string _errorMessage;

        public LoginViewModel()
        {
            Magstripe.setCardSwipedDelegate(loginWithCardNumber);
            Magstripe.enableDataEvent();
        }

        private void loginWithCardNumber(Microsoft.PointOfService.Msr msr)
        {
            User user = UserDTO.loadByCardNumber(Util.Util.TrackDataToString(msr.Track2Data));
            if(user != null)
            {
                Application.Current.Dispatcher.Invoke((Action)(() =>
                {
                    LoginViewModel._currentUser = user;

                    App.currentUser = user;
                    AuditTrail.action("LOGIN", "MSR");
                    App.openRegister();
                    closeWindow();
                }));
            }
            else
            {
                pin = "";
                AuditTrail.action("FAILED_LOGIN", "MSR: " + Util.Util.TrackDataToString(msr.Track2Data));
                errorMessage = "Unrecognized Card.";
                Magstripe.enableDataEvent();
            }
        }

        public IEnumerable<User> loadAllUsers
        {
            get
            {
                ObservableCollection<User> users = UserDTO.loadAll();
                return users;
            }
        }

        public User selectedUser
        {
            get { return _selectedUser; }
            set
            {
                _selectedUser = value;
                RaisePropertyChangedEvent("selectedUser");
            }
        }

        public string pin
        {
            get { return _pin; }
            set
            {
                _pin = value;
                RaisePropertyChangedEvent("pin");
            }
        }

        public string errorMessage
        {
            get { return _errorMessage; }
            set
            {
                _errorMessage = value;
                RaisePropertyChangedEvent("errorMessage");
            }
        }

        public static User currentUser
        {
            get { return LoginViewModel._currentUser; }
        }


        public ICommand loginCommand
        {
            get { return new DelegateCommand(login); }
        }

        private void login(object parameter)
        {
            if(selectedUser == null)
            {
                errorMessage = "Please select a user.";
            }
            else if (pin == null || pin.Trim().Length == 0)
            {
                errorMessage = "Please type in your pin.";
            }
            else
            {
                if (pin == selectedUser.pin)
                {
                    LoginViewModel._currentUser = selectedUser;
                    App.currentUser = selectedUser;

                    AuditTrail.action("LOGIN", "PIN");
                    App.openRegister();
                    //Might have logged out during open register process
                    if(App.currentUser != null)
                    {
                        closeWindow();
                    } else
                    {
                        //if we did logout, clear pin
                        pin = "";
                    }
                }
                else
                {
                    pin = "";
                    errorMessage = "Invalid pin.";
                    AuditTrail.action("FAILED_LOGIN", "PIN: " + pin);
                }
            }
        }
    }
}
