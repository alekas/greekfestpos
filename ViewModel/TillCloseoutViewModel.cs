﻿using GreekFestPOS.Model;
using GreekFestPOS.POCO;
using GreekFestPOS.Util;
using GreekFestPOS.Windows.Admin;
using GreekFestPOS.Windows.User;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace GreekFestPOS.ViewModel
{
    class TillCloseoutViewModel : ViewModelBase
    {
        private string _errorText;

        public TillCloseoutViewModel()
        {
            TillDTO.populateSales(App.till);
            Drawer.openDrawer();
        }

        public DelegateCommand closeoutTillCommand
        {
            get { return new DelegateCommand(closeTill); }
        }

        public Register register
        {
            get
            {
                return App.register;
            }
        }

        public User user
        {
            get
            {
                return App.currentUser;
            }
        }

        public Till till
        {
            get
            {
                return App.till;
            }
        }

        public string errorText
        {
            get
            {
                return _errorText;
            }

            set
            {
                _errorText = value;
                RaisePropertyChangedEvent("errorText");
            }
        }

        public void closeTill(object parameter)
        {
            till.end_time = DateTime.Now;
            till.status = TillStatus.CLOSED;

            Printer.printZTape();

            TillDTO.saveTill(till);

            App.till = null;
            closeWindow();
        }
    }
}
