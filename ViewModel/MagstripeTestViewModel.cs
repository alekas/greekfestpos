﻿using GreekFestPOS.Model;
using GreekFestPOS.POCO;
using GreekFestPOS.Util;
using GreekFestPOS.Windows.Admin;
using GreekFestPOS.Windows.User;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace GreekFestPOS.ViewModel
{
    class MagstripeTestViewModel : ViewModelBase
    {
        private string _data;

        public MagstripeTestViewModel()
        {
            Magstripe.enableDataEvent();
            Magstripe.setCardSwipedDelegate(cardSwiped);
        }

        private void cardSwiped(Microsoft.PointOfService.Msr msr)
        {
            data = Util.Util.TrackDataToString(msr.Track2Data);
            Magstripe.enableDataEvent();
        }
        
        public string data
        {
            get
            {
                return _data;
            }

            set
            {
                _data = value;
                RaisePropertyChangedEvent("data");
            }
        }
    }
}
