﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace GreekFestPOS
{
    class Database
    {
        protected static SqlConnection conn;

        public static bool init(string hostname)
        {
            if(conn==null || conn.State == System.Data.ConnectionState.Closed || conn.State == System.Data.ConnectionState.Broken)
            {
                try
                {
                    Database.conn = new SqlConnection("user id=greekfest;" +
                            "password=greekfest;" +
                            "data source=" + hostname + "; " +
                            "Integrated Security=False; " +
                            "database=GreekFestivalPOS; " +
                            "connect timeout=5;MultipleActiveResultSets=True");
                    Database.conn.Open();
                }
                catch (SqlException e)
                {
                    if (!DesignerProperties.GetIsInDesignMode(new DependencyObject()))
                    {
                        MessageBox.Show("A database exception has occurred: " + e.Message + "\n\n" + Database.conn.ConnectionString, "Database Exception", MessageBoxButton.OK, MessageBoxImage.Warning);
                    }
                    return false;
                }
            }

            return true;
        }

        public static bool init()
        {
            return init((string)App.getConfig("database_hostname"));
        }

        internal static SqlConnection getConn()
        {
            if(conn == null || conn.State != System.Data.ConnectionState.Open)
            {
                Database.init();
            }
            return conn;
        }

        public static bool connected
        {
            get { return conn != null && conn.State == System.Data.ConnectionState.Open; }
        }
    }
}
