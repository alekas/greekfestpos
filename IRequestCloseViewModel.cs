﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GreekFestPOS
{
    public interface IRequestCloseViewModel
    {
        event EventHandler RequestClose;
    }
}
