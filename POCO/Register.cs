﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GreekFestPOS.POCO
{
    public class Register
    {
        public Register() : this(0, new Location(), "") { }

        public Register(int id, Location location, string name)
        {
            this.id = id;
            this.location = location;
            this.name = name;
        }

        public string name
        {
            get;
            set;
        }

        public int id
        {
            get;
            set;
        }

        public Location location
        {
            get;
            set;
        }
    }
}
