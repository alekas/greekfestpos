﻿using GreekFestPOS.Model;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GreekFestPOS.POCO
{
    public class Location
    {
        private ObservableCollection<Product> _products;

        public Location() : this(0, "", "", true) { }

        public Location(int id, string account_number, string name, bool hasInventory)
        {
            this.id = id;
            this.account_number = account_number;
            this.name = name;
            this.hasInventory = hasInventory;
        }

        public string name
        {
            get;
            set;
        }

        public bool hasInventory
        {
            get;
            set;
        }

        public int id
        {
            get;
            set;
        }

        public string account_number
        {
            get;
            set;
        }

        public override string ToString()
        {
            return name;
        }

        public ObservableCollection<Product> products
        {
            get
            {
                if(_products == null)
                {
                    _products = ProductDTO.loadByLocation(this);
                }

                return _products;
            }
        }
    }
}
