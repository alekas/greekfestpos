﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GreekFestPOS.POCO
{

    public abstract class ConfigurationItem
    {
        public string name { get; set; }
        public ConfigurationItem(string name)
        {
            this.name = name;
        }
    }

    public abstract class ConfigurationItem<T> : ConfigurationItem, INotifyPropertyChanged
    {
        private T _value;

        public ConfigurationItem(string name) : base(name) { }

        public ConfigurationItem(string name, T value) : base(name)
        {
            this.value = value;
        }

        public T value
        {
            get
            {
                return _value;
            }

            set
            {
                _value = value;
                RaisePropertyChangedEvent("value");
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;
        protected void RaisePropertyChangedEvent(string propertyName)
        {
            var handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    }

    public class LocalConfigurationItem<T> : ConfigurationItem<T>
    {
        public LocalConfigurationItem(string name) : base(name) { }

        public LocalConfigurationItem(string name, T value) : base(name, value) { }
    }

    public class GlobalConfigurationItem<T> : ConfigurationItem<T>
    {
        public GlobalConfigurationItem(string name, string key) : base(name) 
        {
            this.key = key;
        }

        public GlobalConfigurationItem(string name, T value, string key) : base(name, value) 
        {
            this.key = key;
        }

        public string key { get; set; }
    }
}
