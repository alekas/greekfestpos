﻿using GreekFestPOS.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GreekFestPOS.POCO
{
    public class PriceModification
    {
        public PriceModification(ModificationType modificationType, string amount, DollarOrPercent dollarOrPercent, string reason)
        {
            this.modificationType = modificationType;
            this.amount = amount;
            this.dollarOrPercent = dollarOrPercent;
            this.reason = reason;
        }

        public PriceModification()
        {
            this.reason = "";
        }

        public string amount { get; set; }

        public DollarOrPercent dollarOrPercent { get; set; }

        public string reason { get; set; }

        public ModificationType modificationType { get; set; }

        public string description
        {
            get
            {
                string str = "";
                if(dollarOrPercent == DollarOrPercent.Dollar)
                {
                    str += decimal.Parse(amount).ToString("C");
                }
                else
                {
                    str += amount + "%";
                }
                if(modificationType==ModificationType.Discount)
                {
                    str += " Discount";
                }
                else
                {
                    str += " Upcharge";
                }

                return str;
            }
        }

        public decimal calculatePrice(decimal price)
        {
            decimal localAmount = 0;
            decimal afterAmount = price;
            if (decimal.TryParse(amount, out localAmount))
            {
                if (modificationType == ModificationType.Discount)
                {
                    if (dollarOrPercent == DollarOrPercent.Dollar)
                    {
                        afterAmount = price - localAmount;
                    }
                    else
                    {
                        afterAmount = price - (price * (localAmount / 100));
                    }
                }
                else
                {
                    if (dollarOrPercent == DollarOrPercent.Dollar)
                    {
                        afterAmount = price + localAmount;
                    }
                    else
                    {
                        afterAmount = price + (price * (localAmount / 100));
                    }
                }
            }

            return afterAmount;
        }
    }
}
