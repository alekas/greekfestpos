﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GreekFestPOS.POCO
{
    public class Product : INotifyPropertyChanged
    {
        public int _quantity = 1;
        public decimal _price = 0;
        private int? _group;

        private PriceModification _priceModification;

        private List<string> _specialRequests = new List<string>();

        public Product() : this(0, "", new byte[] { }, false, false , "") { }

        public Product(int id, string name, byte[] picture, bool printToKitchen, bool printDuplicate, string duplicatePrinter)
        {
            this.id = id;
            this.name = name;
            this.picture = picture;
            this.enabled = false;
            this.price = 0;
            this.printToKitchen = printToKitchen;
            this.printDuplicate = printDuplicate;
            this.duplicatePrinter = duplicatePrinter;
        }

        public Product(int id, string name) : this(id, name, new byte[] { }, false, false, "") { }

        public string name
        {
            get;
            set;
        }

        public bool printDuplicate
        {
            get;
            set;
        }

        public string duplicatePrinter
        {
            get;
            set;
        }

        public bool printToKitchen
        {
            get;
            set;
        }

        public int id
        {
            get;
            set;
        }

        public byte[] picture
        {
            get;
            set;
        }

        public bool enabled { get; set; }
        public decimal price
        {
            get { return _price; }
            set { _price = value; RaisePropertyChangedEvent("price"); RaisePropertyChangedEvent("adjustedPrice"); }
        }

        public List<string> specialRequests
        {
            get
            {
                return _specialRequests;
            }
            set
            {
                this._specialRequests = value;
            }
        }

        public void addSpecialRequest(string request)
        {
            foreach(string s in _specialRequests)
            {
                if(s.Equals(request))
                {
                    return;
                }
            }
            _specialRequests.Add(request);
            RaisePropertyChangedEvent("specialRequests");
        }

        public void removeSpecialRequest(string request)
        {
            _specialRequests.Remove(request);
            RaisePropertyChangedEvent("specialRequests");
        }

        public decimal adjustedPrice
        {
            get
            {
                if(priceModification != null)
                {
                    return priceModification.calculatePrice(price);
                }

                return price;
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;
        protected void RaisePropertyChangedEvent(string propertyName)
        {
            var handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        public bool isModified
        {
            get { return priceModification != null; }
        }

        public bool canModify()
        {
            return priceModification == null && hasCoupon == false;
        }

        public bool hasCoupon { get; set; }

        public bool isSelected { get; set; }


        private void modificationsChanged()
        {
            RaisePropertyChangedEvent("coupon");
            RaisePropertyChangedEvent("priceModification");
            RaisePropertyChangedEvent("isModified"); 
            RaisePropertyChangedEvent("adjustedPrice");
        }

        public PriceModification priceModification 
        { 
            get
            {
                return _priceModification;
            }

            set
            {
                _priceModification = value;
                modificationsChanged();
            }
        }

        public int? group 
        { 
            get 
            {
                return _group;
            } 
            set
            {
                _group = value;
                RaisePropertyChangedEvent("group");
            } 
        }
    }
}
