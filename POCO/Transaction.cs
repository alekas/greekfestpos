﻿using GreekFestPOS.Util;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GreekFestPOS.POCO
{
    public class Transaction
    {
        protected List<Product> _products = new List<Product>();
        protected List<Coupon> _coupons = new List<Coupon>();
        protected List<Payment> _payments = new List<Payment>();

        public Transaction(int id, Register register, User user, decimal total, DateTime created_timestamp, DateTime completed_timestamp, Status status)
            : this(register, user)
        {
            this.id = id;
            this.total = total;
            this.created_timestamp = created_timestamp;
            this.completed_timestamp = completed_timestamp;
            this.status = status;
        }

        public Transaction(Register register, User user)
        {
            this.register = register;
            this.user = user;
            this.status = Status.NEW;
        }

        public int id { get; set; }
        public decimal total { get; set; }
        public decimal subtotal 
        {
            get 
            {
                return calculatedTotal - transaction_fee;
            }
        }
        public decimal transaction_fee { get; set; }
        public Register register { get; set; }
        public User user { get; set; }
        public Status status { get; set; }
        public List<Payment> payments 
        {
            get
            {
                if(this._payments == null)
                {
                    this._payments = new List<Payment>();
                }

                return this._payments;
            }
        }

        public DateTime created_timestamp { get; set; }
        public DateTime completed_timestamp { get; set; }
        public IReadOnlyList<Product> products { get { return _products; } set { _products = value.ToList<Product>(); } }
        public IReadOnlyList<Coupon> coupons { get { return _coupons; } set { _coupons = value.ToList<Coupon>(); } }

        public Payment firstPayment
        {
            get 
            {
                if (this.payments.Count > 0)
                {
                    return this.payments[0];
                }
                else
                {
                    return null;
                }
            }
        }

        public decimal calculatedTotal
        { 
            get 
            {
                decimal total = 0;
                foreach (Product product in products)
                {
                    total += product.adjustedPrice;
                }
                foreach (Coupon coupon in coupons)
                {
                    total += coupon.amount;
                }

                total += transaction_fee;

                return total;
            }
        }

        public void addProduct(Product product)
        {
            _products.Add(product);
        }

        public void removeProduct(Product product)
        {
          
            _products.Remove(product);
            product = null;
        }

        public void addCoupon(Coupon coupon)
        {
            _coupons.Add(coupon);
        }

        public void removeCoupon(Coupon coupon)
        {
            _coupons.Remove(coupon);
            coupon = null;
        }

        internal Product getProduct(int id)
        {
            foreach (Product p in _products)
            {
                if (p.id == id)
                {
                    return p;
                }
            }

            return null;
        }

        internal Coupon getCoupon(int id)
        {
            foreach (Coupon c in _coupons)
            {
                if (c.id == id)
                {
                    return c;
                }
            }

            return null;
        }
    }
}
