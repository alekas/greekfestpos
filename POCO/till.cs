﻿using GreekFestPOS.Util;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GreekFestPOS.POCO
{
    public class Till
    {
        public Till(int id, Register register, User user, decimal amount, DateTime start_time, DateTime? end_time, TillStatus status, decimal? total_sales, decimal? total_credit_card_sales, decimal? total_cash_sales, decimal? total_coupons, decimal? total_coupon_sales)
            : this(register, user)
        {
            this.id = id;
            this.amount = amount;
            this.start_time = start_time;
            this.end_time = end_time;
            this.status = status;
            this.total_sales = total_sales;
            this.total_credit_card_sales = total_credit_card_sales;
            this.total_cash_sales = total_cash_sales;
            this.total_coupons = total_coupons;
            this.total_coupon_sales = total_coupon_sales;
        }

        public Till(Register register, User user)
        {
            this.register = register;
            this.user = user;
            this.status = TillStatus.OPEN;
        }

        public Till(Register register, User user, string amount, DateTime start_time)
        {
            this.register = register;
            this.user = user;
            this.amount = decimal.Parse(amount);
            this.start_time = start_time;
            this.status = TillStatus.OPEN;
        }

        public int id { get; set; }
        public Register register { get; set; }
        public User user { get; set; }
        public TillStatus status { get; set; }
        public decimal amount { get; set; }
        public DateTime start_time { get; set; }
        public DateTime? end_time { get; set; }

        public decimal? total_coupons { get; set; }
        public decimal? total_cash_sales { get; set; }
        public decimal? total_credit_card_sales { get; set; }
        public decimal? total_credit_card_fees { get; set; }
        public decimal? total_sales { get; set; }


        public decimal? total_coupon_sales { get; set; }
    }
}
