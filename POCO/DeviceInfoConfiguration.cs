﻿using Microsoft.PointOfService;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GreekFestPOS.POCO
{
    class DeviceInfoConfiguration
    {
        public DeviceInfoConfiguration(DeviceInfo device)
        {
            this.device = device;
        }

        public override string ToString()
        {
            if(device==null)
            {
                return null;
            }
            return device.LogicalNames[0];
        }

        public DeviceInfo device { get; set; }
    }
}
