﻿using Clifton.Payment;
using GreekFestPOS.Util;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static Clifton.Payment.Gateway.PayeezyGateway;

namespace GreekFestPOS.POCO
{
    public class Payment
    {
        public Payment(int id, Transaction transaction, DateTime timestamp, PaymentType payment_type, decimal amount_tendered, string paypal_pnref, int paypal_result, string paypal_authcode)
        {
            this.paymentProcessor = "paypal";
            this.id = id;
            this.transaction = transaction;
            this.timestamp = timestamp;
            this.paymentType = payment_type;
            this.amountTendered = amount_tendered;
            this.paypalPNRef = paypal_pnref;
            this.paypalResult = paypal_result;
            this.paypalAuthCode = paypal_authcode;
        }

        public Payment(int id, Transaction transaction, DateTime timestamp, PaymentType payment_type, decimal amount_tendered, CreditCardType cardType, Response response)
        {
            this.paymentProcessor = "payeezy";
            this.id = id;
            this.transaction = transaction;
            this.timestamp = timestamp;
            this.paymentType = payment_type;
            this.amountTendered = amount_tendered;


            this.creditCardType = cardType.ToString();
            this.payeezyTransactionStatus = response.TransactionStatus;
            this.payeezyTransactionId = response.TransactionId;
            this.payeezyTransactionType = response.TransactionType;
            this.payeezyTransactionTag = response.TransactionTag;
            this.payeezyBankResponseCode = response.BankResponseCode;
            this.payeezyBankMessage = response.BankMessage;
            this.payeezyGatewayResponseCode = response.GatewayResponseCode;
            this.payeezyGatewayMessage = response.GatewayMessage;
            this.payeezyCorrelationId = response.CorrelationId;

        }

        public Payment(int id, Transaction transaction, DateTime timestamp, PaymentType payment_type, decimal amount_tendered)
        {
            this.id = id;
            this.transaction = transaction;
            this.timestamp = timestamp;
            this.paymentType = payment_type;
            this.amountTendered = amount_tendered;
        }

        public int id { get; set; }
        public string paymentProcessor { get; set; }
        public Transaction transaction { get; set; }
        public DateTime timestamp { get; set; }
        public PaymentType paymentType { get; set; }
        public decimal amountTendered { get; set; }

        public string paypalPNRef { get; set; }
        public int paypalResult { get; set; }
        public string paypalAuthCode { get; set; }

        public string creditCardType { get; set; }
        public string payeezyTransactionStatus { get; set; }
        public string payeezyTransactionId { get; set; }
        public string payeezyTransactionType { get; set; }
        public string payeezyTransactionTag { get; set; }
        public string payeezyBankResponseCode { get; set; }
        public string payeezyBankMessage { get; set; }
        public string payeezyGatewayResponseCode { get; set; }
        public string payeezyGatewayMessage { get; set; }
        public string payeezyCorrelationId { get; set; }

        public string creditCardNumber { get; set; }
        public string expirationDate { get; set; }
        public bool postProcessingRequired { get; set; }
    }
}
