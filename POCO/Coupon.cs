﻿using GreekFestPOS.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GreekFestPOS.POCO
{
    public class Coupon
    {
        private List<Product> _products = new List<Product>();
        private List<Product> _eligibleProducts;

        public Coupon() : this(0, "", "") { }

        public Coupon(int id, string name, string code)
        {
            this.id = id;
            this.name = name;
            this.code = code;
        }

        public string name
        {
            get;
            set;
        }

        public decimal amount
        {
            get 
            {
                decimal amount = 0;
                foreach(Product p in products)
                {
                    amount -= p.adjustedPrice;
                }

                return amount;
            }
        }

        public string code
        {
            get;
            set;
        }

        public string nameAndCode
        {
            get 
            {
                return name + " - " + code.ToUpper();
            }
        }

        public int id
        {
            get;
            set;
        }

        public List<Product> products
        {
            get
            {
                return _products;
            }
            set
            {
                _products = value;
            }
        }

        public List<Product> eligibleProducts
        {
            get
            {
                if(_eligibleProducts == null)
                {
                    _eligibleProducts = new List<Product>(ProductDTO.loadByCoupon(this));
                }

                return _eligibleProducts;
            }
            set
            {
                _eligibleProducts = value;
            }
        }

        public override string ToString()
        {
            return name;
        }

        public int getGroupCount()
        {
            int groups = 0;
            foreach (Product coupon_product in eligibleProducts)
            {
                if (coupon_product.group > groups)
                {
                    groups = (int)coupon_product.group;
                }
            }

            return groups;
        }

        public void tryAdd(Transaction transaction)
        {
            //figure out how many groups are in the coupon
            int groups = getGroupCount();

            for (int i = 0; i <= groups; i++)
            {
                Product min_product = null;
                foreach (Product eligible_product in eligibleProducts)
                {
                    foreach (Product product in transaction.products)
                    {
                        bool found_existing_group = false;
                        foreach (Product coupon_product in products)
                        {
                            if (coupon_product.group == i)
                            {
                                found_existing_group = true;
                                break;
                            }
                        }

                        if (!found_existing_group && eligible_product.group == i)
                        {
                            if (product.id == eligible_product.id)
                            {
                                if (product.canModify() && !product.hasCoupon && (min_product == null || (product.price < min_product.price)))
                                {
                                    if (min_product != null)
                                    {
                                        min_product.group = null;
                                    }
                                    min_product = product;
                                    min_product.group = i;
                                }
                            }
                        }
                    }
                }


                if (min_product != null)
                {
                    min_product.hasCoupon = true;
                    this.products.Add(min_product);
                }
            }

        }

        public bool tryRemove(Product product)
        {
            foreach(Product p in products)
            {
                if(product.id == p.id && p.hasCoupon && product.hasCoupon)
                {
                    _products.Remove(p);
                    return true;
                }
            }

            return false;
        }
    }
}
