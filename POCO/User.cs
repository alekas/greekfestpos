﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GreekFestPOS.POCO
{
    public class User
    {
        public User(int id, string name, string pin, string card_number, bool isAdmin)
        {
            this.id = id;
            this.name = name;
            this.pin = pin;
            this.card_number = card_number;
            this.isAdmin = isAdmin;
        }

        public User(int id, string name)
        {
            this.id = id;
            this.name = name;
        }

        public string name
        {
            get;
            set;
        }

        public int id
        {
            get;
            set;
        }

        public string pin
        {
            get;
            set;
        }

        public string card_number
        {
            get;
            set;
        }

        public bool isAdmin
        {
            get;
            set;
        }
    }
}
