﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace GreekFestPOS.UserControls
{
    /// <summary>
    /// Interaction logic for Keypad.xaml
    /// </summary>
    public partial class Keypad : UserControl
    {
        private bool written = false;

        public Keypad()
        {
            InitializeComponent();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            if(FormatForMoney)
            {
                if(Value == null)
                {
                    Value = "";
                }
            }
            if (written || !OverrideDefault)
            {
                string content = (String)((Button)sender).Content;
                if (content.Contains("+"))
                {
                    if(Value == "")
                    {
                        Value = "0.00";
                    }
                    Value = (Double.Parse(Value) + Double.Parse(content.Substring(1))).ToString();
                    if(Value.IndexOf(".") == -1)
                    {
                        Value += ".00";
                    } else if(Value.Substring(Value.IndexOf(".")+1).Length < 2)
                    {
                        Value += "0";
                    }
                }
                else
                {
                    Value += ((Button)sender).Content;
                }
                if(Value.Substring(0, 1)=="0")
                {
                    Value = Value.Substring(1);
                }
            }
            else if(OverrideDefault)
            {
                Value = (String)((Button)sender).Content;
            }
            addDecimalPoint();
            written = true;
        }

        private void Decimal_Click(object sender, RoutedEventArgs e)
        {
            if (Value.IndexOf('.') == -1)
            {
                if (written || !OverrideDefault)
                {
                    Value += ((Button)sender).Content;
                }
                else if (OverrideDefault)
                {
                    Value = (String)((Button)sender).Content;
                }
            }
        }

        private void Backspace_Click(object sender, RoutedEventArgs e)
        {
            if (Value.Length > 0)
            {
                Value = Value.Substring(0, Value.Length - 1);
                if(Value.IndexOf('.') != -1 && Value.Substring(Value.Length - 1)==".")
                {
                    Value = Value.Replace(".", "");
                }

                addDecimalPoint();
            }
        }

        private void addDecimalPoint()
        {
            if (FormatForMoney)
            {
                Value = Value.Replace(".", "");
                if(Value.Replace(".", "").Length < 3)
                {
                    Value = Value.PadLeft(3, '0');
                }
                if (Value.IndexOf('.') == -1)
                {
                    Value = Value.Substring(0, Value.Length - 2) + "." + Value.Substring(Value.Length - 2);
                }
            }
        }

        public string Value
        {
            get 
            {
                string value = (string)GetValue(ValueProperty);
                return value; 
            }
            set { SetValue(ValueProperty, value); }
        }

        public bool ShowTextBox
        {
            get { return (bool)GetValue(ShowTextBoxProperty); }
            set { SetValue(ShowTextBoxProperty, value); }
        }

        public bool FormatForMoney
        {
            get { return (bool)GetValue(FormatForMoneyProperty); }
            set { SetValue(FormatForMoneyProperty, value); }
        }

        public bool OverrideDefault
        {
            get { return (bool)GetValue(OverrideDefaultProperty); }
            set { SetValue(OverrideDefaultProperty, value); }
        }

        private static void valueChangedCallback(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            //throw new NotImplementedException();
        }

        public string CurrencyColumnWidth
        {
            get { return FormatForMoney ? "" : "0"; }
        }

        public static readonly DependencyProperty ValueProperty = DependencyProperty.Register("Value", typeof(string), typeof(Keypad), new FrameworkPropertyMetadata("", FrameworkPropertyMetadataOptions.BindsTwoWayByDefault) { PropertyChangedCallback = valueChangedCallback });
        public static readonly DependencyProperty FormatForMoneyProperty = DependencyProperty.Register("FormatForMoney", typeof(bool), typeof(Keypad), new FrameworkPropertyMetadata(false, FrameworkPropertyMetadataOptions.BindsTwoWayByDefault) { PropertyChangedCallback = valueChangedCallback });
        public static readonly DependencyProperty ShowTextBoxProperty = DependencyProperty.Register("ShowTextBox", typeof(bool), typeof(Keypad), new FrameworkPropertyMetadata(false, FrameworkPropertyMetadataOptions.BindsTwoWayByDefault));
        public static readonly DependencyProperty OverrideDefaultProperty = DependencyProperty.Register("OverrideDefault", typeof(bool), typeof(Keypad), new FrameworkPropertyMetadata(false, FrameworkPropertyMetadataOptions.BindsTwoWayByDefault));
    }
}
