﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace GreekFestPOS.Windows
{
    /// <summary>
    /// Interaction logic for KeypadModalWindow.xaml
    /// </summary>
    public partial class KeypadModalWindow : WindowBase
    {
        protected bool _formatForMoney = false;

        public KeypadModalWindow()
        {
            InitializeComponent();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        public string Value
        {
            get { return Keypad.Value; }
            set { Keypad.Value = value; }
        }

        public bool formatForMoney
        {
            get { return _formatForMoney; }
            set 
            { 
                _formatForMoney = value;
                Keypad.FormatForMoney = value;
            }
        }

        public bool cancelled 
        {
            get
            {
                return Value == null || Value.Equals("");
            }
        }
    }
}
