﻿using GreekFestPOS.POCO;
using GreekFestPOS.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace GreekFestPOS.Windows.Admin
{
    /// <summary>
    /// Interaction logic for CouponManagementWindow.xaml
    /// </summary>
    public partial class CouponManagementWindow : WindowBase
    {
        public CouponManagementWindow()
        {
            InitializeComponent();
        }

        private CouponManagementViewModel ViewModel
        {
            get { return DataContext as CouponManagementViewModel; }
        }

        private bool HasViewModel { get { return ViewModel != null; } }


        public ICommand deleteCouponConfirmationCommand
        {
            get { return new DelegateCommand(confirmDeleteProduct); }
        }

        public void confirmDeleteProduct(object parameter)
        {
            if (MessageBox.Show("Are you sure you want to delete this coupon?", "Are you sure?", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
            {
                if (parameter is Coupon)
                {
                    ViewModel.deleteCoupon((Coupon)parameter);
                }
            }
        }
    }
}
