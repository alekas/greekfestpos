﻿using GreekFestPOS.POCO;
using GreekFestPOS.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace GreekFestPOS.Windows.Admin
{
    /// <summary>
    /// Interaction logic for RegisterManagementWindow.xaml
    /// </summary>
    public partial class RegisterManagementWindow : Window
    {
        public RegisterManagementWindow()
        {
            InitializeComponent();
        }

        private RegisterManagementViewModel ViewModel
        {
            get { return DataContext as RegisterManagementViewModel; }
        }

        private bool HasViewModel { get { return ViewModel != null; } }


        public ICommand deleteRegisterConfirmationCommand
        {
            get { return new DelegateCommand(confirmDeleteRegister); }
        }

        public void confirmDeleteRegister(object parameter)
        {
            if(MessageBox.Show("Are you sure you want to delete this register?", "Are you sure?", MessageBoxButton.YesNo, MessageBoxImage.Question)==MessageBoxResult.Yes)
            {
                if (parameter is Register)
                {
                    ViewModel.deleteRegister((Register)parameter);
                }
            }
        }
    }
}
