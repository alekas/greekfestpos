﻿using GreekFestPOS.POCO;
using GreekFestPOS.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace GreekFestPOS.Windows.Admin
{
    /// <summary>
    /// Interaction logic for TransactionManagementWindow.xaml
    /// </summary>
    public partial class TransactionManagementWindow : WindowBase
    {
        public TransactionManagementWindow()
        {
            InitializeComponent();
        }

        private void DataGrid_PreviewMouseWheel(object sender, MouseWheelEventArgs args)
        {
            transactionScrollViewer.ScrollToVerticalOffset(transactionScrollViewer.ContentVerticalOffset - args.Delta);
            args.Handled = true;
        }
    }
}
