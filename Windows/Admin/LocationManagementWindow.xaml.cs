﻿using GreekFestPOS.POCO;
using GreekFestPOS.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace GreekFestPOS.Windows.Admin
{
    /// <summary>
    /// Interaction logic for ProductManagementWindow.xaml
    /// </summary>
    public partial class LocationManagementWindow : Window
    {
        public LocationManagementWindow()
        {
            InitializeComponent();
        }

        private LocationManagementViewModel ViewModel
        {
            get { return DataContext as LocationManagementViewModel; }
        }

        private bool HasViewModel { get { return ViewModel != null; } }


        public ICommand deleteLocationConfirmationCommand
        {
            get { return new DelegateCommand(confirmDeleteLocation); }
        }

        public void confirmDeleteLocation(object parameter)
        {
            if(MessageBox.Show("Are you sure you want to delete this location?", "Are you sure?", MessageBoxButton.YesNo, MessageBoxImage.Question)==MessageBoxResult.Yes)
            {
                if (parameter is Location)
                {
                    ViewModel.deleteLocation((Location)parameter);
                }
            }
        }
    }
}
