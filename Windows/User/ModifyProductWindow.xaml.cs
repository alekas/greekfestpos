﻿using GreekFestPOS.Util;
using GreekFestPOS.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace GreekFestPOS.Windows.User
{
    /// <summary>
    /// Interaction logic for ModifyProductWindow.xaml
    /// </summary>
    public partial class ModifyProductWindow : WindowBase
    {
        public ModifyProductWindow()
        {
            InitializeComponent();
        }

        private void WindowBase_Closed(object sender, EventArgs e)
        {

        }
    }
}
