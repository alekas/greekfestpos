﻿using PayPal.Payments.Common;
using PayPal.Payments.Common.Utility;
using PayPal.Payments.DataObjects;
using PayPal.Payments.Transactions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace GreekFestPOS.Windows.User
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : WindowBase
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        protected override void OnClosed(EventArgs e)
        {
            base.OnClosed(e);

            Application.Current.Shutdown();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            // Create the Data Objects.
            // Create the User data object with the required user details.
            UserInfo User = new UserInfo("HolyTrinity", "HolyTrinity", "PAYPAL", "GreekFest123$");

            // Create the Payflow  Connection data object with the required connection details.
            // The PAYFLOW_HOST property is defined in the App config file.
            PayflowConnectionData Connection = new PayflowConnectionData();
            

            // Create a new Invoice data object with the Amount, Billing Address etc. details.
            Invoice Inv = new Invoice();

            // Set Amount.
            Currency Amt = new Currency(new decimal(25.25));
            Inv.Amt = Amt;
            Inv.PoNum = "PO12345";
            Inv.InvNum = "INV12345";
            Inv.Comment1 = "Swipe Example";

            // Create a new Payment Device - Swipe data object.  The input parameter is Swipe Data.
            // Used to pass the Track 1 or Track 2 data (the card’s magnetic stripe information) for card-present
            // transactions. Include either Track 1 or Track 2 data—not both. If Track 1 is physically damaged,
            // the POS application can send Track 2 data instead.

            // The parameter data for the SwipeCard object is usually obtained with a card reader.
            // NOTE: The SWIPE parameter is not supported on accounts where PayPal is the Processor.
            SwipeCard Swipe = new SwipeCard(";5105105105105100=15121011000012345678?");
            // Create a new Tender - Swipe Tender data object.
            CardTender Card = new CardTender(Swipe);

            // Create a new Sale Transaction using Swipe data.
            SaleTransaction Trans = new SaleTransaction(User, Connection, Inv, Card, PayflowUtility.RequestId);
            

            // Submit the Transaction
            Response Resp = Trans.SubmitTransaction();

            // Display the transaction response parameters.
            if (Resp != null)
            {
                // Get the Transaction Response parameters.
                TransactionResponse TrxnResponse = Resp.TransactionResponse;

                if (TrxnResponse != null)
                {
                    Console.WriteLine("RESULT = " + TrxnResponse.Result);
                    Console.WriteLine("PNREF = " + TrxnResponse.Pnref);
                    Console.WriteLine("RESPMSG = " + TrxnResponse.RespMsg);
                    Console.WriteLine("AUTHCODE = " + TrxnResponse.AuthCode);
                    // If value is true, then the Request ID has not been changed and the original response
                    // of the original transction is returned. 
                    Console.WriteLine("DUPLICATE = " + TrxnResponse.Duplicate);
                }

                // Display the response.
                Console.WriteLine(Environment.NewLine + PayflowUtility.GetStatus(Resp));

                // Get the Transaction Context and check for any contained SDK specific errors (optional code).
                Context TransCtx = Resp.TransactionContext;
                if (TransCtx != null && TransCtx.getErrorCount() > 0)
                {
                    Console.WriteLine(Environment.NewLine + "Transaction Errors = " + TransCtx.ToString());
                }
            }
            Console.WriteLine("Press Enter to Exit ...");
            Console.ReadLine();
        }

        private void Expander_Expanded(object sender, RoutedEventArgs e)
        {
            ExpandExculsively(sender as Expander);
        }

        private void ExpandExculsively(Expander expander)
        {
            for(int i = 0; i < transactionListItems.Items.Count; i++)
            {
                ContentPresenter cp = (ContentPresenter)transactionListItems.ItemContainerGenerator.ContainerFromItem(transactionListItems.Items[i]);
                Expander child = FindVisualChild<Expander>(cp);

                if (child is Expander && child != expander)
                    ((Expander)child).IsExpanded = false;
            }
        }

        public static T FindVisualChild<T>(DependencyObject depObj) where T : DependencyObject
        {
            if (depObj != null)
            {
                for (int i = 0; i < VisualTreeHelper.GetChildrenCount(depObj); i++)
                {
                    DependencyObject child = VisualTreeHelper.GetChild(depObj, i);
                    if (child != null && child is T)
                    {
                        return (T)child;
                    }

                    T childItem = FindVisualChild<T>(child);
                    if (childItem != null) return childItem;
                }
            }
            return null;
        }
    }
}
