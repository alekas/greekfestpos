﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace GreekFestPOS
{
    public class DelegateCommand : ICommand
    {
        private readonly Action<object> _action;

        private Func<bool> _canExecute = () => {return true;};

        public DelegateCommand(Action<object> action)
        {
            _action = action;
        }

        public DelegateCommand(Action<object> action, Func<bool> canExecute)
        {
            _action = action;
            _canExecute = canExecute;
        }

        public void Execute(object parameter)
        {
            _action(parameter);
        }

        public bool CanExecute(object parameter)
        {
            return _canExecute();

        }

        public event EventHandler CanExecuteChanged
        {
            add { CommandManager.RequerySuggested += value; }
            remove { CommandManager.RequerySuggested -= value; }
        }

        public void RaiseCanExecuteChanged()
        {
            CommandManager.InvalidateRequerySuggested();
            //            var handler = CanExecuteChanged;
            //            if (handler != null)
            //            {
            //                handler(this, EventArgs.Empty);
            //            }
        }
    }
}
