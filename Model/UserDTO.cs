﻿using GreekFestPOS.POCO;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GreekFestPOS.Model
{
    class UserDTO
    {
        private static ObservableCollection<User> users = new ObservableCollection<User>();

        public static ObservableCollection<User> loadAll()
        {
            users.Clear();
            try
            {
                using (var command = Database.getConn().CreateCommand())
                {
                    command.CommandText = "SELECT id, name, pin, card_number, isAdmin FROM users ORDER BY name";
                    using (var reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            string card_number = null;
                            if (reader["card_number"] != DBNull.Value)
                            {
                                card_number = reader.GetString(reader.GetOrdinal("card_number"));
                            }
                            users.Add(new User
                            (
                                reader.GetInt32(reader.GetOrdinal("id")), 
                                reader.GetString(reader.GetOrdinal("name")),
                                reader.GetString(reader.GetOrdinal("pin")),
                                card_number, 
                                reader.GetBoolean(reader.GetOrdinal("isAdmin"))
                            ));
                        }
                    }
                }
            }
            catch (SqlException e)
            {
                Console.WriteLine(e.StackTrace);
            }
            return users;
        }

        public static bool saveUser(User user)
        {
            try
            {
                using(var command = Database.getConn().CreateCommand())
                {
                    command.CommandText = @"UPDATE users SET name=@name, pin=@pin, card_number=@card_number WHERE id=@id";
                    command.Parameters.AddWithValue("@name", user.name);
                    command.Parameters.AddWithValue("@pin", user.pin);
                    command.Parameters.AddWithValue("@card_number", user.card_number);
                    command.Parameters.AddWithValue("@id", user.id);

                    command.ExecuteNonQuery();
                    return true;
                }
            }
            catch(SqlException e)
            {
                Console.WriteLine(e.StackTrace);
                return false;
            }
        }

        public static bool createUser(User user)
        {
            try
            {
                using(var command = Database.getConn().CreateCommand())
                {
                    command.CommandText = @"INSERT INTO users (name, pin, card_number) VALUES (@name, @pin, @card_number)";
                    command.Parameters.AddWithValue("@name", user.name);
                    command.Parameters.AddWithValue("@card_number", user.card_number);
                    command.Parameters.AddWithValue("@pin", user.pin);

                    command.ExecuteNonQuery();
                    return true;
                }
            }
            catch(SqlException e)
            {
                Console.WriteLine(e.StackTrace);
                return false;
            }
        }

        public static bool deleteUser(User user)
        {
            try
            {
                using(var command = Database.getConn().CreateCommand())
                {
                    command.CommandText = @"DELETE FROM users WHERE id=@id";
                    command.Parameters.AddWithValue("@id", user.id);

                    command.ExecuteNonQuery();
                    return true;
                }
            }
            catch(SqlException e)
            {
                Console.WriteLine(e.StackTrace);
                return false;
            }
        }

        internal static User loadById(int users_id)
        {
            try
            {
                using (var command = Database.getConn().CreateCommand())
                {
                    command.CommandText = "SELECT id, name, pin, card_number, isAdmin FROM users WHERE id = @users_id";
                    command.Parameters.AddWithValue("@users_id", users_id);

                    using (var reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            string card_number = null;
                            if (reader["card_number"] != DBNull.Value)
                            {
                                card_number = reader.GetString(reader.GetOrdinal("card_number"));
                            }
                            return new User
                            (
                                reader.GetInt32(reader.GetOrdinal("id")),
                                reader.GetString(reader.GetOrdinal("name")),
                                reader.GetString(reader.GetOrdinal("pin")),
                                card_number,
                                reader.GetBoolean(reader.GetOrdinal("isAdmin"))
                            );
                        }
                    }
                }
            }
            catch (SqlException e)
            {
                Console.WriteLine(e.StackTrace);
            }
            return null;
        }

        internal static User loadByCardNumber(string card_number)
        {
            try
            {
                using (var command = Database.getConn().CreateCommand())
                {
                    command.CommandText = "SELECT id, name, pin, card_number, isAdmin FROM users WHERE card_number = @card_number";
                    command.Parameters.AddWithValue("@card_number", card_number);

                    using (var reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            return new User
                            (
                                reader.GetInt32(reader.GetOrdinal("id")),
                                reader.GetString(reader.GetOrdinal("name")),
                                reader.GetString(reader.GetOrdinal("pin")),
                                reader.GetString(reader.GetOrdinal("card_number")),
                                reader.GetBoolean(reader.GetOrdinal("isAdmin"))
                            );
                        }
                    }
                }
            }
            catch (SqlException e)
            {
                Console.WriteLine(e.StackTrace);
            }
            return null;
        }
    }
}
