﻿using GreekFestPOS.POCO;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GreekFestPOS.Model
{
    class ProductDTO
    {
        private static ObservableCollection<Product> products = new ObservableCollection<Product>();

        public static ObservableCollection<Product> loadAll()
        {
            products.Clear();
            try
            {
                using (var command = Database.getConn().CreateCommand())
                {
                    command.CommandText = "SELECT products.id, name, pictures.data, print_to_kitchen, print_duplicate, ISNULL(duplicate_printer, '') AS duplicate_printer FROM products LEFT JOIN pictures ON products.pictures_id = pictures.id WHERE deleted = 0 ORDER BY name";
                    using (var reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            products.Add(new Product
                            (
                                reader.GetInt32(reader.GetOrdinal("id")),
                                reader.GetString(reader.GetOrdinal("name")),
                                (byte[])reader["data"],
                                reader.GetBoolean(reader.GetOrdinal("print_to_kitchen")),
                                reader.GetBoolean(reader.GetOrdinal("print_duplicate")),
                                reader.GetString(reader.GetOrdinal("duplicate_printer"))

                            ));
                        }
                    }
                }
            }
            catch (SqlException e)
            {
                Console.Write(e.Message);
            }
            return products;
        }

        public static ObservableCollection<Product> loadAllByLocation(Location location)
        {
            ObservableCollection<Product> products = new ObservableCollection<Product>();
            try
            {
                using (var command = Database.getConn().CreateCommand())
                {
                    command.CommandText =
                    @"SELECT 
                        products.id, 
                        name, 
                        pictures.data,
                        price,
                        print_to_kitchen,
                        print_duplicate,
                        ISNULL(duplicate_printer, '') AS duplicate_printer,
                        CASE WHEN price IS NULL THEN 0 ELSE 1 END AS enabled
                    FROM 
                        products 
                    LEFT JOIN pictures ON products.pictures_id = pictures.id 
                    LEFT JOIN products_to_locations p2l ON products.id = p2l.products_id AND p2l.locations_id = @locations_id
                    WHERE products.deleted = 0
                    ORDER BY CASE WHEN price IS NULL THEN 1 ELSE 0 END, name";
                    command.Parameters.AddWithValue("@locations_id", location.id);

                    using (var reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            Product product = new Product
                            (
                                reader.GetInt32(reader.GetOrdinal("id")),
                                reader.GetString(reader.GetOrdinal("name")),
                                (byte[])reader["data"],
                                reader.GetBoolean(reader.GetOrdinal("print_to_kitchen")),
                                reader.GetBoolean(reader.GetOrdinal("print_duplicate")),
                                reader.GetString(reader.GetOrdinal("duplicate_printer"))
                            );
                            if (reader.IsDBNull(reader.GetOrdinal("price")))
                            {
                                product.price = 0;
                            }
                            else
                            {
                                product.price = reader.GetDecimal(reader.GetOrdinal("price"));
                            }

                            product.enabled = reader.GetInt32(reader.GetOrdinal("enabled")) == 1 ? true : false;
                            products.Add(product);
                        }
                    }
                }
            }
            catch (SqlException e)
            {
                Console.Write(e.Message);
            }
            return products;
        }

        public static ObservableCollection<Product> loadAllByCoupon(Coupon coupon)
        {
            ObservableCollection<Product> products = new ObservableCollection<Product>();
            try
            {
                using (var command = Database.getConn().CreateCommand())
                {
                    command.CommandText =
                    @"SELECT 
                        products.id, 
                        name, 
                        print_to_kitchen,
                        print_duplicate,
                        ISNULL(duplicate_printer, '') AS duplicate_printer,
                        pictures.data,
                        ISNULL([group], 0) AS [group],
                        CASE WHEN [group] IS NULL THEN 0 ELSE 1 END AS enabled
                    FROM 
                        products 
                    LEFT JOIN pictures ON products.pictures_id = pictures.id 
                    LEFT JOIN products_to_coupons p2c ON products.id = p2c.products_id AND p2c.coupons_id = @coupons_id
                    WHERE products.deleted = 0
                    ORDER BY CASE WHEN [group] IS NULL THEN 1 ELSE 0 END, [group], name";
                    command.Parameters.AddWithValue("@coupons_id", coupon.id);

                    using (var reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            Product product = new Product
                            (
                                reader.GetInt32(reader.GetOrdinal("id")),
                                reader.GetString(reader.GetOrdinal("name")),
                                (byte[])reader["data"],
                                reader.GetBoolean(reader.GetOrdinal("print_to_kitchen")),
                                reader.GetBoolean(reader.GetOrdinal("print_duplicate")),
                                reader.GetString(reader.GetOrdinal("duplicate_printer"))
                            );

                            product.enabled = reader.GetInt32(reader.GetOrdinal("enabled")) == 1 ? true : false;
                            product.group = reader.GetInt32(reader.GetOrdinal("group"));
                            products.Add(product);
                        }
                    }
                }
            }
            catch (SqlException e)
            {
                Console.Write(e.Message);
            }
            return products;
        }

        public static ObservableCollection<Product> loadByLocation(Location location)
        {
            ObservableCollection<Product> products = new ObservableCollection<Product>();
            try
            {
                using (var command = Database.getConn().CreateCommand())
                {
                    command.CommandText =
                    @"SELECT 
                        products.id, 
                        name, 
                        print_to_kitchen,
                        print_duplicate,
                        ISNULL(duplicate_printer, '') AS duplicate_printer,
                        pictures.data,
                        price,
                        CASE WHEN price IS NULL THEN 0 ELSE 1 END AS enabled
                    FROM 
                        products 
                    LEFT JOIN pictures ON products.pictures_id = pictures.id 
                    LEFT JOIN products_to_locations p2l ON products.id = p2l.products_id
                    WHERE p2l.locations_id = @locations_id AND products.deleted = 0
                    ORDER BY CASE WHEN price IS NULL THEN 1 ELSE 0 END, name";
                    command.Parameters.AddWithValue("@locations_id", location.id);

                    using (var reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            string duplicatePrinter = "";
                            if(reader.IsDBNull(reader.GetOrdinal("duplicate_printer")))
                            {
                                duplicatePrinter = "";
                            }
                            else
                            {
                                duplicatePrinter = reader.GetString(reader.GetOrdinal("duplicate_printer"));
                            }

                            Product product = new Product
                            (
                                reader.GetInt32(reader.GetOrdinal("id")),
                                reader.GetString(reader.GetOrdinal("name")),
                                (byte[])reader["data"],
                                reader.GetBoolean(reader.GetOrdinal("print_to_kitchen")),
                                reader.GetBoolean(reader.GetOrdinal("print_duplicate")),
                                duplicatePrinter
                            );
                            if (reader.IsDBNull(reader.GetOrdinal("price")))
                            {
                                product.price = 0;
                            }
                            else
                            {
                                product.price = reader.GetDecimal(reader.GetOrdinal("price"));
                            }

                            product.enabled = reader.GetInt32(reader.GetOrdinal("enabled")) == 1 ? true : false;
                            products.Add(product);
                        }
                    }
                }
            }
            catch (SqlException e)
            {
                Console.Write(e.Message);
            }
            return products;
        }

        public static ObservableCollection<Product> loadByCoupon(Coupon coupon)
        {
            ObservableCollection<Product> products = new ObservableCollection<Product>();
            try
            {
                using (var command = Database.getConn().CreateCommand())
                {
                    command.CommandText =
                    @"SELECT 
                        products.id, 
                        name,
                        [group]
                    FROM 
                        products 
                    LEFT JOIN products_to_coupons p2c ON products.id = p2c.products_id
                    WHERE p2c.coupons_id = @coupons_id AND products.deleted = 0
                    ORDER BY name";
                    command.Parameters.AddWithValue("@coupons_id", coupon.id);

                    using (var reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            Product product = new Product
                            (
                                reader.GetInt32(reader.GetOrdinal("id")),
                                reader.GetString(reader.GetOrdinal("name"))
                            );

                            product.group = reader.GetInt32(reader.GetOrdinal("group"));
                            products.Add(product);
                        }
                    }
                }
            }
            catch (SqlException e)
            {
                Console.Write(e.Message);
            }
            return products;
        }

        public static bool saveProduct(Product product)
        {
            try
            {
                using(var command = Database.getConn().CreateCommand())
                {
                    int pictures_id = addPicture(product);
                    command.CommandText = @"UPDATE products SET name=@name, pictures_id=@pictures_id, print_to_kitchen=@print_to_kitchen, print_duplicate=@print_duplicate, duplicate_printer=@duplicate_printer WHERE id=@id";
                    command.Parameters.Clear();
                    command.Parameters.AddWithValue("@name", product.name);
                    command.Parameters.AddWithValue("@pictures_id", pictures_id);
                    command.Parameters.AddWithValue("@print_to_kitchen", product.printToKitchen);
                    command.Parameters.AddWithValue("@print_duplicate", product.printDuplicate);
                    command.Parameters.AddWithValue("@duplicate_printer", product.duplicatePrinter);
                    command.Parameters.AddWithValue("@id", product.id);

                    command.ExecuteNonQuery();
                    return true;
                }
            }
            catch(SqlException e)
            {
                Console.WriteLine(e.Message);
                return false;
            }
        }

        public static int addPicture(Product product)
        {
            try
            {
                using(var command = Database.getConn().CreateCommand())
                {
                    command.CommandText = @"INSERT INTO pictures (data) VALUES (@data); SELECT SCOPE_IDENTITY();";
                    command.Parameters.Clear();
                    command.Parameters.AddWithValue("@data", product.picture);

                    return Convert.ToInt32(command.ExecuteScalar());
                }
            }
            catch (SqlException e)
            {
                Console.WriteLine(e.Message);
                return 0;
            }
        }

        public static bool createProduct(Product product)
        {
            try
            {
                using(var command = Database.getConn().CreateCommand())
                {
                    int pictures_id = addPicture(product);
                    command.CommandText = @"INSERT INTO products (name, pictures_id, print_to_kitchen, print_duplicate, duplicate_printer) OUTPUT INSERTED.ID VALUES (@name, @pictures_id, @print_to_kitchen, @print_duplicate, @duplicate_printer)";
                    command.Parameters.AddWithValue("@name", product.name);
                    command.Parameters.AddWithValue("@print_to_kitchen", product.printToKitchen);
                    command.Parameters.AddWithValue("@print_duplicate", product.printDuplicate);
                    command.Parameters.AddWithValue("@duplicate_printer", product.duplicatePrinter);
                    command.Parameters.AddWithValue("@pictures_id", pictures_id);

                    product.id = (int) command.ExecuteScalar();
                    products.Add(product);
                    return true;
                }
            }
            catch(SqlException e)
            {
                Console.WriteLine(e.Message);
                return false;
            }
        }

        public static bool deleteProduct(Product product)
        {
            try
            {
                using(var command = Database.getConn().CreateCommand())
                {
                    command.CommandText = @"UPDATE products SET deleted = 1 WHERE id=@id";
                    command.Parameters.AddWithValue("@id", product.id);

                    command.ExecuteNonQuery();
                    loadAll();
                    return true;
                }
            }
            catch(SqlException e)
            {
                Console.WriteLine(e.StackTrace);
                return false;
            }
        }

        internal static List<Product> loadByTransaction(Transaction transaction)
        {
            List<Product> products = new List<Product>();
            try
            {
                using (var command = Database.getConn().CreateCommand())
                {
                    command.CommandText =
                    @"SELECT 
                        products.id, 
                        name,
                        print_to_kitchen,
                        print_duplicate,
                        ISNULL(duplicate_printer, '') AS duplicate_printer,

                        line_number,
                        price,
                        ext_price,
                        is_modified,
                        modification_type,
                        dollar_or_percent,
                        modification_amount,
                        modification_reason,
                        special_requests
                    FROM 
                        products 
                    LEFT JOIN products_to_transactions p2t ON products.id = p2t.products_id
                    WHERE p2t.transactions_id = @transactions_id
                    ORDER BY line_number";
                    command.Parameters.AddWithValue("@transactions_id", transaction.id);

                    using (var reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            Product product = new Product
                            (
                                reader.GetInt32(reader.GetOrdinal("id")),
                                reader.GetString(reader.GetOrdinal("name")),
                                new byte[] { },
                                reader.GetBoolean(reader.GetOrdinal("print_to_kitchen")),
                                reader.GetBoolean(reader.GetOrdinal("print_duplicate")),
                                reader.GetString(reader.GetOrdinal("duplicate_printer"))
                            );

                            product.price = reader.GetDecimal(reader.GetOrdinal("price"));
                            string specialRequests = reader.GetString(reader.GetOrdinal("special_requests"));

                            if(specialRequests.Length > 0)
                            {
                                product.specialRequests = specialRequests.Split(',').ToList<string>();
                            }
                            if(reader.GetBoolean(reader.GetOrdinal("is_modified")))
                            {
                                string reason = "";
                                if (reader["modification_reason"]!=DBNull.Value)
                                {
                                    reason = reader.GetString(reader.GetOrdinal("modification_reason"));
                                }
                                product.priceModification = new PriceModification(reader.GetString(reader.GetOrdinal("modification_type")) == "discount" ? Enums.ModificationType.Discount : Enums.ModificationType.Upcharge,
                                    reader.GetDecimal(reader.GetOrdinal("modification_amount")).ToString(),
                                    reader.GetString(reader.GetOrdinal("dollar_or_percent")) == "dollar" ? Enums.DollarOrPercent.Dollar : Enums.DollarOrPercent.Percent,
                                    reason);
                            }
                            products.Add(product);
                        }
                    }
                }
            }
            catch (SqlException e)
            {
                Console.Write(e.Message);
            }
            return products;
        }

        internal static List<Product> loadByTransactionCoupon(int transactionCoupons_id)
        {
            List<Product> products = new List<Product>();
            try
            {
                using (var command = Database.getConn().CreateCommand())
                {
                    command.CommandText =
                    @"SELECT 
                        products.id, 
                        name,
                        print_to_kitchen,
                        print_duplicate,
                        ISNULL(duplicate_printer, '') AS duplicate_printer,

                        price
                    FROM 
                        products 
                    LEFT JOIN products_to_transactionCoupons p2tc ON products.id = p2tc.products_id
                    WHERE p2tc.transactionCoupons_id = @transactionCoupons_id";
                    command.Parameters.AddWithValue("@transactionCoupons_id", transactionCoupons_id);

                    using (var reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            Product product = new Product
                            (
                                reader.GetInt32(reader.GetOrdinal("id")),
                                reader.GetString(reader.GetOrdinal("name")),
                                new byte[] { },
                                reader.GetBoolean(reader.GetOrdinal("print_to_kitchen")),
                                reader.GetBoolean(reader.GetOrdinal("print_duplicate")),
                                reader.GetString(reader.GetOrdinal("duplicate_printer"))
                            );

                            product.price = reader.GetDecimal(reader.GetOrdinal("price"));
                            products.Add(product);
                        }
                    }
                }
            }
            catch (SqlException e)
            {
                Console.Write(e.Message);
            }
            return products;
        }
    }
}
