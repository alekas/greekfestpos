﻿using GreekFestPOS.POCO;
using GreekFestPOS.Util;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GreekFestPOS.Model
{
    class PaymentDTO
    {
        private static ObservableCollection<Payment> payments = new ObservableCollection<Payment>();

        public static bool savePayment(Payment payment)
        {
            try
            {
                using(var command = Database.getConn().CreateCommand())
                {
                    if(payment.paymentType.Equals("paypal"))
                    {
                        command.CommandText = @"UPDATE payments SET transactions_id=@transactions_id, timestamp=@timestamp, payment_types_id=@payment_types_id, amount_tendered=@amount_tendered, paypal_pnref=@paypal_pnref, paypal_result=@paypal_result, paypal_authcode=@paypal_authcode WHERE id=@id";
                        command.Parameters.Clear();
                        command.Parameters.AddWithValue("@transactions_id", payment.transaction.id);
                        command.Parameters.AddWithValue("@timestamp", payment.timestamp);
                        command.Parameters.AddWithValue("@payment_types_id", payment.paymentType.id);
                        command.Parameters.AddWithValue("@amount_tendered", payment.amountTendered);
                        command.Parameters.AddWithValue("@paypal_pnref", (object)payment.paypalPNRef ?? DBNull.Value);
                        command.Parameters.AddWithValue("@paypal_result", payment.paypalResult);
                        command.Parameters.AddWithValue("@paypal_authcode", (object)payment.paypalAuthCode ?? DBNull.Value);
                        command.Parameters.AddWithValue("@id", payment.id);

                        command.ExecuteNonQuery();
                        return true;
                    }
                    else if(payment.paymentType.Equals("payeezy"))
                    {
                        command.CommandText = @"UPDATE payments SET transactions_id=@transactions_id, timestamp=@timestamp, payment_types_id=@payment_types_id, amount_tendered=@amount_tendered, payeezy_transaction_status=@payeezy_transaction_status, payeezy_transaction_id=@payeezy_transaction_id, payeezy_transaction_type=@payeezy_transaction_type, payeezy_transaction_tag=@payeezy_transaction_tag, payeezy_bank_response_code=@payeezy_bank_response_code, payeezy_bank_message=@payeezy_bank_message, payeezy_gateway_response_code=@payeezy_gateway_response_code, payeezy_gateway_message=@payeezy_gateway_message, payeezy_correlation_id=@payeezy_correlation_id, credit_card_type=@credit_card_type, payment_processor=@payment_processor WHERE id=@id";
                        command.Parameters.Clear();
                        command.Parameters.AddWithValue("@transactions_id", payment.transaction.id);
                        command.Parameters.AddWithValue("@timestamp", payment.timestamp);
                        command.Parameters.AddWithValue("@payment_types_id", payment.paymentType.id);
                        command.Parameters.AddWithValue("@amount_tendered", payment.amountTendered);
                        command.Parameters.AddWithValue("@payeezy_transaction_status", (object)payment.payeezyTransactionStatus ?? DBNull.Value);
                        command.Parameters.AddWithValue("@payeezy_transaction_id", (object)payment.payeezyTransactionId ?? DBNull.Value);
                        command.Parameters.AddWithValue("@payeezy_transaction_type", (object)payment.payeezyTransactionType ?? DBNull.Value);
                        command.Parameters.AddWithValue("@payeezy_transaction_tag", (object)payment.payeezyTransactionTag ?? DBNull.Value);
                        command.Parameters.AddWithValue("@payeezy_bank_response_code", (object)payment.payeezyBankResponseCode ?? DBNull.Value);
                        command.Parameters.AddWithValue("@payeezy_bank_message", (object)payment.payeezyBankMessage ?? DBNull.Value);
                        command.Parameters.AddWithValue("@payeezy_gateway_response_code", (object)payment.payeezyGatewayResponseCode ?? DBNull.Value);
                        command.Parameters.AddWithValue("@payeezy_gateway_message", (object)payment.payeezyGatewayMessage ?? DBNull.Value);
                        command.Parameters.AddWithValue("@payeezy_correlation_id", (object)payment.payeezyCorrelationId ?? DBNull.Value);
                        command.Parameters.AddWithValue("@credit_card_type", (object)payment.creditCardType ?? DBNull.Value);
                        command.Parameters.AddWithValue("@payment_processor", payment.paymentProcessor);

                        command.Parameters.AddWithValue("@id", payment.id);

                        command.ExecuteNonQuery();
                        return true;
                    }

                    return true;
                }
            }
            catch(SqlException e)
            {
                Console.WriteLine(e.Message);
                return false;
            }
        }

        public static bool createPayment(Payment payment)
        {
            try
            {
                using(var command = Database.getConn().CreateCommand())
                {
                    command.CommandText = @"INSERT INTO payments 
                    (
                        transactions_id, 
                        timestamp, 
                        payment_types_id, 
                        amount_tendered, 
                        paypal_pnref, 
                        paypal_result, 
                        paypal_authcode, 
                        payeezy_transaction_status, 
                        payeezy_transaction_id, 
                        payeezy_transaction_type, 
                        payeezy_transaction_tag, 
                        payeezy_bank_response_code, 
                        payeezy_bank_message, 
                        payeezy_gateway_response_code, 
                        payeezy_gateway_message, 
                        payeezy_correlation_id, 
                        credit_card_type, 
                        payment_processor
                    ) 
                    OUTPUT INSERTED.ID 
                    VALUES 
                    (
                        @transactions_id, 
                        @timestamp, 
                        @payment_types_id, 
                        @amount_tendered, 
                        @paypal_pnref, 
                        @paypal_result, 
                        @paypal_authcode, 
                        @payeezy_transaction_status, 
                        @payeezy_transaction_id, 
                        @payeezy_transaction_type, 
                        @payeezy_transaction_tag, 
                        @payeezy_bank_response_code, 
                        @payeezy_bank_message, 
                        @payeezy_gateway_response_code, 
                        @payeezy_gateway_message, 
                        @payeezy_correlation_id, 
                        @credit_card_type, 
                        @payment_processor
                    )";
                    command.Parameters.Clear();
                    command.Parameters.AddWithValue("@transactions_id", payment.transaction.id);
                    command.Parameters.AddWithValue("@timestamp", payment.timestamp);
                    command.Parameters.AddWithValue("@payment_types_id", payment.paymentType.id);
                    command.Parameters.AddWithValue("@amount_tendered", payment.amountTendered);

                    command.Parameters.AddWithValue("@paypal_pnref", (object)payment.paypalPNRef ?? DBNull.Value);
                    command.Parameters.AddWithValue("@paypal_result", payment.paypalResult);
                    command.Parameters.AddWithValue("@paypal_authcode", (object)payment.paypalAuthCode ?? DBNull.Value);
                    
                    command.Parameters.AddWithValue("@payeezy_transaction_status", (object)payment.payeezyTransactionStatus ?? DBNull.Value);
                    command.Parameters.AddWithValue("@payeezy_transaction_id", (object)payment.payeezyTransactionId ?? DBNull.Value);
                    command.Parameters.AddWithValue("@payeezy_transaction_type", (object)payment.payeezyTransactionType ?? DBNull.Value);
                    command.Parameters.AddWithValue("@payeezy_transaction_tag", (object)payment.payeezyTransactionTag ?? DBNull.Value);
                    command.Parameters.AddWithValue("@payeezy_bank_response_code", (object)payment.payeezyBankResponseCode ?? DBNull.Value);
                    command.Parameters.AddWithValue("@payeezy_bank_message", (object)payment.payeezyBankMessage ?? DBNull.Value);
                    command.Parameters.AddWithValue("@payeezy_gateway_response_code", (object)payment.payeezyGatewayResponseCode ?? DBNull.Value);
                    command.Parameters.AddWithValue("@payeezy_gateway_message", (object)payment.payeezyGatewayMessage ?? DBNull.Value);
                    command.Parameters.AddWithValue("@payeezy_correlation_id", (object)payment.payeezyCorrelationId ?? DBNull.Value);
                    command.Parameters.AddWithValue("@credit_card_type", (object)payment.creditCardType ?? DBNull.Value);
                    command.Parameters.AddWithValue("@payment_processor", (object)payment.paymentProcessor ?? DBNull.Value);

                    payment.id = (int) command.ExecuteScalar();
                    payments.Add(payment);
                    return true;
                }
            }
            catch(SqlException e)
            {
                Console.WriteLine(e.Message);
                return false;
            }
        }
    }
}
