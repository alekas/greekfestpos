﻿using GreekFestPOS.POCO;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GreekFestPOS.Model
{
    class CouponDTO
    {
        private static ObservableCollection<Coupon> coupons = new ObservableCollection<Coupon>();

        public static Coupon loadByCode(string code)
        {
            try
            {
                Coupon coupon = null;
                using (var command = Database.getConn().CreateCommand())
                {
                    command.CommandText =
                    @"SELECT coupons.id, name, code FROM coupons WHERE code = @code";
                    command.Parameters.AddWithValue("@code", code);

                    using (var reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            coupon = new Coupon
                            (
                                reader.GetInt32(reader.GetOrdinal("id")),
                                reader.GetString(reader.GetOrdinal("name")),
                                reader.GetString(reader.GetOrdinal("code"))
                            );
                        }
                    }
                }

                if (coupon != null)
                {
                    coupon.products = new List<Product>(ProductDTO.loadAllByCoupon(coupon));

                    return coupon;
                }
            }
            catch (SqlException e)
            {
                Console.Write(e.Message);
            }
            return null;
        }

        public static ObservableCollection<Coupon> loadAll()
        {
            coupons.Clear();
            try
            {
                using (var command = Database.getConn().CreateCommand())
                {
                    command.CommandText = "SELECT coupons.id, name, code FROM coupons WHERE deleted != 1 ORDER BY name";
                    using (var reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            coupons.Add(new Coupon
                            (
                                reader.GetInt32(reader.GetOrdinal("id")),
                                reader.GetString(reader.GetOrdinal("name")),
                                reader.GetString(reader.GetOrdinal("code"))
                            ));
                        }
                    }
                }
            }
            catch (SqlException e)
            {
                Console.Write(e.Message);                
            }
            return coupons;
        }

        public static bool saveCoupon(Coupon coupon)
        {
            try
            {
                using(var command = Database.getConn().CreateCommand())
                {
                    command.CommandText = @"UPDATE coupons SET name=@name, code=@code WHERE id=@id";
                    command.Parameters.Clear();
                    command.Parameters.AddWithValue("@name", coupon.name);
                    command.Parameters.AddWithValue("@code", coupon.code);
                    command.Parameters.AddWithValue("@id", coupon.id);

                    command.ExecuteNonQuery();
                    return true;
                }
            }
            catch(SqlException e)
            {
                Console.WriteLine(e.Message);
                return false;
            }
        }
        
        public static bool createCoupon(Coupon coupon)
        {
            try
            {
                using(var command = Database.getConn().CreateCommand())
                {
                    command.CommandText = @"INSERT INTO coupons (name, code, deleted) OUTPUT INSERTED.ID VALUES (@name, @code, 0);";
                    command.Parameters.AddWithValue("@name", coupon.name);
                    command.Parameters.AddWithValue("@code", coupon.code);

                    coupon.id = (int) command.ExecuteScalar();

                    coupons.Add(coupon);
                    return true;
                }
            }
            catch(SqlException e)
            {
                Console.WriteLine(e.Message);
                return false;
            }
        }

        public static bool deleteCoupon(Coupon coupon)
        {
            try
            {
                using(var command = Database.getConn().CreateCommand())
                {
                    command.CommandText = @"UPDATE coupons SET deleted=1 WHERE id=@id";
                    command.Parameters.AddWithValue("@id", coupon.id);

                    command.ExecuteNonQuery();
                    loadAll();
                    return true;
                }
            }
            catch(SqlException e)
            {
                Console.WriteLine(e.StackTrace);
                return false;
            }
        }

        public static void setProducts(Coupon coupon, IEnumerable<Product> products)
        {
            try
            {
                using (var command = Database.getConn().CreateCommand())
                {
                    command.CommandText = "DELETE FROM products_to_coupons WHERE coupons_id = @coupons_id";
                    command.Parameters.AddWithValue("@coupons_id", coupon.id);
                    command.ExecuteNonQuery();

                    command.CommandText = @"INSERT INTO products_to_coupons (products_id, coupons_id, [group]) VALUES (@products_id, @coupons_id, @group)";

                    foreach(Product product in products)
                    {
                        if(!product.enabled)
                        {
                            continue;
                        }

                        command.Parameters.Clear();
                        
                        command.Parameters.AddWithValue("@coupons_id", coupon.id);
                        command.Parameters.AddWithValue("@products_id", product.id);
                        command.Parameters.AddWithValue("@group", product.group);
                        command.ExecuteNonQuery();
                    }
                }
            }
            catch (SqlException e)
            {
                Console.WriteLine(e.Message);
            }
        }

        internal static IReadOnlyList<Coupon> loadByTransaction(Transaction transaction)
        {
            coupons.Clear();
            try
            {
                using (var command = Database.getConn().CreateCommand())
                {
                    command.CommandText = @"SELECT 
                    coupons.id, 
                    name, 
                    code,
                    
                    transactionCoupons.id AS transactionCoupons_id,
                    amount
                    FROM coupons
                    LEFT JOIN transactionCoupons ON coupons.id = transactionCoupons.coupons_id
                    WHERE transactions_id = @transactions_id";

                    command.Parameters.AddWithValue("@transactions_id", transaction.id);

                    using (var reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            Coupon coupon = new Coupon
                            (
                                reader.GetInt32(reader.GetOrdinal("id")),
                                reader.GetString(reader.GetOrdinal("name")),
                                reader.GetString(reader.GetOrdinal("code"))
                            );

                            coupon.products = ProductDTO.loadByTransactionCoupon(reader.GetInt32(reader.GetOrdinal("transactionCoupons_id")));
                            coupons.Add(coupon);
                        }
                    }
                }
            }
            catch (SqlException e)
            {
                Console.Write(e.Message);                
            }
            return coupons;
        }
    }
}
