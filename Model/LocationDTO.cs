﻿using GreekFestPOS.POCO;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GreekFestPOS.Model
{
    class LocationDTO
    {
        private static ObservableCollection<Location> locations = new ObservableCollection<Location>();

        public static ObservableCollection<Location> loadAll()
        {
            locations.Clear();
            try
            {
                using (var command = Database.getConn().CreateCommand())
                {
                    command.CommandText = "SELECT locations.id, account_number, name, hasInventory FROM locations WHERE deleted = 0 ORDER BY name";
                    using (var reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            locations.Add(new Location
                            (
                                reader.GetInt32(reader.GetOrdinal("id")),
                                reader.GetString(reader.GetOrdinal("account_number")),
                                reader.GetString(reader.GetOrdinal("name")),
                                reader.GetBoolean(reader.GetOrdinal("hasInventory"))
                            ));
                        }
                    }
                }
            }
            catch (SqlException e)
            {
                Console.Write(e.Message);                
            }
            return locations;
        }

        public static bool saveLocation(Location location)
        {
            try
            {
                using(var command = Database.getConn().CreateCommand())
                {
                    command.CommandText = @"UPDATE locations SET name=@name, account_number=@account_number, hasInventory=@hasinventory WHERE id=@id";
                    command.Parameters.Clear();
                    command.Parameters.AddWithValue("@name", location.name);
                    command.Parameters.AddWithValue("@account_number", location.account_number);
                    command.Parameters.AddWithValue("@id", location.id);
                    command.Parameters.AddWithValue("@hasInventory", location.hasInventory);

                    command.ExecuteNonQuery();
                    return true;
                }
            }
            catch(SqlException e)
            {
                Console.WriteLine(e.Message);
                return false;
            }
        }
        
        public static bool createLocation(Location location)
        {
            try
            {
                using(var command = Database.getConn().CreateCommand())
                {
                    command.CommandText = @"INSERT INTO locations (name, account_number, hasInventory) VALUES (@name, @account_number, @hasInventory)";
                    command.Parameters.AddWithValue("@name", location.name);
                    command.Parameters.AddWithValue("@account_number", location.account_number);
                    command.Parameters.AddWithValue("@hasInventory", location.hasInventory);

                    command.ExecuteNonQuery();
                    locations.Add(location);
                    return true;
                }
            }
            catch(SqlException e)
            {
                Console.WriteLine(e.Message);
                return false;
            }
        }

        public static bool deleteLocation(Location location)
        {
            try
            {
                using(var command = Database.getConn().CreateCommand())
                {
                    command.CommandText = @"UPDATE locations SET deleted = 1 WHERE id=@id";
                    command.Parameters.AddWithValue("@id", location.id);

                    command.ExecuteNonQuery();
                    loadAll();
                    return true;
                }
            }
            catch(SqlException e)
            {
                Console.WriteLine(e.StackTrace);
                return false;
            }
        }

        public static void setProducts(Location location, IEnumerable<Product> products)
        {
            try
            {
                using (var command = Database.getConn().CreateCommand())
                {
                    command.CommandText = "DELETE FROM products_to_locations WHERE locations_id = @locations_id";
                    command.Parameters.AddWithValue("@locations_id", location.id);
                    command.ExecuteNonQuery();

                    command.CommandText = @"INSERT INTO products_to_locations (products_id, locations_id, price) VALUES (@products_id, @locations_id, @price)";

                    foreach(Product product in products)
                    {
                        if(!product.enabled)
                        {
                            continue;
                        }

                        command.Parameters.Clear();
                        
                        command.Parameters.AddWithValue("@locations_id", location.id);
                        command.Parameters.AddWithValue("@products_id", product.id);
                        command.Parameters.AddWithValue("@price", product.price);
                        command.ExecuteNonQuery();
                    }
                }
            }
            catch (SqlException e)
            {
                Console.WriteLine(e.Message);
            }
        }

        internal static Location loadById(int p)
        {
            throw new NotImplementedException();
        }
    }
}
