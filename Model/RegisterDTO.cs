﻿using GreekFestPOS.POCO;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GreekFestPOS.Model
{
    class RegisterDTO
    {
        private static ObservableCollection<Register> registers = new ObservableCollection<Register>();

        public static ObservableCollection<Register> loadAll()
        {
            registers.Clear();
            try
            {
                using (var command = Database.getConn().CreateCommand())
                {
                    command.CommandText = "SELECT registers.id, registers.name, locations_id, locations.name AS locations_name, locations.hasInventory AS locations_hasInventory, account_number FROM registers LEFT JOIN locations ON registers.locations_id = locations.id ORDER BY locations.name, registers.name";
                    using (var reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            registers.Add(new Register
                            (
                                reader.GetInt32(reader.GetOrdinal("id")),
                                new Location
                                (
                                    reader.GetInt32(reader.GetOrdinal("locations_id")),
                                    reader.GetString(reader.GetOrdinal("account_number")),
                                    reader.GetString(reader.GetOrdinal("locations_name")),
                                    reader.GetBoolean(reader.GetOrdinal("locations_hasInventory"))
                                ),
                                reader.GetString(reader.GetOrdinal("name"))
                            ));
                        }
                    }
                }
            }
            catch (SqlException e)
            {
                Console.Write(e.Message);                
            }
            return registers;
        }

        public static bool saveRegister(Register register)
        {
            try
            {
                using(var command = Database.getConn().CreateCommand())
                {
                    command.CommandText = @"UPDATE registers SET name=@name, locations_id=@locations_id WHERE id=@id";
                    command.Parameters.Clear();
                    command.Parameters.AddWithValue("@name", register.name);
                    command.Parameters.AddWithValue("@locations_id", register.location.id);
                    command.Parameters.AddWithValue("@id", register.id);

                    command.ExecuteNonQuery();
                    return true;
                }
            }
            catch(SqlException e)
            {
                Console.WriteLine(e.Message);
                return false;
            }
        }
        
        public static bool createRegister(Register register)
        {
            try
            {
                using(var command = Database.getConn().CreateCommand())
                {
                    command.CommandText = @"INSERT INTO registers (name, locations_id) VALUES (@name, @locations_id)";
                    command.Parameters.AddWithValue("@name", register.name);
                    command.Parameters.AddWithValue("@locations_id", register.location.id);

                    command.ExecuteNonQuery();
                    registers.Add(register);
                    return true;
                }
            }
            catch(SqlException e)
            {
                Console.WriteLine(e.Message);
                return false;
            }
        }

        public static bool deleteRegister(Register register)
        {
            try
            {
                using(var command = Database.getConn().CreateCommand())
                {
                    command.CommandText = @"DELETE FROM registers WHERE id=@id";
                    command.Parameters.AddWithValue("@id", register.id);

                    command.ExecuteNonQuery();
                    loadAll();
                    return true;
                }
            }
            catch(SqlException e)
            {
                Console.WriteLine(e.StackTrace);
                return false;
            }
        }

        internal static Register loadById(int registers_id)
        {
            Register register = null;
            try
            {
                using (var command = Database.getConn().CreateCommand())
                {
                    command.CommandText = "SELECT registers.id, registers.name, locations_id, locations.name AS locations_name, locations.hasInventory, account_number FROM registers LEFT JOIN locations ON registers.locations_id = locations.id WHERE registers.id = @registers_id";
                    command.Parameters.AddWithValue("@registers_id", registers_id);
                    using (var reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            register = new Register
                            (
                                reader.GetInt32(reader.GetOrdinal("id")),
                                new Location
                                (
                                    reader.GetInt32(reader.GetOrdinal("locations_id")),
                                    reader.GetString(reader.GetOrdinal("account_number")),
                                    reader.GetString(reader.GetOrdinal("locations_name")),
                                    reader.GetBoolean(reader.GetOrdinal("hasInventory"))
                                ),
                                reader.GetString(reader.GetOrdinal("name"))
                            );
                        }
                    }
                }
            }
            catch (SqlException e)
            {
                Console.Write(e.Message);
            }
            return register;
        }
    }
}
