﻿using GreekFestPOS.POCO;
using GreekFestPOS.Util;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GreekFestPOS.Model
{
    class TillDTO
    {

        public static Till loadByRegisterAndStatus(Register register, TillStatus status)
        {
            try
            {
                using (var command = Database.getConn().CreateCommand())
                {
                    command.CommandText =
@"SELECT 
    tills.id,
    tills.amount,
    tills.start_time,
    tills.end_time,

    registers.id AS registers_id,
    registers.name AS registers_name,

    locations.id AS locations_id,
    locations.account_number AS locations_account_number,
    locations.name AS locations_name,
    hasInventory,

    users.id AS users_id,
    users.name AS users_name,
    
    tills.statuses_id,

    total_sales,
    total_credit_card_sales,
    total_cash_sales,
    total_coupons,
    total_coupon_sales
FROM 
    tills
LEFT JOIN registers ON tills.registers_id = registers.id
LEFT JOIN locations ON registers.locations_id = locations.id
LEFT JOIN users ON tills.users_id = users.id
WHERE statuses_id = @statuses_id
AND registers.id = @registers_id";

                    command.Parameters.AddWithValue("@statuses_id", status.id);
                    command.Parameters.AddWithValue("@registers_id", register.id);
                    using (var reader = command.ExecuteReader())
                    {
                        if (reader.Read())
                        {
                            return new Till
                            (
                                reader.GetInt32(reader.GetOrdinal("id")),
                                new Register
                                (
                                    reader.GetInt32(reader.GetOrdinal("registers_id")),
                                    new Location
                                    (
                                        reader.GetInt32(reader.GetOrdinal("locations_id")),
                                        reader.GetString(reader.GetOrdinal("locations_account_number")),
                                        reader.GetString(reader.GetOrdinal("locations_name")),
                                        reader.GetBoolean(reader.GetOrdinal("hasInventory"))
                                    ),
                                    reader.GetString(reader.GetOrdinal("registers_name"))
                                ),
                                new User
                                (
                                    reader.GetInt32(reader.GetOrdinal("users_id")),
                                    reader.GetString(reader.GetOrdinal("users_name"))
                                ),
                                reader.GetDecimal(reader.GetOrdinal("amount")),
                                reader.GetDateTime(reader.GetOrdinal("start_time")),
                                reader[reader.GetOrdinal("end_time")] as DateTime?,
                                TillStatus.loadById(reader.GetInt32(reader.GetOrdinal("statuses_id"))),
                                
                                reader[reader.GetOrdinal("total_sales")] as decimal?,
                                reader[reader.GetOrdinal("total_credit_card_sales")] as decimal?,
                                reader[reader.GetOrdinal("total_cash_sales")] as decimal?,
                                reader[reader.GetOrdinal("total_coupons")] as decimal?,
                                reader[reader.GetOrdinal("total_coupon_sales")] as decimal?
                                
                            );
                        }
                    }
                }
            }
            catch (SqlException e)
            {
                Console.Write(e.Message);
            }

            return null;
        }
    
        internal static void openTill(Till till)
        {
            try
            {
                using (var command = Database.getConn().CreateCommand())
                {
                    command.CommandText =
@"INSERT INTO tills
(
    registers_id,
    statuses_id,
    users_id,
    amount,
    start_time
)
OUTPUT INSERTED.ID VALUES
(
    @registers_id,
    @statuses_id,
    @users_id,
    @amount,
    @start_time
)
";
                    command.Parameters.Clear();
                    command.Parameters.AddWithValue("@registers_id", till.register.id);
                    command.Parameters.AddWithValue("@statuses_id", till.status.id);
                    command.Parameters.AddWithValue("@users_id", till.user.id);
                    command.Parameters.AddWithValue("@amount", till.amount);
                    command.Parameters.AddWithValue("@start_time", till.start_time);

                    till.id = (int)command.ExecuteScalar();
                }
            }
            catch (SqlException e)
            {
                Console.WriteLine(e.Message);
            }
        }

        public static void populateSales(Till till)
        {
            try
            {
                using (var command = Database.getConn().CreateCommand())
                {
                    command.CommandText =
@"SELECT 
   (SELECT SUM(total) FROM transactions WHERE registers_id = @registers_id AND completed_timestamp > @start_time AND completed_timestamp <= CURRENT_TIMESTAMP AND statuses_id=@statuses_id) AS total_sales,
    (SELECT SUM(total-transaction_fee) FROM payments LEFT JOIN transactions ON payments.transactions_id = transactions.id WHERE registers_id = @registers_id AND completed_timestamp > @start_time AND completed_timestamp <= CURRENT_TIMESTAMP AND statuses_id=@statuses_id AND payment_types_id = @payment_types_id_credit_card) AS total_credit_card_sales,
	(SELECT SUM(transaction_fee) FROM payments LEFT JOIN transactions ON payments.transactions_id = transactions.id WHERE registers_id = @registers_id AND completed_timestamp > @start_time AND completed_timestamp <= CURRENT_TIMESTAMP AND statuses_id=@statuses_id AND payment_types_id = @payment_types_id_credit_card) AS total_credit_card_fees,
    (SELECT SUM(total) FROM payments LEFT JOIN transactions ON payments.transactions_id = transactions.id WHERE registers_id = @registers_id AND completed_timestamp > @start_time AND completed_timestamp <= CURRENT_TIMESTAMP AND statuses_id=@statuses_id AND payment_types_id = @payment_types_id_cash) AS total_cash_sales,
    (SELECT COUNT(*) FROM transactionCoupons AS tc LEFT JOIN transactions ON tc.transactions_id = transactions.id WHERE registers_id = @registers_id AND completed_timestamp > @start_time AND completed_timestamp <= CURRENT_TIMESTAMP AND statuses_id=@statuses_id) AS total_coupons,
    (SELECT SUM(amount) FROM transactionCoupons AS tc LEFT JOIN transactions ON tc.transactions_id = transactions.id WHERE registers_id = @registers_id AND completed_timestamp > @start_time AND completed_timestamp <= CURRENT_TIMESTAMP AND statuses_id=@statuses_id) AS total_coupon_sales
";

                    command.Parameters.AddWithValue("@registers_id", till.register.id);
                    command.Parameters.AddWithValue("@start_time", till.start_time);
                    command.Parameters.AddWithValue("@statuses_id", Status.COMPLETED.id);
                    command.Parameters.AddWithValue("@payment_types_id_credit_card", PaymentType.CREDIT.id);
                    command.Parameters.AddWithValue("@payment_types_id_cash", PaymentType.CASH.id);
                    using (var reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            till.total_sales = reader["total_sales"] as decimal?;
                            till.total_credit_card_sales = reader["total_credit_card_sales"] as decimal?;
                            till.total_credit_card_fees = reader["total_credit_card_fees"] as decimal?;
                            till.total_cash_sales = reader["total_cash_sales"] as decimal?;
                            till.total_coupons = reader["total_coupons"] as int?;
                            till.total_coupon_sales = reader["total_coupon_sales"] as decimal?;
                        }
                    }
                }
            }
            catch (SqlException e)
            {
                Console.Write(e.Message);
            }
        }

        internal static void saveTill(Till till)
        {
            try
            {
                using (var command = Database.getConn().CreateCommand())
                {
                    command.CommandText = @"UPDATE tills SET end_time=@end_time, statuses_id=@statuses_id, total_sales=@total_sales, total_credit_card_sales=@total_credit_card_sales, total_cash_sales=@total_cash_sales, total_coupons=@total_coupons, total_coupon_sales=@total_coupon_sales WHERE id=@id";
                    command.Parameters.Clear();
                    command.Parameters.AddWithValue("@end_time", till.end_time);
                    command.Parameters.AddWithValue("@statuses_id", till.status.id);
                    command.Parameters.AddWithValue("@total_sales", (object)till.total_sales ?? DBNull.Value);
                    command.Parameters.AddWithValue("@total_credit_card_sales", (object)till.total_credit_card_sales ?? DBNull.Value);
                    command.Parameters.AddWithValue("@total_cash_sales", (object)till.total_cash_sales ?? DBNull.Value);
                    command.Parameters.AddWithValue("@total_coupons", (object)till.total_coupons ?? DBNull.Value);
                    command.Parameters.AddWithValue("@total_coupon_sales", (object)till.total_coupon_sales ?? DBNull.Value);
                    command.Parameters.AddWithValue("@id", till.id);

                    command.ExecuteNonQuery();
                }
            }
            catch (SqlException e)
            {
                Console.Write(e.Message);
            }
        }

        internal static decimal getCurrentCashAmount()
        {
            try
            {
                using (var command = Database.getConn().CreateCommand())
                {
                    command.CommandText = @"SELECT ISNULL(@till_amount + SUM(transactions.total), @till_amount2) FROM transactions LEFT JOIN payments ON transactions.id = payments.transactions_id WHERE payments.payment_types_id = @cash_payment_types_id AND transactions.statuses_id = @completed_statuses_id AND transactions.registers_id = @registers_id AND transactions.completed_timestamp > @start_time";

                    command.Parameters.Clear();
                    command.Parameters.AddWithValue("@till_amount", App.till.amount);
                    command.Parameters.AddWithValue("@till_amount2", App.till.amount);
                    command.Parameters.AddWithValue("@cash_payment_types_id", PaymentType.CASH.id);
                    command.Parameters.AddWithValue("@completed_statuses_id", Status.COMPLETED.id);
                    command.Parameters.AddWithValue("@registers_id", App.register.id);
                    command.Parameters.AddWithValue("@start_time", App.till.start_time);

                    return (decimal)command.ExecuteScalar();
                }
            }
            catch (SqlException e)
            {
                Console.Write(e.Message);
            }

            return 0;
        }

        internal static int getCurrentTransactionCount()
        {
            try
            {
                using (var command = Database.getConn().CreateCommand())
                {
                    command.CommandText = @"SELECT COUNT(*) FROM transactions LEFT JOIN payments ON transactions.id = payments.transactions_id WHERE transactions.statuses_id = @completed_statuses_id AND transactions.registers_id = @registers_id AND transactions.completed_timestamp > @start_time";

                    command.Parameters.Clear();
                    command.Parameters.AddWithValue("@completed_statuses_id", Status.COMPLETED.id);
                    command.Parameters.AddWithValue("@registers_id", App.register.id);
                    command.Parameters.AddWithValue("@start_time", App.till.start_time);

                    return (int)command.ExecuteScalar();
                }
            }
            catch (SqlException e)
            {
                Console.Write(e.Message);
            }

            return 0;
        }
    }
}
