﻿using GreekFestPOS.POCO;
using GreekFestPOS.Util;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GreekFestPOS.Model
{
    class TransactionDTO
    {
        private static ObservableCollection<Transaction> transactions = new ObservableCollection<Transaction>();

        public static ObservableCollection<Transaction> loadAll()
        {
            transactions.Clear();
            try
            {
                using (var command = Database.getConn().CreateCommand())
                {
                    command.CommandText =
                    @"SELECT 
                        transactions.id,
                        statuses_id,
                        total,
                        created_timestamp,
                        completed_timestamp,

                        users.id AS users_id,
                        users.name AS users_name,

                        registers.id AS registers_id,
                        registers.name AS registers_name,

                        locations.name AS locations_name,
                        locations.id AS locations_id,
                        locations.account_number AS locations_account_number,
                        hasInventory
                    FROM 
                        transactions 
                    LEFT JOIN registers ON transactions.registers_id = registers.id
                    LEFT JOIN locations ON registers.locations_id = locations.id
                    ORDER BY name";
                    using (var reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            transactions.Add(new Transaction
                            (
                                reader.GetInt32(reader.GetOrdinal("id")),
                                new Register
                                (
                                    reader.GetInt32(reader.GetOrdinal("registers_id")),
                                    new Location
                                    (
                                        reader.GetInt32(reader.GetOrdinal("locations_id")),
                                        reader.GetString(reader.GetOrdinal("locations_account_number")),
                                        reader.GetString(reader.GetOrdinal("locations_name")),
                                        reader.GetBoolean(reader.GetOrdinal("hasInventory"))
                                    ),
                                    reader.GetString(reader.GetOrdinal("registers_name"))
                                ),
                                new User
                                (
                                    reader.GetInt32(reader.GetOrdinal("users_id")),
                                    reader.GetString(reader.GetOrdinal("users_name"))
                                ),
                                reader.GetDecimal(reader.GetOrdinal("total")),
                                reader.GetDateTime(reader.GetOrdinal("created_timestamp")),
                                reader.GetDateTime(reader.GetOrdinal("completed_timestamp")),
                                Status.loadById(reader.GetInt32(reader.GetOrdinal("statuses_id")))
                                
                            ));
                        }
                    }
                }
            }
            catch (SqlException e)
            {
                Console.Write(e.Message);
            }
            return transactions;
        }

        public static bool saveTransaction(Transaction transaction)
        {
            try
            {
                using(var command = Database.getConn().CreateCommand())
                {
                    command.CommandText = 
@"UPDATE 
    transactions 
SET 
    registers_id=@registers_id, 
    users_id=@users_id, 
    total=@total, 
    transaction_fee=@transaction_fee, 
    completed_timestamp=@completed_timestamp, 
    statuses_id=@statuses_id 
WHERE id=@id";
                    command.Parameters.Clear();
                    command.Parameters.AddWithValue("@registers_id", transaction.register.id);
                    command.Parameters.AddWithValue("@users_id", transaction.user.id);
                    command.Parameters.AddWithValue("@total", transaction.calculatedTotal);
                    command.Parameters.AddWithValue("@transaction_fee", transaction.transaction_fee);
                    command.Parameters.AddWithValue("@completed_timestamp", transaction.completed_timestamp);
                    command.Parameters.AddWithValue("@statuses_id", transaction.status.id);
                    command.Parameters.AddWithValue("@id", transaction.id);

                    command.ExecuteNonQuery();

                    for (int i = 0; i < transaction.products.Count; i++)
                    {

                        Product product = transaction.products[i];

                        using (var commandProduct = Database.getConn().CreateCommand())
                        {
                            commandProduct.CommandText =
                            @"INSERT INTO products_to_transactions 
                            (
                                products_id, 
                                transactions_id, 
                                line_number,
                                price, 
                                ext_price,
    
                                is_modified,
                                modification_type,
                                dollar_or_percent,
                                modification_amount,
                                special_requests
                            ) 
                            VALUES 
                            (
                                @products_id,
                                @transactions_id, 
                                @line_number, 
                                @price, 
                                @ext_price,
    
                                @is_modified,
                                @modification_type,
                                @dollar_or_percent,
                                @modification_amount,
                                @special_requests
                            )";
                            commandProduct.Parameters.Clear();
                            //@products_id, @transactions_id, @line_number, @quantity, @price, @ext_price
                            commandProduct.Parameters.AddWithValue("@products_id", product.id);
                            commandProduct.Parameters.AddWithValue("@transactions_id", transaction.id);
                            commandProduct.Parameters.AddWithValue("@line_number", i);
                            commandProduct.Parameters.AddWithValue("@price", product.price);
                            commandProduct.Parameters.AddWithValue("@ext_price", product.adjustedPrice);
                            commandProduct.Parameters.AddWithValue("@special_requests", string.Join(",", product.specialRequests.ToArray()));

                            commandProduct.Parameters.AddWithValue("@is_modified", product.priceModification != null);
                            if (product.priceModification != null)
                            {
                                commandProduct.Parameters.AddWithValue("@modification_type", product.priceModification.modificationType == Enums.ModificationType.Discount ? "discount" : "upcharge");
                                commandProduct.Parameters.AddWithValue("@dollar_or_percent", product.priceModification.dollarOrPercent == Enums.DollarOrPercent.Dollar ? "dollar" : "percent");
                                commandProduct.Parameters.AddWithValue("@modification_amount", product.priceModification.amount);
                                commandProduct.Parameters.AddWithValue("@modification_reason", product.priceModification.reason);
                            }
                            else
                            {
                                commandProduct.Parameters.AddWithValue("@modification_type", DBNull.Value);
                                commandProduct.Parameters.AddWithValue("@dollar_or_percent", DBNull.Value);
                                commandProduct.Parameters.AddWithValue("@modification_amount", DBNull.Value);
                                commandProduct.Parameters.AddWithValue("@modification_reason", DBNull.Value);
                            }

                            commandProduct.ExecuteNonQuery();
                        }
                    }
                }
                
                for(int i=0; i<transaction.coupons.Count; i++)
                {

                    Coupon coupon = transaction.coupons[i];

                    using (var commandCoupon = Database.getConn().CreateCommand())
                    {
                        commandCoupon.CommandText =
                        @"INSERT INTO transactionCoupons 
                        (
                            coupons_id, 
                            transactions_id, 
                            amount
                        ) 
                        OUTPUT INSERTED.ID 
                        VALUES 
                        (
                            @coupons_id,
                            @transactions_id, 
                            @amount
                        )";
                        commandCoupon.Parameters.Clear();
                        commandCoupon.Parameters.AddWithValue("@coupons_id", coupon.id);
                        commandCoupon.Parameters.AddWithValue("@transactions_id", transaction.id);
                        commandCoupon.Parameters.AddWithValue("@amount", coupon.amount);
                            
                        int transactionCoupons_id = (int) commandCoupon.ExecuteScalar();
                        foreach(Product product in coupon.products)
                        {
                            using (var commandProduct = Database.getConn().CreateCommand())
                            {
                                commandProduct.CommandText =
                                @"INSERT INTO products_to_transactionCoupons
                                (
                                    transactionCoupons_id, 
                                    products_id, 
                                    price
                                ) 
                                VALUES 
                                (
                                    @transactionCoupons_id,
                                    @products_id,
                                    @price
                                )";
                                commandProduct.Parameters.Clear();
                                //@products_id, @transactions_id, @line_number, @quantity, @price, @ext_price
                                commandProduct.Parameters.AddWithValue("@transactionCoupons_id", transactionCoupons_id);
                                commandProduct.Parameters.AddWithValue("@products_id", product.id);
                                commandProduct.Parameters.AddWithValue("@price", product.adjustedPrice);

                                commandProduct.ExecuteNonQuery();
                            }
                        }
                    }
                }

                for (int i = 0; i < transaction.payments.Count; i++)
                {
                    PaymentDTO.createPayment(transaction.payments[i]);
                }
                return true;
                
            }
            catch(SqlException e)
            {
                Console.WriteLine(e.Message);
                return false;
            }
        }

        public static bool createTransaction(Transaction transaction)
        {
            try
            {
                using(var command = Database.getConn().CreateCommand())
                {
                    command.CommandText = @"INSERT INTO transactions (registers_id, users_id, total, created_timestamp, statuses_id) OUTPUT INSERTED.ID VALUES (@registers_id, @users_id, @total, GETDATE(), @statuses_id)";
                    command.Parameters.AddWithValue("@registers_id", transaction.register.id);
                    command.Parameters.AddWithValue("@users_id", transaction.user.id);
                    command.Parameters.AddWithValue("@total", transaction.calculatedTotal);
                    command.Parameters.AddWithValue("@statuses_id", Status.NEW.id);

                    transaction.id = (int) command.ExecuteScalar();
                    transactions.Add(transaction);
                    return true;
                }
            }
            catch(SqlException e)
            {
                Console.WriteLine(e.Message);
                return false;
            }
        }

        internal static ObservableCollection<Transaction> loadComplete()
        {
            transactions.Clear();
            try
            {
                using (var command = Database.getConn().CreateCommand())
                {
                    command.CommandText =
                    @"SELECT 
                        transactions.id,
                        transactions.statuses_id,
                        transactions.total,
                        transactions.transaction_fee,
                        transactions.created_timestamp,
                        transactions.completed_timestamp,

                        users.id AS users_id,
                        users.name AS users_name,

                        registers.id AS registers_id,
                        registers.name AS registers_name,

                        locations.name AS locations_name,
                        locations.id AS locations_id,
                        locations.account_number AS locations_account_number,
                        hasInventory,
            
                        payments.id AS payments_id,
                        payments.timestamp AS payments_timestamp,
                        payments.payment_types_id AS payments_payment_types_id,
                        payments.amount_tendered AS payments_amount_tendered,
                        payments.paypal_pnref AS payments_paypal_pnref,
                        payments.paypal_result AS payments_paypal_result,
                        payments.paypal_authcode AS payments_paypal_authcode
                    FROM 
                        transactions 
                    LEFT JOIN registers ON transactions.registers_id = registers.id
                    LEFT JOIN locations ON registers.locations_id = locations.id
                    LEFT JOIN users ON transactions.users_id = users.id
                    LEFT JOIN payments ON transactions.id = payments.transactions_id
                    WHERE statuses_id = @statuses_id_completed OR statuses_id = @statuses_id_voided
                    ORDER BY completed_timestamp";

                    command.Parameters.AddWithValue("@statuses_id_completed", Status.COMPLETED.id);
                    command.Parameters.AddWithValue("@statuses_id_voided", Status.VOIDED.id);

                    using (var reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            Transaction transaction = new Transaction
                            (
                                reader.GetInt32(reader.GetOrdinal("id")),
                                new Register
                                (
                                    reader.GetInt32(reader.GetOrdinal("registers_id")),
                                    new Location
                                    (
                                        reader.GetInt32(reader.GetOrdinal("locations_id")),
                                        reader.GetString(reader.GetOrdinal("locations_account_number")),
                                        reader.GetString(reader.GetOrdinal("locations_name")),
                                        reader.GetBoolean(reader.GetOrdinal("hasInventory"))
                                    ),
                                    reader.GetString(reader.GetOrdinal("registers_name"))
                                ),
                                new User
                                (
                                    reader.GetInt32(reader.GetOrdinal("users_id")),
                                    reader.GetString(reader.GetOrdinal("users_name"))
                                ),
                                reader.GetDecimal(reader.GetOrdinal("total")),
                                reader.GetDateTime(reader.GetOrdinal("created_timestamp")),
                                reader.GetDateTime(reader.GetOrdinal("completed_timestamp")),
                                Status.loadById(reader.GetInt32(reader.GetOrdinal("statuses_id")))
                            );

                            if (!reader.IsDBNull(reader.GetOrdinal("payments_id")))
                            {
                                transaction.payments.Add(new Payment(

                                            reader.GetInt32(reader.GetOrdinal("payments_id")),
                                            transaction,
                                            reader.GetDateTime(reader.GetOrdinal("payments_timestamp")),
                                            PaymentType.loadById(reader.GetInt32(reader.GetOrdinal("payments_payment_types_id"))),
                                            reader.IsDBNull(reader.GetOrdinal("payments_amount_tendered")) ? 0 : reader.GetDecimal(reader.GetOrdinal("payments_amount_tendered")),
                                            reader.IsDBNull(reader.GetOrdinal("payments_paypal_pnref")) ? "" : reader.GetString(reader.GetOrdinal("payments_paypal_pnref")),
                                            reader.IsDBNull(reader.GetOrdinal("payments_paypal_result")) ? 0 : reader.GetInt32(reader.GetOrdinal("payments_paypal_result")),
                                            reader.IsDBNull(reader.GetOrdinal("payments_paypal_authcode")) ? "" : reader.GetString(reader.GetOrdinal("payments_paypal_authcode"))
                                ));
                            }

                            if (!reader.IsDBNull(reader.GetOrdinal("transaction_fee")))
                            {
                                transaction.transaction_fee = reader.GetDecimal(reader.GetOrdinal("transaction_fee"));
                            }
                            else
                            {
                                transaction.transaction_fee = 0;
                            }
                            transactions.Add(transaction);
                        }
                    }
                }
            }
            catch (SqlException e)
            {
                Console.Write(e.Message);
            }
            return transactions;
        }

        internal static Transaction loadById(int id)
        {
            Transaction transaction = null;
            try
            {
                using (var command = Database.getConn().CreateCommand())
                {
                    command.CommandText =
                    @"SELECT 
                        transactions.id,
                        transactions.statuses_id,
                        transactions.total,
                        transactions.transaction_fee,
                        transactions.created_timestamp,
                        transactions.completed_timestamp,

                        users.id AS users_id,
                        users.name AS users_name,

                        registers.id AS registers_id,
                        registers.name AS registers_name,

                        locations.name AS locations_name,
                        locations.id AS locations_id,
                        locations.account_number AS locations_account_number,
                        hasInventory,
            
                        payments.id AS payments_id,
                        payments.timestamp AS payments_timestamp,
                        payments.payment_types_id AS payments_payment_types_id,
                        payments.amount_tendered AS payments_amount_tendered,
                        payments.paypal_pnref AS payments_paypal_pnref,
                        payments.paypal_result AS payments_paypal_result,
                        payments.paypal_authcode AS payments_paypal_authcode
                    FROM 
                        transactions 
                    LEFT JOIN registers ON transactions.registers_id = registers.id
                    LEFT JOIN locations ON registers.locations_id = locations.id
                    LEFT JOIN users ON transactions.users_id = users.id
                    LEFT JOIN payments ON transactions.id = payments.transactions_id
                    WHERE transactions.id = @id
                    ORDER BY completed_timestamp";

                    command.Parameters.AddWithValue("@id", id);

                    using (var reader = command.ExecuteReader())
                    {
                        if (reader.Read())
                        {
                            transaction = new Transaction
                            (
                                reader.GetInt32(reader.GetOrdinal("id")),
                                new Register
                                (
                                    reader.GetInt32(reader.GetOrdinal("registers_id")),
                                    new Location
                                    (
                                        reader.GetInt32(reader.GetOrdinal("locations_id")),
                                        reader.GetString(reader.GetOrdinal("locations_account_number")),
                                        reader.GetString(reader.GetOrdinal("locations_name")),
                                        reader.GetBoolean(reader.GetOrdinal("hasInventory"))
                                    ),
                                    reader.GetString(reader.GetOrdinal("registers_name"))
                                ),
                                new User
                                (
                                    reader.GetInt32(reader.GetOrdinal("users_id")),
                                    reader.GetString(reader.GetOrdinal("users_name"))
                                ),
                                reader.GetDecimal(reader.GetOrdinal("total")),
                                reader.GetDateTime(reader.GetOrdinal("created_timestamp")),
                                reader.GetDateTime(reader.GetOrdinal("completed_timestamp")),
                                Status.loadById(reader.GetInt32(reader.GetOrdinal("statuses_id")))
                            );

                            if (!reader.IsDBNull(reader.GetOrdinal("payments_id")))
                            {
                                transaction.payments.Add(new Payment(

                                            reader.GetInt32(reader.GetOrdinal("payments_id")),
                                            transaction,
                                            reader.GetDateTime(reader.GetOrdinal("payments_timestamp")),
                                            PaymentType.loadById(reader.GetInt32(reader.GetOrdinal("payments_payment_types_id"))),
                                            reader.IsDBNull(reader.GetOrdinal("payments_amount_tendered")) ? 0 : reader.GetDecimal(reader.GetOrdinal("payments_amount_tendered")),
                                            reader.IsDBNull(reader.GetOrdinal("payments_paypal_pnref")) ? "" : reader.GetString(reader.GetOrdinal("payments_paypal_pnref")),
                                            reader.IsDBNull(reader.GetOrdinal("payments_paypal_result")) ? 0 : reader.GetInt32(reader.GetOrdinal("payments_paypal_result")),
                                            reader.IsDBNull(reader.GetOrdinal("payments_paypal_authcode")) ? "" : reader.GetString(reader.GetOrdinal("payments_paypal_authcode"))
                                ));
                            }

                            if (!reader.IsDBNull(reader.GetOrdinal("transaction_fee")))
                            {
                                transaction.transaction_fee = reader.GetDecimal(reader.GetOrdinal("transaction_fee"));
                            }
                            else
                            {
                                transaction.transaction_fee = 0;
                            }
                        }
                    }

                    transaction.products = ProductDTO.loadByTransaction(transaction);
                    transaction.coupons = CouponDTO.loadByTransaction(transaction);
                }
            }
            catch (SqlException e)
            {
                Console.Write(e.Message);
            }
            return transaction;
        }
    }
}
