﻿using GreekFestPOS.Model;
using GreekFestPOS.POCO;
using GreekFestPOS.Util;
using GreekFestPOS.ViewModel;
using GreekFestPOS.Windows;
using GreekFestPOS.Windows.Admin;
using GreekFestPOS.Windows.User;
using Microsoft.PointOfService;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Drawing;
using System.Drawing.Printing;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Markup;

namespace GreekFestPOS
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        private static Dictionary<string, ConfigurationItem> _configItems = new Dictionary<string, ConfigurationItem>();

        public static ISplashScreen splashScreen;

        private ManualResetEvent ResetSplashCreated;
        private Thread SplashThread;

        private static User _currentUser;

        private static LoginWindow loginWindow;

        public static MainWindow mainWindow;

        protected override void OnStartup(StartupEventArgs e)
        {
            
            // ManualResetEvent acts as a block.  It waits for a signal to be set.
            ResetSplashCreated = new ManualResetEvent(false);

            // Create a new thread for the splash screen to run on
            SplashThread = new Thread(ShowSplash);
            SplashThread.SetApartmentState(ApartmentState.STA);
            SplashThread.IsBackground = true;
            SplashThread.Name = "Splash Screen";
            SplashThread.Start();

            // Wait for the blocker to be signaled before continuing. This is essentially the same as: while(ResetSplashCreated.NotSet) {}
            ResetSplashCreated.WaitOne();

            AppDomain.CurrentDomain.UnhandledException += unhandledExceptionHandler;
            this.DispatcherUnhandledException += dispatcherUnhandledExceptionHandler;
            base.OnStartup(e);
            FrameworkElement.StyleProperty.OverrideMetadata(typeof(Window), new FrameworkPropertyMetadata
            {
                DefaultValue = FindResource(typeof(Window))
            });

            
            FrameworkElement.LanguageProperty.OverrideMetadata(
                typeof(FrameworkElement),
                new FrameworkPropertyMetadata(
                XmlLanguage.GetLanguage(
                CultureInfo.CurrentCulture.IetfLanguageTag))
            );

            CultureInfo currentCulture = Thread.CurrentThread.CurrentCulture;
            CultureInfo newCulture = new CultureInfo(currentCulture.IetfLanguageTag);
            newCulture.NumberFormat.CurrencyNegativePattern = 1;
            Thread.CurrentThread.CurrentCulture = newCulture;
            Console.WriteLine((-111.98).ToString("c"));

            CultureInfo currentUICulture = Thread.CurrentThread.CurrentUICulture;
            CultureInfo newUICulture = new CultureInfo(currentUICulture.IetfLanguageTag);
            newUICulture.NumberFormat.CurrencyNegativePattern = 1;
            Thread.CurrentThread.CurrentUICulture = newUICulture;


            splashScreen.AddMessage("Reticulating Splines");
            posExplorer = new PosExplorer();

            splashScreen.AddMessage("Loading Configuration");

            loadLocalConfiguration();
            splashScreen.AddMessage("Connecting to Database");
            while(!Database.init())
            {
                DatabaseHostnameWindow db_window = new DatabaseHostnameWindow();
                db_window.ShowDialog();

                db_window = null;

                splashScreen.AddMessage("Reloading Configuration");
            }
            loadGlobalConfiguration();
            splashScreen.AddMessage("Logging in");
#if DEBUG
            register = RegisterDTO.loadById(10);
            currentUser = UserDTO.loadById(2);

            till = TillDTO.loadByRegisterAndStatus(register, TillStatus.OPEN);
            
            App.splashScreen.LoadComplete();
            App.openRegister();
#else
            if (register == null)
            {
                RegisterSelectionWindow window = new RegisterSelectionWindow();
                window.ShowDialog();
            }

            till = TillDTO.loadByRegisterAndStatus(register, TillStatus.OPEN);

            App.splashScreen.LoadComplete();
            loginWindow = new LoginWindow();
            loginWindow.ShowDialog();
#endif

        }

        private void ShowSplash()
        {
            // Create the window
            SplashScreenWindow splashScreen = new SplashScreenWindow();
            App.splashScreen = splashScreen;

            // Show it
            splashScreen.Show();

            // Now that the window is created, allow the rest of the startup to run
            ResetSplashCreated.Set();
            System.Windows.Threading.Dispatcher.Run();
        }

        private void unhandledExceptionHandler(object sender, UnhandledExceptionEventArgs args)
        {
            Exception e = (Exception)args.ExceptionObject;
            ExceptionWindow exceptionWindow = new ExceptionWindow();
            ExceptionViewModel exceptionVM = new ExceptionViewModel();
            this.Dispatcher.BeginInvoke((Action)(() =>
            {
                exceptionVM.exception = e;
                exceptionWindow.DataContext = exceptionVM;
                exceptionWindow.ShowDialog();
            Application.Current.Shutdown();
            }));
        }

        private void dispatcherUnhandledExceptionHandler(object sender, System.Windows.Threading.DispatcherUnhandledExceptionEventArgs args)
        {
            Exception e = (Exception)args.Exception;
            ExceptionWindow exceptionWindow = new ExceptionWindow();
            ExceptionViewModel exceptionVM = new ExceptionViewModel();
            exceptionVM.exception = e;
            exceptionWindow.DataContext = exceptionVM;
            exceptionWindow.ShowDialog(); 
            
            System.Diagnostics.Process proc = System.Diagnostics.Process.GetCurrentProcess();
            proc.Kill();
        }

        public static Register register
        {
            get;
            set;
        }

        public static PosExplorer posExplorer
        {
            get;
            set;
        }

        public static Till till
        {
            get;
            set;
        }

        public static User currentUser
        {
            get
            {
                return _currentUser;
            }
            set
            {
                _currentUser = value;
                if (mainWindow != null)
                {
                    ((MainWindowViewModel)mainWindow.DataContext).RaisePropertyChangedEvent("user");
                }
            }
        }

        public static Location location
        {
            get { return register.location; }
        }

        private void DataGridCell_PreviewMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            DataGridCell cell = sender as DataGridCell;

            if (!cell.IsEditing)
            {
                // enables editing on single click

                if (!cell.IsFocused)

                    cell.Focus();

                if (!cell.IsSelected)

                    cell.IsSelected = true;
            }

        }

        internal static void openRegister()
        {
            if(till == null)
            {
                if(currentUser.isAdmin)
                {
                    MessageBox.Show("There isn't currently an open till in this register. You will now be directed to admin menu so you can add a till to the register.");
                    AdminDashboardWindow window = new AdminDashboardWindow();

                    window.ShowDialog();

                    //11th hour change to prevent NPE if the user logged out from adminWindow
                    if(currentUser != null)
                    {
                        App.openRegister();
                    } else
                    {
                        App.logout();
                    }
                }
                else
                {
                    MessageBox.Show("There isn't currently an open till in this register. Please login as an admin to add a till to the register.");
                    App.logout();
                }
            }
            else
            {
                App.showMainWindow();
            }
        }

        public static void showMainWindow()
        {
            if(mainWindow == null)
            {
                mainWindow = new MainWindow();
            }
            if(!mainWindow.IsVisible)
            {
                mainWindow.Show();
            }
        }

        public static Dictionary<string, ConfigurationItem> configurationItems
        {
            get
            {
                if (_configItems.Count == 0)
                {
                    _configItems["cash_drawer_so"] = new LocalConfigurationItem<DeviceInfoConfiguration>("Cash Drawer SO");
                    _configItems["local_receipt_so"] = new LocalConfigurationItem<DeviceInfoConfiguration>("Receipt Printer SO");
                    _configItems["msr_so"] = new LocalConfigurationItem<DeviceInfoConfiguration>("MagStripe Reader SO");
                    _configItems["network_receipt_so"] = new LocalConfigurationItem<DeviceInfoConfiguration>("Network Printer SO");
                    _configItems["print_to_network_printer"] = new LocalConfigurationItem<bool>("Print to Network Printer");
                    _configItems["cash_notification_threshold"] = new LocalConfigurationItem<string>("Cash Notification Threshold");
                    _configItems["credit_card_receipt_threshold"] = new LocalConfigurationItem<string>("Credit Card Receipt Threshold");
                    _configItems["registers_id"] = new LocalConfigurationItem<int>("Register ID");
                    _configItems["database_hostname"] = new LocalConfigurationItem<string>("Database Hostname");

                    _configItems["payflow_host"] = new GlobalConfigurationItem<string>("PayPal Payflow Host", "payflow_host");
                    _configItems["credit_card_fee"] = new GlobalConfigurationItem<string>("Credit Card Fee", "credit_card_fee");
                    _configItems["cash_pickup_notification_email"] = new GlobalConfigurationItem<string>("Cash Pickup Notification Email", "cash_pickup_notification_email");
                    _configItems["medical_notification_email"] = new GlobalConfigurationItem<string>("Medical Notification Email", "medical_notification_email");
                }

                return _configItems;
            }
        }

        public static void persistConfiguration()
        {
            var configFile = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
            var settings = configFile.AppSettings.Settings;
            foreach (KeyValuePair<string, ConfigurationItem> entry in _configItems)
            {
                ConfigurationItem configItem = entry.Value;
                string key = entry.Key;
                if (Util.Util.IsInstanceOfGenericType(typeof(ConfigurationItem<>), configItem))
                {
                    var type = configItem.GetType();

                    var valueProperty = type.GetProperty("value");
                    object value = valueProperty.GetValue(configItem, new object[] { });

                    if (value != null)
                    {
                        if (Util.Util.IsInstanceOfGenericType(typeof(LocalConfigurationItem<>), configItem))
                        {
                            if (settings[key] == null)
                            {
                                settings.Add(key, value.ToString());
                            }
                            else
                            {
                                settings[key].Value = value.ToString();
                            }
                        }
                        else if (Util.Util.IsInstanceOfGenericType(typeof(GlobalConfigurationItem<>), configItem))
                        {
                            using (var command = Database.getConn().CreateCommand())
                            {
                                command.CommandText = @"MERGE dbo.configuration as target
                                                    USING (SELECT @name, @key, @value) as source (name, [key], value)
                                                    ON (target.[key] = source.[key])
                                                    WHEN MATCHED THEN
                                                        UPDATE SET 
                                                            value = source.value,
				                                            last_updated_timestamp = GETDATE()
                                                    WHEN NOT MATCHED THEN
                                                        INSERT (name, [key], value)
                                                        VALUES(source.name, source.[key], source.value);";
                                command.Parameters.AddWithValue("@value", value.ToString());
                                command.Parameters.AddWithValue("@key", key);
                                command.Parameters.AddWithValue("@name", configItem.name);

                                command.ExecuteNonQuery();
                            }
                        }
                    }
                }
            }
            configFile.Save(ConfigurationSaveMode.Modified);
            ConfigurationManager.RefreshSection(configFile.AppSettings.SectionInformation.Name);
        }

        public static void loadLocalConfiguration()
        {
            //Local Configuration
            var configFile = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
            var settings = configFile.AppSettings.Settings;

            if (settings["local_receipt_so"] != null && settings["local_receipt_so"].Value != "")
            {
                DeviceInfo posPrinter = posExplorer.GetDevice(DeviceType.PosPrinter, settings["local_receipt_so"].Value);
                if (posPrinter != null)
                {
                    ((LocalConfigurationItem<DeviceInfoConfiguration>)configurationItems["local_receipt_so"]).value = new DeviceInfoConfiguration(posPrinter);

                    Printer.init(posExplorer.CreateInstance(posPrinter));
                }
            }

            if (settings["cash_drawer_so"] != null && settings["cash_drawer_so"].Value != "")
            {
                DeviceInfo cashDrawer = posExplorer.GetDevice(DeviceType.CashDrawer, settings["cash_drawer_so"].Value);
                if (cashDrawer != null)
                {
                    ((LocalConfigurationItem<DeviceInfoConfiguration>)configurationItems["cash_drawer_so"]).value = new DeviceInfoConfiguration(cashDrawer);

                    Drawer.init(posExplorer.CreateInstance(cashDrawer));
                }
            }
            if (settings["msr_so"] != null)
            {
                DeviceInfo msr = posExplorer.GetDevice(DeviceType.Msr, settings["msr_so"].Value);
                if (msr != null)
                {
                    ((LocalConfigurationItem<DeviceInfoConfiguration>)configurationItems["msr_so"]).value = new DeviceInfoConfiguration(msr);

                    Magstripe.init(posExplorer.CreateInstance(msr));
                }
            }
            if (settings["network_receipt_so"] != null)
            {
                DeviceInfo posPrinter = posExplorer.GetDevice(DeviceType.PosPrinter, settings["network_receipt_so"].Value);
                if (posPrinter != null)
                {
                    ((LocalConfigurationItem<DeviceInfoConfiguration>)configurationItems["network_receipt_so"]).value = new DeviceInfoConfiguration(posPrinter);

                    //Printer.setPrinter(posExplorer.CreateInstance(posPrinter));
                }
            }
            if (settings["print_to_network_printer"] != null)
            {
                ((LocalConfigurationItem<bool>)configurationItems["print_to_network_printer"]).value = bool.Parse(settings["print_to_network_printer"].Value);
            }
            if (settings["cash_notification_threshold"] != null)
            {
                ((LocalConfigurationItem<string>)configurationItems["cash_notification_threshold"]).value = settings["cash_notification_threshold"].Value;
            }
            if (settings["credit_card_receipt_threshold"] != null)
            {
                ((LocalConfigurationItem<string>)configurationItems["credit_card_receipt_threshold"]).value = settings["credit_card_receipt_threshold"].Value;
            }
            if (settings["database_hostname"] != null)
            {
                ((LocalConfigurationItem<string>)configurationItems["database_hostname"]).value = settings["database_hostname"].Value;
            }
            if (settings["registers_id"] != null)
            {
                ((LocalConfigurationItem<int>)configurationItems["registers_id"]).value = int.Parse(settings["registers_id"].Value);
                int registers_id = int.Parse(settings["registers_id"].Value);

                Register register = RegisterDTO.loadById(registers_id);
                if(register != null)
                {
                    App.register = register;
                }
            }
        }

        public static void loadGlobalConfiguration()
        {
            if (Database.connected)
            {
                //Global Configuration
                using (var command = Database.getConn().CreateCommand())
                {
                    command.CommandText = "SELECT * FROM configuration";

                    using (var reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            GlobalConfigurationItem<string> configItem = new GlobalConfigurationItem<string>(reader["name"].ToString(), reader["key"].ToString());
                            configItem.value = reader["value"].ToString();

                            configurationItems[reader["key"].ToString()] = configItem;
                        }
                    }
                }
            }
        }

        internal static object getConfig(string key)
        {
            ConfigurationItem configItem = configurationItems[key];
            if (Util.Util.IsInstanceOfGenericType(typeof(ConfigurationItem<>), configItem))
            {
                var type = configItem.GetType();

                var valueProperty = type.GetProperty("value");
                object value = valueProperty.GetValue(configItem, new object[] { });
                return value;
            }



            return null;
        }

        public static void logout()
        {
            if (loginWindow == null)
            {
                loginWindow = new LoginWindow();
            }
            loginWindow.Show();
            ((LoginViewModel)loginWindow.DataContext).pin = "";
            App.currentUser = null;
        }

        internal static void sendException(PosException e)
        {
            MailMessage mail = new MailMessage("andreaslekas@gmail.com", "andreaslekas+greekfestpos@gmail.com");
            mail.Subject = "Exception on "+App.register.name+", cashier "+App.currentUser.name;
            mail.Body = e.Message + "\n\n" + e.StackTrace;
            SmtpClient client = new SmtpClient();
            client.Host = "smtp.googlemail.com";
            client.Port = 587;
            client.UseDefaultCredentials = false;
            client.DeliveryMethod = SmtpDeliveryMethod.Network;
            client.EnableSsl = true;
            client.Credentials = new NetworkCredential("andreaslekas@gmail.com", "bootee-ascribe-skyjack-verdure");
            client.Send(mail);
        }
    }
}
