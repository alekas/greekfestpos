﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GreekFestPOS.Enums
{
    public enum DollarOrPercent { Dollar, Percent };
    public enum ModificationType { Upcharge, Discount };
}
