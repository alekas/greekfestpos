﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.PointOfService;
using MagTekServiceObject;
using System.Windows;

namespace GreekFestPOS.Util
{
    class Magstripe
    {
        protected static Msr magstripe;

        public delegate void CardSwipedDelegate(Msr msr);

        private static CardSwipedDelegate callback;

        public static void init(PosDevice msr)
        {
            magstripe = (Msr)msr;

            if (magstripe.State == ControlState.Closed)
            {
                try
                {
                    magstripe.Open();
                    magstripe.Claim(100);
                    magstripe.DecodeData = true;
                    magstripe.AutoDisable = true;
                    magstripe.DataEvent += Magstripe.MSRDataEventHandler;
                    magstripe.ErrorEvent += Magstripe.MSRErrorEventHandler;
                }
                catch (Exception e)
                {
                    magstripe = null;
                    MessageBox.Show("Card reader could not be opened. Please make sure it is plugged in and configured properly.\n\n Error Message: "+e.Message, "Card Reader Failed to Initialize", MessageBoxButton.OK, MessageBoxImage.Error);
                }
            }
        }

        private static void MSRErrorEventHandler(object sender, DeviceErrorEventArgs e)
        {
            if (magstripe == null)
            {
                return;
            }
            Msr msr = (Msr)sender;

            if(msr.ErrorReportingType == MsrErrorReporting.Card)
            {
                MessageBox.Show("There was a problem with the card swipe. Please try again.");
            }
        }

        public static void enableDataEvent()
        {
            if(magstripe==null)
            {
                return;
            }
            if(!magstripe.DeviceEnabled)
            {
                magstripe.DeviceEnabled = true;
            }
            if (!magstripe.DataEventEnabled)
            {
                magstripe.DataEventEnabled = true;
            }
        }

        public static void setCardSwipedDelegate(CardSwipedDelegate callback)
        {
            if (magstripe == null)
            {
                return;
            }
            Magstripe.callback = callback;
        }

        private static void MSRDataEventHandler(object sender, Microsoft.PointOfService.DataEventArgs e)
        {
            Msr msr = (Msr)sender;

            if(callback != null)
            {
                callback(msr);
            }
        }

        public static void destroy()
        {
            if (magstripe != null)
            {
                magstripe.DataEventEnabled = false;
                magstripe.DeviceEnabled = false;
                magstripe.Release();
                magstripe.Close();
                magstripe = null;
            }
        }
    }
}
