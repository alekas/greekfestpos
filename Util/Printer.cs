﻿using GreekFestPOS.Model;
using GreekFestPOS.POCO;
using Microsoft.PointOfService;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Printing;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GreekFestPOS.Util
{
    class Printer
    {
        protected static PosPrinter printer;
        protected const string ESC = "\x1B";
        protected const string GS = "\x1D";
        protected const string UNDERLINE = ESC + "|uC";
        protected const string DOUBLE_WIDE = ESC + "|2C";
        protected const string SUBSCRIPT = ESC + "|tbC";
        protected const string BOLD = ESC + "|bC";

        protected static int linesToPaperCut = 2;
        private static int lineChars = 48;
        private static int[] lineWidths = {48};
        public static void init(PosDevice posDevice)
        {
            try
            {
                printer = (PosPrinter)posDevice;
                printer.Open();
                printer.Claim(100);
                printer.DeviceEnabled = true;

                if (printer.RecLinesToPaperCut != 0)
                {
                    linesToPaperCut = printer.RecLinesToPaperCut;
                }
                if (printer.RecLineChars != 0)
                {
                    lineChars = printer.RecLineChars;
                }

                if (printer.RecLineCharsList.Length != 0)
                {
                    lineWidths = printer.RecLineCharsList;
                }
            }
            catch (Exception e)
            {
                printer = null;
                MessageBox.Show("Printer could not be opened. Please make sure it is plugged in and configured properly.\n\n Error Message: " + e.Message, "Printer Failed to Initialize", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            
        }

        public static void printCompanyInformation()
        {
            printLine(CenteredText("St. Augustine Greek Festival"));
            printLine(CenteredText("Holy Trinity Greek Orthodox Church"));
            printLine(CenteredText("2940 CR 214") + "\n");
            printLine(CenteredText("St. Augustine, Florida 32084"));
            printLine(CenteredText("(904) 829-0504"));
        }

        public static void printTransaction(Transaction transaction, bool withSignatureLine = false)
        {
            if(Printer.printer == null)
            {
                return;
            }
            beginTransaction();
            printHeader(transaction);

            foreach (Product product in transaction.products)
            {
                printLine(SplitText(product.name, product.price.ToString("C")));
                if (product.isModified)
                {
                    if(product.priceModification != null)
                    {
                        PriceModification pm = product.priceModification;

                        printLine(SUBSCRIPT+SplitText("  "+pm.description, (pm.calculatePrice(product.price) - product.price).ToString("c")));

                        printLine(" ");
                    }
                }
            }
            
            foreach (Coupon coupon in transaction.coupons)
            {
                printLine(SplitText("Coupon: "+coupon.name, coupon.amount.ToString("C")));
            }

            printLine(UNDERLINE + new String(' ', 48));

            lineFeed();

            if(transaction.firstPayment.paymentType == PaymentType.CREDIT)
            {
                printLine(SplitText("Subtotal", transaction.subtotal.ToString("C")));
                printLine(SplitText("Credit Card Fee", transaction.transaction_fee.ToString("C")));
            }

            lineFeed();

            printLine(DOUBLE_WIDE + BOLD + SplitText(lineChars/2, "Total", transaction.calculatedTotal.ToString("C")));

            lineFeed();

            if(transaction.firstPayment.paymentType == PaymentType.CASH)
            {
                printLine(SplitText("CASH", transaction.firstPayment.amountTendered.ToString("C")));
                printLine(SplitText("CHANGE", (transaction.firstPayment.amountTendered - transaction.calculatedTotal).ToString("C")));
            }
            else
            {
                printLine(SplitText(transaction.firstPayment.creditCardType.ToString().ToUpper(), transaction.firstPayment.amountTendered.ToString("C")));

                if (transaction.firstPayment.paymentProcessor == "paypal")
                {
                    printLine(SplitText("AUTH", transaction.firstPayment.paypalAuthCode == null ? "" : transaction.firstPayment.paypalAuthCode));
                    printLine(SplitText("TRANSACTION #", transaction.firstPayment.paypalPNRef));
                } else if(transaction.firstPayment.paymentProcessor == "payeezy")
                {
                    printLine(SplitText("TAG", transaction.firstPayment.payeezyTransactionTag == null ? "" : transaction.firstPayment.payeezyTransactionTag));
                    printLine(SplitText("TRANSACTION #", transaction.firstPayment.payeezyTransactionId));
                }

                if (withSignatureLine)
                {
                    printLine(" ");
                    printLine("I agree to pay the above total amount according to the card issuer agreement");
                    printLine(" ");
                    printLine(" ");
                    printLine(UNDERLINE + "x" + new String(' ', 47));
                    printLine(CenteredText("Merchant Copy"));
                }
            }

            feedAndCut();
            endTransaction();
        }

        public static void printToNetwork(Transaction transaction)
        {
            printToPrinter(transaction, "P11-USL", false);

            List<string> printersUsed = new List<string>();
            foreach (Product product in transaction.products)
            {
                if (product.printDuplicate && !printersUsed.Contains(product.duplicatePrinter))
                {
                    printToPrinter(transaction, product.duplicatePrinter, true);
                    printersUsed.Add(product.duplicatePrinter);
                }
            }
        }

        public static void printToPrinter(Transaction transaction, string printer, bool duplicate)
        {
            bool productsToPrint = false;
            foreach (Product product in transaction.products)
            {
                if (product.printToKitchen)
                {
                    productsToPrint = true;
                    break;
                }

                if (product.printDuplicate && duplicate && product.duplicatePrinter.Equals(printer))
                {
                    productsToPrint = true;
                    break;
                }

            }

            if (!productsToPrint)
            {
                return;
            }

            PrintDocument p = new PrintDocument();
            p.PrinterSettings.PrinterName = printer;

            Font font12 = new Font("Courier New", 12);
            Font font20 = new Font("Courier New", 16);

            float lineheight12 = font12.GetHeight() + 4;
            float lineheight20 = font20.GetHeight() + 4;

            float startX = 0;
            float startY = 4;
            float yOffset = 0;
            
            StringFormat formatCenter = new StringFormat(StringFormatFlags.NoClip);   
            formatCenter.Alignment = StringAlignment.Center;

            SizeF layoutSize = new SizeF(p.DefaultPageSettings.PrintableArea.Width - yOffset * 2, lineheight12);
            RectangleF layout = new RectangleF(new PointF(startX, startY + yOffset), layoutSize);

            Brush brush = Brushes.Black;

            p.PrintPage += delegate(object sender1, PrintPageEventArgs e1)
            {
                Graphics g = e1.Graphics;

                e1.Graphics.DrawString(" _ _ _ _ _ _ _ _ _ _ _ _ _", font20, brush, layout, formatCenter);
                yOffset += lineheight20;
                if (duplicate)
                {
                    e1.Graphics.DrawString(" _ _ _DUPLICATE_ _ _", font20, brush, layout, formatCenter);
                }
                yOffset += lineheight20;
                layout = new RectangleF(new PointF(startX, startY + yOffset), layoutSize);
                e1.Graphics.DrawString("Date/Time: " + DateTime.Now.ToString("M/d hh:mm tt"), font12, brush, layout);
                yOffset += lineheight12;
                layout = new RectangleF(new PointF(startX, startY + yOffset), layoutSize);
                e1.Graphics.DrawString("Register: " + transaction.register.name, font12, brush, layout);
                yOffset += lineheight12;
                layout = new RectangleF(new PointF(startX, startY + yOffset), layoutSize);
                e1.Graphics.DrawString("Cashier: " + App.currentUser.name, font12, brush, layout);
                yOffset += lineheight12;
                layout = new RectangleF(new PointF(startX, startY + yOffset), layoutSize);

                e1.Graphics.DrawString(" _ _ _ _ _ _ _ _ _ _ _ _ _", font20, brush, layout, formatCenter);
                yOffset += lineheight20;
                layout = new RectangleF(new PointF(startX, startY + yOffset), layoutSize);

                e1.Graphics.DrawString("    ", font20, brush, layout, formatCenter);
                yOffset += lineheight20;
                layout = new RectangleF(new PointF(startX, startY + yOffset), layoutSize);

                e1.Graphics.DrawString("    ", font20, brush, layout, formatCenter);
                yOffset += lineheight20;
                layout = new RectangleF(new PointF(startX, startY + yOffset), layoutSize);

                foreach (Product product in transaction.products)
                {
                    if(duplicate)
                    {
                        //we're printing a duplicate ticket at a different printer than the kitchen

                        //this product is configured to not print a duplicate, so let's skip it
                        if(!product.printDuplicate)
                        {
                            continue;
                        }
                        else
                        {
                            //This product IS configured to print a duplicate, so let's make sure we're on the correct printer
                            if(!product.duplicatePrinter.Equals(printer))
                            {
                                //skip if not on the correct printer
                                continue;
                            }
                        }
                    }
                    else
                    {
                        //we're printing the kitchen ticket, so let's make sure we only print kitchen products
                        if(!product.printToKitchen)
                        {
                            continue;
                        }
                    }
                    g.DrawString(product.name, font12, brush, layout);
                    yOffset += lineheight12;
                    layout = new RectangleF(new PointF(startX, startY + yOffset), layoutSize);
                    if (product.isModified)
                    {
                        if (product.priceModification != null)
                        {
                            PriceModification pm = product.priceModification;

                            g.DrawString("  " + pm.description, font12, brush, layout);
                            
                            yOffset += lineheight12;
                            layout = new RectangleF(new PointF(startX, startY + yOffset), layoutSize);
                        }
                    }
                }
                
                foreach (Coupon coupon in transaction.coupons)
                {
                    g.DrawString("Coupon: " + coupon.name, font12, brush, layout);
                    yOffset += lineheight12;
                    layout = new RectangleF(new PointF(startX, startY + yOffset), layoutSize);
                }

                e1.Graphics.DrawString("    ", font20, brush, layout, formatCenter);
                yOffset += lineheight20;
                layout = new RectangleF(new PointF(startX, startY + yOffset), layoutSize);

                e1.Graphics.DrawString("Order Number", font20, brush, layout, formatCenter);
                yOffset += lineheight20;
                layout = new RectangleF(new PointF(startX, startY + yOffset), layoutSize);

                e1.Graphics.DrawString(transaction.id.ToString(), font20, brush, layout, formatCenter);
                yOffset += lineheight20;
                layout = new RectangleF(new PointF(startX, startY + yOffset), layoutSize);

                e1.Graphics.DrawString("   ", font20, brush, layout, formatCenter);
                yOffset += lineheight20;
                layout = new RectangleF(new PointF(startX, startY + yOffset), layoutSize);

                e1.Graphics.DrawString("    ", font20, brush, layout, formatCenter);
                yOffset += lineheight20;
                layout = new RectangleF(new PointF(startX, startY + yOffset), layoutSize);

                e1.Graphics.DrawString(" _ _ _ _ _ _ _ _ _ _ _ _ _", font20, brush, layout, formatCenter);
                yOffset += lineheight20;
                layout = new RectangleF(new PointF(startX, startY + yOffset), layoutSize);

            };
            try
            {
                p.Print();
            }
            catch (Exception ex)
            {
                throw new Exception("Exception Occured While Printing", ex);
            }
        }

        private static void feedAndCut()
        {
            lineFeed(linesToPaperCut);
            cut();
        }

        public static void printHeader(Transaction transaction = null)
        {
            if (Printer.printer == null)
            {
                return;
            }
            DialogResult dialogResult;
            try
            {
                if (printer.CapRecPresent == true)
                {

                    while (true)
                    {
                        try
                        {
                            printCompanyInformation();

                            print("\u001b|200uF");

                            printLine(SplitText(DateTime.Now.ToString("MM/dd/yyyy"), DateTime.Now.ToString("hh:mm:ss tt")));
                            printLine(SplitText("Location: " + App.location.name, "Cashier: " + App.currentUser.name));
                            printLine(UNDERLINE + SplitText("Register: " + App.register.name, (transaction != null ? "Ticket #: "+transaction.id.ToString("####") : "")));

                            printer.PrintNormal(PrinterStation.Receipt, "\u001b|500uF");
                            break;
                        }
                        catch (PosControlException)
                        {
                            // When error occurs, display a message to ask the user whether retry or not.
                            dialogResult = MessageBox.Show("Failed to output to printer.\n\nRetry?"
                                , "Printer_SampleStep16", MessageBoxButtons.AbortRetryIgnore);

                            if (dialogResult == DialogResult.Abort)
                            {
                                try
                                {
                                    // Clear the buffered data since the buffer retains print data when an error occurs during printing.
                                    printer.ClearOutput();
                                }
                                catch (PosControlException)
                                {
                                }
                                return;
                            }
                            else if (dialogResult == DialogResult.Ignore)
                            {
                                break;
                            }
                            try
                            {
                                // Clear the buffered data since the buffer retains print data when an error occurs during printing.
                                printer.ClearOutput();
                            }
                            catch (PosControlException)
                            {
                            }
                            continue;
                        }

                    }

                    //print all the buffer data. and exit the batch processing mode.
                    while (printer.State != ControlState.Idle)
                    {
                        try
                        {
                            System.Threading.Thread.Sleep(100);
                        }
                        catch (Exception)
                        {
                        }
                    }
                }
                else
                {
                    MessageBox.Show("Cannot use a Receipt Stateion.", "Printer_SampleStep16"
                        , MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return;
                }
            }
            catch (PosControlException)
            {
                // Clear the buffered data since the buffer retains print data when an error occurs during printing.
                printer.ClearOutput();
            }
        }

        private static string SplitText(int characters_per_line, string p1, string p2)
        {
            int middle_offset = characters_per_line - p1.Length - p2.Length;

            string offset_string = new String(' ', middle_offset);

            return p1 + offset_string + p2;
        }

        private static string SplitText(string p1, string p2)
        {
            return SplitText(lineChars, p1, p2);
        }

        private static string CenteredText(int characters_per_line, string p)
        {
            int offset = (characters_per_line - p.Length) / 2;
            String offset_string = new String(' ', offset);
            return offset_string + p + offset_string;
        }

        private static string CenteredText(string p)
        {
            return CenteredText(lineChars, p);
        }
        
        private static long GetRecLineChars(ref int[] RecLineChars)
        {
            long lRecLineChars = 0;
            long lCount;
            int i;

            // Calculate the element count.
            lCount = lineWidths.GetLength(0);

            if (lCount == 0)
            {
                lRecLineChars = 0;
            }
            else
            {
                if (lCount > 2)
                {
                    lCount = 2;
                }

                for (i = 0; i < lCount; i++)
                {
                    RecLineChars[i] = lineWidths[i];
                }

                lRecLineChars = lCount;
            }

            return lRecLineChars;
        }

        public static String MakePrintString(int iLineChars, String strBuf, String strPrice)
        {
            int iSpaces = 0;
            String tab = "";
            try
            {
                iSpaces = iLineChars - (strBuf.Length + strPrice.Length);
                for (int j = 0; j < iSpaces; j++)
                {
                    tab += " ";
                }
            }
            catch (Exception)
            {
            }
            return strBuf + tab + strPrice;
        }

        public static void beginTransaction()
        {
            if (Printer.printer == null)
            {
                return;
            }
            printer.TransactionPrint(PrinterStation.Receipt, PrinterTransactionControl.Transaction);

            printer.RecLetterQuality = true;
            printer.MapMode = MapMode.Metric;
        }

        public static void endTransaction()
        {
            if (Printer.printer == null)
            {
                return;
            }
            printer.TransactionPrint(PrinterStation.Receipt, PrinterTransactionControl.Normal);
        }

        public static void lineFeed(int numberOfLines = 1)
        {
            print(ESC + "|"+numberOfLines+"lF");
        }

        public static void print(string data)
        {
            if (Printer.printer == null)
            {
                return;
            }
            printer.PrintNormal(PrinterStation.Receipt, data);
        }

        public static void printLine(string data)
        {
            print(data + "\n");
        }

        public static void cut()
        {
            print(ESC + "|95fP");
        }

        internal static void printZTape()
        {
            if (Printer.printer == null)
            {
                return;
            }
            beginTransaction();
            int transactionCount = TillDTO.getCurrentTransactionCount();

            print(SplitText("Date: ", DateTime.Now.ToString("MM/dd/yyyy")));
            print(SplitText("Time: ", DateTime.Now.ToString("hh:mm tt")));
            print(SplitText("Cashier: ", App.currentUser.name));
            print(SplitText("Register: ", App.register.name));
            print(UNDERLINE + SplitText("Location: ", App.location.name));
            print(new String(' ', 48));
            print(SplitText("Transactions: ", transactionCount.ToString()));
            print(SplitText("Sales: ", App.till.total_sales == null ? "$0.00" : ((decimal)App.till.total_sales).ToString("C")));
            print(SplitText("Credit Card: ", App.till.total_credit_card_sales == null ? "$0.00" : ((decimal)App.till.total_credit_card_sales).ToString("C")));
            print(SplitText("Credit Card Transaction Fees: ", App.till.total_credit_card_fees == null ? "$0.00" : ((decimal)App.till.total_credit_card_fees).ToString("C")));
            print(SplitText("Cash: ", App.till.total_cash_sales == null ? "$0.00" : ((decimal)App.till.total_cash_sales).ToString("C")));
            print(SplitText("Coupon Sales: ", App.till.total_coupon_sales == null ? "$0.00" : ((decimal)App.till.total_coupon_sales).ToString()));
            print(SplitText("Coupons: ", App.till.total_coupons == null ? "0" : ((decimal)App.till.total_coupons).ToString()));

            feedAndCut();
            endTransaction();
        }

        internal static bool printTest()
        {
            if(printer==null)
            {
                return false;
            }
            print("ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890!@#$%^&*()");
            feedAndCut();

            return true;
        }
    }
}
