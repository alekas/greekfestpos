﻿using GreekFestPOS.POCO;
using Microsoft.PointOfService;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;

namespace GreekFestPOS.Util
{
    public class ConfigurationItemTemplateSelector : DataTemplateSelector
    {
        public DataTemplate DeviceTemplate
        { get; set; }
        public DataTemplate BooleanTemplate
        { get; set; }
        public DataTemplate StringTemplate
        { get; set; }
        public DataTemplate IntegerTemplate
        { get; set; }

        public override DataTemplate SelectTemplate(object item, DependencyObject container)
        {
            ConfigurationItem obj = item as ConfigurationItem;

            if (obj != null)
            {
                if (obj is ConfigurationItem<DeviceInfoConfiguration>)
                {
                    return DeviceTemplate;
                }
                if (obj is ConfigurationItem<bool>)
                {
                    return BooleanTemplate;
                }
                if (obj is ConfigurationItem<string>)
                {
                    return StringTemplate;
                }
                if (obj is ConfigurationItem<int>)
                {
                    return IntegerTemplate;
                }

                if(Util.IsInstanceOfGenericType(typeof(ConfigurationItem<>), obj))
                {
                    var type = obj.GetType();

                    var valueProperty = type.GetProperty("value");
                    object data = valueProperty.GetValue(obj, new object[] { });

                    base.SelectTemplate(data, container);
                }

                return base.SelectTemplate(item, container);
            }
            else
                return base.SelectTemplate(item, container);
        }
    }
}
