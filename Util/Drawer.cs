﻿using GreekFestPOS.Model;
using GreekFestPOS.POCO;
using Microsoft.PointOfService;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GreekFestPOS.Util
{
    class Drawer
    {
        protected static CashDrawer drawer;
        public static void init(PosDevice posDevice)
        {
            try
            {
                drawer = (CashDrawer)posDevice;
                drawer.Open();
                drawer.Claim(100);
                drawer.DeviceEnabled = true;
            }
            catch (Exception e)
            {
                drawer = null;
                MessageBox.Show("Drawer could not be opened. Please make sure it is plugged in and configured properly.\n\n Error Message: "+e.Message, "Drawer Failed to Initialize", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public static void openDrawer()
        {
            try
            {
                if (drawer != null)
                {
                    drawer.OpenDrawer();
                }
            }
            catch(PosException e)
            {
                App.sendException(e);
            }
        }

        internal static bool testOpen()
        {
            if(drawer==null)
            {
                return false;
            }

            drawer.OpenDrawer();

            return true;
        }
    }
}
