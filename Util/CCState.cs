﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GreekFestPOS.Util
{
    public sealed class CCState
    {

        private readonly String name;
        private readonly int value;

        public static readonly CCState WAITING_FOR_SWIPE = new CCState(1, "Swipe Card or Type Credit Card Number");
        public static readonly CCState AUTHORIZING = new CCState(2, "Authorizing");
        public static readonly CCState APPROVED = new CCState(3, "Approved");
        public static readonly CCState DENIED = new CCState(4, "Declined");
        public static readonly CCState SWIPE_ERROR = new CCState(5, "Swipe Error");

        private CCState(int value, String name)
        {
            this.name = name;
            this.value = value;
        }

        public override String ToString()
        {
            return name;
        }
    }
}
