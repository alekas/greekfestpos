﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GreekFestPOS.Util
{
    public sealed class TillStatus
    {

        public readonly String name;
        public readonly int id;

        public static readonly TillStatus OPEN = new TillStatus(1, "Open");
        public static readonly TillStatus CLOSED = new TillStatus(2, "Closed");

        private TillStatus(int id, String name)
        {
            this.name = name;
            this.id = id;
        }

        public static TillStatus loadById(int id)
        {
            if(id==OPEN.id)
            {
                return OPEN;
            }
            else if (id == CLOSED.id)
            {
                return CLOSED;
            }

            return null;
        }

        public override String ToString()
        {
            return name;
        }
    }
}
