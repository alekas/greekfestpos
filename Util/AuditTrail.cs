﻿using GreekFestPOS.POCO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GreekFestPOS.Util
{
    class AuditTrail
    {
        private static void log(string type, string message, User user)
        {
            using(var command = Database.getConn().CreateCommand())
            {
                command.CommandText = "INSERT INTO audit_trail (type, message, users_id, timestamp) VALUES (@type, @message, @users_id, GETDATE())";
                command.Parameters.AddWithValue("@type", type);
                command.Parameters.AddWithValue("@message", message);

                if (user == null)
                {
                    command.Parameters.AddWithValue("@users_id", DBNull.Value);
                }
                else
                {
                    command.Parameters.AddWithValue("@users_id", user.id);
                }

                command.ExecuteNonQuery();
            }
        }

        public static void info(string message)
        {
            log("INFO", message, App.currentUser);
        }

        public static void action(string action, string description="")
        {
            if(description!="")
            {
                action += ": " + description;
            }
            log("INFO", action, App.currentUser);
        }

        public static void error(string message)
        {
            log("ERROR", message, App.currentUser);
        }
    }
}
