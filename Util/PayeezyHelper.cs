﻿using Clifton.Payment;
using Clifton.Payment.Gateway;
using GreekFestPOS.POCO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using static Clifton.Payment.Gateway.PayeezyGateway;

namespace GreekFestPOS.Util
{
    class PayeezyHelper
    {
        private static Regex regex = new Regex(@"^%?(?<FormatCode>.)(?<PrimaryAccountNumber>[\d]{1,19})\^(?<Name>.{2,26})\^(?<ExpirationDate>[\d]{0,4}|\^)(?<ServiceCode>[\d]{0,3}|\^)(?<DiscretionaryData>.*)\??;(?<PrimaryAccountNumber2>[\d]{1,19})=(?<ExpirationDate2>[\d]{0,4}|=)(?<ServiceCode2>[\d]{0,3}|=)(?<DiscretionaryData2>.*)\??\Z");

        public static Response processTransaction(Transaction transaction, string swipeData)
        {

#if DEBUG
            swipeData = "%B4111111111111111^DOE/ JOHN                 ^19061010000000      00852000000?;4111111111111111=19061010000000852?";
#endif

            string track1 = swipeData.Substring(0, swipeData.IndexOf(';'));
            string track2 = swipeData.Substring(swipeData.IndexOf(';'));

            MatchCollection matches = regex.Matches(swipeData);
            if (matches.Count > 0)
            {
                foreach (Match match in matches)
                {
                    string formatCode = match.Groups["FormatCode"].Value;
                    string primaryAccountNumber = match.Groups["PrimaryAccountNumber"].Value;
                    string name = match.Groups["Name"].Value;
                    string expirationDate = match.Groups["ExpirationDate"].Value;
                    string expirationYear = expirationDate.Substring(0, 2);
                    string expirationMonth = expirationDate.Substring(2);
                    string serviceCode = match.Groups["ServiceCode"].Value;
                    string discretionaryData = match.Groups["DiscretionaryData"].Value;

                    decimal transaction_fee = decimal.Parse((string)App.getConfig("credit_card_fee"));

                    decimal totalAmount = Math.Round(transaction.calculatedTotal + transaction_fee, 2);


                    #if DEBUG
                                PayeezyGateway gw = new PayeezyGateway(
                                        apiKey: "WxCVNWlOHtkujzDRvncAISuZqdSe4jGJ",
                                        apiSecret: "87486dc9fc72c651cdb95254752cf567f39afd24a88bb496213cda88f87d457c",
                                        token: "fdoa-512fe3c70168dcbcc8cd34efb3e3d992512fe3c70168dcbc",
                                        environment: PayeezyEnvironment.Certification
                                    );
                    #else
                                        PayeezyGateway gw = new PayeezyGateway(
                                                apiKey: "NDo26B5AGVuEWOp0qZe2zGeWBeB7SLcf",
                                                apiSecret: "47179083810ecfdd4a8d48bc77bdbabc384dd2db7ddf9e57b6b11a07e822191f",
                                                token: "fdoa-e0b531ea1675d9897f0894a4548f4a2ae0b531ea1675d989",
                                                environment: PayeezyEnvironment.Production
                                            );
                    #endif
                    Response resp = gw.CreditCardPurchase(primaryAccountNumber, expirationMonth, expirationYear, totalAmount.ToString(), name, "", transaction.id.ToString());

                    if (resp.ParsedBankResponseCode == BankResponseCode.Approved && resp.ParsedGatewayResponseCode == GatewayResponseCode.TransactionNormal)
                    {
                        DateTime parsedExpirationDate;

                        CreditCardType cardType = gw.ValidateAndParseCardDetails(primaryAccountNumber, expirationMonth, expirationYear, out parsedExpirationDate);

                        transaction.payments.Add(new Payment(0, transaction, DateTime.Now, PaymentType.CREDIT, totalAmount, cardType, resp));
                        transaction.transaction_fee = transaction_fee;
                    }

                    return resp;
                }
            }

            return null;
        }

        public static Response processTransaction(Transaction transaction, string creditCardNumber, string expirationDate)
        {
            
            decimal transaction_fee = decimal.Parse((string)App.getConfig("credit_card_fee"));

            decimal totalAmount = Math.Round(transaction.calculatedTotal + transaction_fee, 2);

            expirationDate = expirationDate.Replace("/", "");

            string expirationMonth = expirationDate.Substring(0, 2);
            string expirationYear = expirationDate.Substring(2);
#if DEBUG
            PayeezyGateway gw = new PayeezyGateway(
                    apiKey: "WxCVNWlOHtkujzDRvncAISuZqdSe4jGJ",
                    apiSecret: "87486dc9fc72c651cdb95254752cf567f39afd24a88bb496213cda88f87d457c",
                    token: "fdoa-512fe3c70168dcbcc8cd34efb3e3d992512fe3c70168dcbc",
                    environment: PayeezyEnvironment.Certification
                );
#else
            PayeezyGateway gw = new PayeezyGateway(
                    apiKey: "NDo26B5AGVuEWOp0qZe2zGeWBeB7SLcf",
                    apiSecret: "47179083810ecfdd4a8d48bc77bdbabc384dd2db7ddf9e57b6b11a07e822191f",
                    token: "fdoa-e0b531ea1675d9897f0894a4548f4a2ae0b531ea1675d989",
                    environment: PayeezyEnvironment.Production
                );
#endif
            Response resp = gw.CreditCardPurchase(creditCardNumber, expirationMonth, expirationYear, totalAmount.ToString(), "MANUAL ENTRY", "", transaction.id.ToString());

            if (resp.ParsedBankResponseCode == BankResponseCode.Approved && resp.ParsedGatewayResponseCode == GatewayResponseCode.TransactionNormal)
            {
                DateTime parsedExpirationDate;

                CreditCardType cardType = gw.ValidateAndParseCardDetails(creditCardNumber, expirationMonth, expirationYear, out parsedExpirationDate);

                transaction.payments.Add(new Payment(0, transaction, DateTime.Now, PaymentType.CREDIT, totalAmount, cardType, resp));
                transaction.transaction_fee = transaction_fee;
            }


            return resp;
        }
    }
}
