﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GreekFestPOS.Util
{
    class TransactionResponse
    {
        private PayPal.Payments.DataObjects.TransactionResponse payflowResponse;

        public TransactionResponse(PayPal.Payments.DataObjects.TransactionResponse payflowResponse)
        {
            this.payflowResponse = payflowResponse;
        }

        public bool success
        {
            get { return payflowResponse.Result == 0; }
        }

        public string transactionID
        {
            get { return payflowResponse.Pnref; }
        }

        public PayPal.Payments.DataObjects.TransactionResponse rawResponse 
        {
            get { return payflowResponse; }
        }
    }
}
