﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GreekFestPOS.Util
{
    public class Status
    {

        public String name { get; private set; }
        public int id { get; private set; }

        public static readonly Status NEW = new Status(1, "New");
        public static readonly Status COMPLETED = new Status(2, "Completed");
        public static readonly Status VOIDED = new Status(3, "Voided");
        public static readonly Status DENIED = new Status(4, "Denied");

        private Status(int id, String name)
        {
            this.name = name;
            this.id = id;
        }

        public static Status loadById(int id)
        {
            if(id==NEW.id)
            {
                return NEW;
            }
            else if (id == COMPLETED.id)
            {
                return COMPLETED;
            }
            else if (id == VOIDED.id)
            {
                return VOIDED;
            }
            else if (id == DENIED.id)
            {
                return DENIED;
            }

            return null;
        }

        public override String ToString()
        {
            return name;
        }
    }
}
