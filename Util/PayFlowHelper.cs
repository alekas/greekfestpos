﻿using GreekFestPOS.Model;
using GreekFestPOS.POCO;
using PayPal.Payments.Common;
using PayPal.Payments.Common.Utility;
using PayPal.Payments.DataObjects;
using PayPal.Payments.Transactions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace GreekFestPOS.Util
{
    class PayFlowHelper
    {
        private static Regex regex = new Regex(@"^%(?<FormatCode>.)(?<PrimaryAccountNumber>[\d]{1,19})\^(?<Name>.{2,26})\^(?<ExpirationDate>[\d]{0,4}|\^)(?<ServiceCode>[\d]{0,3}|\^)(?<DiscretionaryData>.*)\?;(?<PrimaryAccountNumber2>[\d]{1,19})=(?<ExpirationDate2>[\d]{0,4}|=)(?<ServiceCode2>[\d]{0,3}|=)(?<DiscretionaryData2>.*)\?\Z");

        public static TransactionResponse processTransaction(Transaction transaction, string swipeData)
        {

#if DEBUG
            swipeData = "%B4111111111111111^DOE/ JOHN                 ^19061010000000      00852000000?;4111111111111111=19061010000000852?";
#endif

            string track1 = swipeData.Substring(0, swipeData.IndexOf(';'));
            string track2 = swipeData.Substring(swipeData.IndexOf(';')); 

            MatchCollection matches = regex.Matches(swipeData);
            if (matches.Count > 0)
            {
                foreach (Match match in matches)
                {
                    string formatCode = match.Groups["FormatCode"].Value;
                    string primaryAccountNumber = match.Groups["PrimaryAccountNumber"].Value;
                    string name = match.Groups["Name"].Value;
                    string expirationDate = match.Groups["ExpirationDate"].Value;
                    string serviceCode = match.Groups["ServiceCode"].Value;
                    string discretionaryData = match.Groups["DiscretionaryData"].Value;
                }
            }


            SwipeCard swipeCard = new SwipeCard(track2);
            // Create a new Tender - Swipe Tender data object.
            CardTender cardTender = new CardTender(swipeCard);

            return processTransaction(transaction, cardTender);
        }

        public static TransactionResponse processTransaction(Transaction transaction, string creditCardNumber, string expirationDate)
        {
            CreditCard creditCard = new CreditCard(creditCardNumber, expirationDate.Replace("/",""));
            CardTender cardTender = new CardTender(creditCard);

            return processTransaction(transaction, cardTender);
        }

        public static TransactionResponse processTransaction(Transaction transaction, CardTender card)
        {
            // Create the Data Objects.
            // Create the User data object with the required user details.
            UserInfo User = new UserInfo("HolyTrinity", "HolyTrinity", "PAYPAL", "GreekFest123$");
            

            // Create the Payflow  Connection data object with the required connection details.
            // The PAYFLOW_HOST property is defined in the App config file.
            PayflowConnectionData Connection = new PayflowConnectionData(App.getConfig("payflow_host").ToString(), 443, 1);


            // Create a new Invoice data object with the Amount, Billing Address etc. details.
            Invoice Inv = new Invoice();
            
            decimal transaction_fee = decimal.Parse((string)App.getConfig("credit_card_fee"));

            Inv.Amt = new Currency(transaction.calculatedTotal + transaction_fee);
            Inv.InvNum = "Tx" + transaction.id;

            // Create a new Payment Device - Swipe data object.  The input parameter is Swipe Data.
            // Used to pass the Track 1 or Track 2 data (the card’s magnetic stripe information) for card-present
            // transactions. Include either Track 1 or Track 2 data—not both. If Track 1 is physically damaged,
            // the POS application can send Track 2 data instead.

            // The parameter data for the SwipeCard object is usually obtained with a card reader.
            // NOTE: The SWIPE parameter is not supported on accounts where PayPal is the Processor.

            // Create a new Sale Transaction using Swipe data.
            SaleTransaction Trans = new SaleTransaction(User, Connection, Inv, card, PayflowUtility.RequestId);


            // Submit the Transaction
            Response Resp = Trans.SubmitTransaction();

            // Display the transaction response parameters.
            if (Resp != null)
            {
                // Get the Transaction Response parameters.
                PayPal.Payments.DataObjects.TransactionResponse TrxnResponse = Resp.TransactionResponse;

                if (TrxnResponse != null)
                {
                    Console.WriteLine("RESULT = " + TrxnResponse.Result);
                    Console.WriteLine("PNREF = " + TrxnResponse.Pnref);
                    Console.WriteLine("RESPMSG = " + TrxnResponse.RespMsg);
                    Console.WriteLine("AUTHCODE = " + TrxnResponse.AuthCode);
                    // If value is true, then the Request ID has not been changed and the original response
                    // of the original transction is returned. 
                    Console.WriteLine("DUPLICATE = " + TrxnResponse.Duplicate);

                    if (TrxnResponse.Result == 0)
                    {
                        transaction.payments.Add(new Payment(0, transaction, DateTime.Now, PaymentType.CREDIT, decimal.Parse(Inv.Amt.ToString()), TrxnResponse.Pnref, TrxnResponse.Result, TrxnResponse.AuthCode));
                        transaction.transaction_fee = transaction_fee;
                    }
                }

                // Display the response.
                Console.WriteLine(Environment.NewLine + PayflowUtility.GetStatus(Resp));

                // Get the Transaction Context and check for any contained SDK specific errors (optional code).
                Context TransCtx = Resp.TransactionContext;
                if (TransCtx != null && TransCtx.getErrorCount() > 0)
                {
                    Console.WriteLine(Environment.NewLine + "Transaction Errors = " + TransCtx.ToString());
                }
                return new TransactionResponse(TrxnResponse);
            }
            return null;
        }
    }
}
