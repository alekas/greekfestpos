﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GreekFestPOS.Util
{
    public class PaymentType
    {

        public String name { get; private set; }
        public int id { get; private set; }

        public static readonly PaymentType CASH = new PaymentType(1, "Cash");
        public static readonly PaymentType CREDIT = new PaymentType(2, "Credit");

        private PaymentType(int id, String name)
        {
            this.name = name;
            this.id = id;
        }

        public static PaymentType loadById(int id)
        {
            if(id==CASH.id)
            {
                return CASH;
            }
            else if (id == CREDIT.id)
            {
                return CREDIT;
            }
            return null;
        }

        public override String ToString()
        {
            return name;
        }
    }
}
