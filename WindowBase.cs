﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;

namespace GreekFestPOS
{
    public class WindowBase : Window
    {
        public WindowBase()
        {
            this.DataContextChanged += new DependencyPropertyChangedEventHandler(this.OnDataContextChanged);
            Loaded += (sender, e) => MoveFocus(new TraversalRequest(FocusNavigationDirection.Next));
        }

        private void OnDataContextChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            if (e.NewValue is IRequestCloseViewModel)
            {
                // if the new datacontext supports the IRequestCloseViewModel we can use
                // the event to be notified when the associated viewmodel wants to close
                // the window
                ((IRequestCloseViewModel)e.NewValue).RequestClose += (s, e1) => this.Hide();
            }
        }
    }
}
